package utils;

/**
 * @author: jzq
 * @date: 2020/7/22 16:58
 * @description:
 */
public class UserDetails {
    private    String name="";
    private    String idcard="";

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getIdcard() {
        return idcard;
    }

    public void setIdcard(String idcard) {
        this.idcard = idcard;
    }
}