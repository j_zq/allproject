package utils;

import java.util.Random;

public class RandomString {

	private static char[] codeSequence = {  '0', '1', '2', '3', '4', '5', '6', '7', '8', '9' };

	public static String getRandomString(int length) {
		String base = "abcdefghijklmnopqrstuvwxyz0123456789";
		Random random = new Random();
		StringBuffer sb = new StringBuffer();
		for (int i = 0; i < length; i++) {
			int number = random.nextInt(base.length());
			sb.append(base.charAt(number));
		}
		return sb.toString();
	}
	
//	public static void main(String[] args) {
//		System.out.println("16位字符串："+getRandomString(16));
//	}

	public static  String   generateCode(Integer codeCount){
		// 随机产生codeCount数字的验证码。
		// 创建一个随机数生成器类
		StringBuffer randomCode = new StringBuffer();
		Random random = new Random();
		for (int i = 0; i < codeCount; i++) {
			// 得到随机产生的验证码数字。
			String code = String.valueOf(codeSequence[random.nextInt(10)]);
			// 将产生的随机数组合在一起。
			randomCode.append(code);
		}
		return randomCode.toString();

	}

	public static void main(String[] args) {

	}

}
