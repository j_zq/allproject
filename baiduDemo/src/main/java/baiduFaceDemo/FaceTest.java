package baiduFaceDemo;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONArray;
import com.baidu.aip.face.AipFace;
import org.json.JSONObject;
import org.junit.Test;
import utils.FaceIdCard;
import utils.RandomString;
import utils.UserDetails;

import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.*;

/**
 * 自己用于测试人脸识别技术
 */
public class FaceTest {
    private static String APP_ID = "18064759";
    private static String API_KEY = "O3rsadGjZSBXnfxZ8HOzBIbQ";
    private static String SECRET_KEY = "ucVqMlPIN4653K2aco78I11ycSW6r6WM";
    private static String appCode = "29c6a6e2a2064a7eb8e3442c3984e658";
    private static String url = "http://faceidcard.api.bdymkt.com/face_id_card/compare";


    /**
     * 这是将指定的图片进行处理
     * @param imgFile
     * @return
     */
    private String GetImageStr(String imgFile) {//将图片文件转化为字节数组字符串，并对其进行Base64编码处理
        //这里改为你照片的位置加上照片的名字
        if(imgFile==null||"".equals(imgFile)){
            imgFile = "D:\\b.jpg";
        }
        InputStream in = null;
        byte[] data = null;
        //读取图片字节数组
        try {
            in = new FileInputStream(imgFile);
            data = new byte[in.available()];
            in.read(data);
            in.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
        //对字节数组Base64编码
//        BASE64Encoder encoder = new BASE64Encoder();
//        return encoder.encode(data);//返回Base64编码过的字节数组字符串
        return Arrays.toString(data);
       // return "nul";
    }
    /**
     * 方法是新增一个角色,相同的人用一次
     */
    @Test
    public void add() throws IOException {
        AipFace aipFace = createAipFace();
        HashMap<String, String> hashMap = new HashMap<>();
        hashMap.put("createTime", new Date() + "");
        String imageStr = GetImageStr("D:\\b.jpg");
        String faceId = RandomString.getRandomString(16);//用户唯一标识  比对时需要
        //JSONObject jsonObject = aipFace.addUser(str.toString(), "jpg", "JNC_PROD", "222", hashMap);
        JSONObject jsonObject = aipFace.addUser(imageStr, "BASE64", "FYX5201", faceId, hashMap);
        System.out.println(jsonObject);
        System.err.println("error_msg:" + jsonObject.get("error_msg"));
        System.err.println("error_code:" + jsonObject.get("error_code"));
    }
    //实名认证
    @Test
    public void addFor() throws IOException {
        HashMap<String, String> params = new HashMap<>();
        UserDetails details = new UserDetails();
//        params.put("name", details.getName());
//        params.put("idcard", details.getIdcard());
        params.put("name", "江中全");
        params.put("idcard", "510922199401254211");
        String imageStr = GetImageStr("D:\\my.jpg");
        params.put("image", imageStr);
        Map result = FaceIdCard.postForm(appCode,url, params);
        System.out.println("实名认证："+result);
    }


    /**
     * 这是验证是否是同一个人。。。
     */
    @Test
    public void search() {
        AipFace aipFace = createAipFace();
        HashMap<String, String> hashMap = new HashMap<>();
        hashMap.put("createTime", new Date() + "");
        String imageStr = GetImageStr("D:\\b.jpg");
        String userId = new Random().nextInt(100) + "";
        //JSONObject jsonObject = aipFace.addUser(str.toString(), "jpg", "JNC_PROD", "222", hashMap);
        JSONObject jsonObject = aipFace.search(imageStr, "BASE64", "FYX520", hashMap);
        JSONObject result = (JSONObject) jsonObject.get("result");
        JSONArray user_list = JSON.parseArray(result.get("user_list").toString());
        System.out.println(jsonObject);
        System.err.println("相似度：" + ((com.alibaba.fastjson.JSONObject) user_list.get(0)).get("score"));
        System.err.println("error_msg:" + jsonObject.get("error_msg"));
        System.err.println("error_code:" + jsonObject.get("error_code"));
    }
    private AipFace createAipFace() {
        return new AipFace(APP_ID, API_KEY, SECRET_KEY);
    }




}
