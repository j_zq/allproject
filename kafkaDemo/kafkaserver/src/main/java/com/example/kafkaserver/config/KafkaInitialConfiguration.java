package com.example.kafkaserver.config;

import org.apache.kafka.clients.admin.NewTopic;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 * @author: jzq
 * @date: 2020/9/1 16:12
 * @description:
 */
@Configuration
public class KafkaInitialConfiguration {

    //创建TopicName为topic.quick.initial的Topic并设置分区数为8以及副本数为1
    @Bean
    public NewTopic initialTopic() {
        return new NewTopic("topic.quick.initial",10, (short) 1 );
    }
    @Bean
    public NewTopic YTopic() {
        return new NewTopic("yTopic",10, (short) 1 );
    }

}