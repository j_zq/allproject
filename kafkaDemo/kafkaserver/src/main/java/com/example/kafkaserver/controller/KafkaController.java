package com.example.kafkaserver.controller;

import io.swagger.annotations.Api;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.kafka.annotation.KafkaListener;
import org.springframework.kafka.annotation.PartitionOffset;
import org.springframework.kafka.annotation.TopicPartition;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.retry.support.RetryTemplate;
import org.springframework.util.concurrent.ListenableFuture;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;

import java.util.concurrent.ExecutionException;

/**
 * @author: jzq
 * @date: 2020/8/31 16:50
 * @description:
 * 本地kafka启动  命令  .\bin\windows\kafka-server-start.bat .\config\server.properties
 * 盘符：D:\chrome\kafka_2.13-2.6.0
 *
 */
@RestController
@RequestMapping("/kafka")
@Api("kafkaServiceController")
public class KafkaController {
    @Autowired
    private KafkaTemplate<String,Object>  kafkaTemplate;
    @Autowired
    private RestTemplate  restTemplate;
    private static final Logger log = LoggerFactory.getLogger(KafkaController.class);
    @GetMapping("/restTemplate")
    public String  restTemplate( ){
        for (int i = 0; i < 1000; i++) {
            new Thread(()->{
                restTemplate.postForObject("http://localhost:8089/goods/updateForGoods",
                        "ok",Object.class);
            }).start();
        }
        return "ok";
    }
    @GetMapping("/")
    public String  kafkaBegin(String work){
        log.info("threadName:{}",Thread.currentThread().getName());
        System.out.println("work："+work);
        System.out.println("kafka:"+kafkaTemplate);
        return "ok";
    }
    @GetMapping("/kafkaTemplate")
    public String  kafkaTemplate(String work){
        for (int i = 0; i < 20; i++) {
            new Thread(()->{
                ListenableFuture topic_input = kafkaTemplate.send("topic_input", work);
            }).start();
        }
        return "kafkaTemplateOk";
    }
    @GetMapping("/kafkaTemplateTopic")
    public String  kafkaTemplateTopic(String work){
        ListenableFuture topic_input = kafkaTemplate.send("topic1", work);
        return "kafkaTemplateTopic";
    }
    @GetMapping("/kafkaTemplateTopic2")
    public String  kafkaTemplateTopic2(String work) throws ExecutionException, InterruptedException {
        ListenableFuture topic_input = kafkaTemplate.send("topic2",work);
        Object o = topic_input.get();
        return "kafkaTemplateTopic2";
    }


    @KafkaListener(id = "webGroup4", topics = "topic2")
    public void  consumerListerN(Object object){
        System.out.println("KafkaConsumerController,consumerListerProvider:"+object);
    }
    @GetMapping("/kafkaTemplateByTopic")
    public String  kafkaTemplateByTopic(String  topics,String  work,Integer size){
        if(size!=null&&size!=-1){
            for (int i = 0; i < 10; i++) {
                new Thread(()->{
                    ListenableFuture topic_input = kafkaTemplate.send(topics,size,"myKey",work);
                }).start();
            }
//            ListenableFuture topic_input = kafkaTemplate.send(topics,size,"MyKey", work);
            return "topics,Ok!";
        }
        for (int i = 0; i < 10; i++) {
            new Thread(()->{
                ListenableFuture topic_input = kafkaTemplate.send(topics,work);
            }).start();
        }
        return "topics,Ok!";
    }
    @GetMapping("/kafkaTemplateAck")
    public String  kafkaTemplateAck() {
        ListenableFuture topic_input = kafkaTemplate.send("ack", "work");
        return "kafkaTemplateAck.ok";
    }
    @KafkaListener(id = "webGroup5", topicPartitions = {
            @TopicPartition(topic = "topic.quick.initial", partitions = {"0", "1"}
            ,partitionOffsets = @PartitionOffset(partition = "5",initialOffset = "10"))
    },concurrency = "6",errorHandler = "myErrorHandler")
    public void listen021(String input) {
        //System.out.println(1/0);
        System.out.println("myErrorHandler:listen021:"+input);
    }
    @KafkaListener(id = "webGroup6", topicPartitions = {
            @TopicPartition(topic = "topic.quick.initial", partitions = {"1","2", "3"})
    },concurrency = "6",errorHandler = "myErrorHandler")
    public void listen023(String input) {
        //System.out.println(1/0);
        System.out.println("myErrorHandler:listen023:"+input);
    }

}