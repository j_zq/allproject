package com.example.kafkaserver;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.web.client.RestTemplateBuilder;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Primary;
import org.springframework.web.client.RestTemplate;

/**
 * @author jzq
 * @description
 * @date 2020/9/1
 * 本地kafka启动  命令  .\bin\windows\kafka-server-start.bat .\config\server.properties
 * 盘符：D:\chrome\kafka_2.13-2.6.0
 **/
@SpringBootApplication
public class KafkaServerApplication {

    public static void main(String[] args) {
        SpringApplication.run(KafkaServerApplication.class, args);
    }
    @Bean
    public RestTemplate restTemplate(RestTemplateBuilder builder) {
        return builder.build();
    }
}
