package com.example.kafkaserver.handle;

import org.apache.kafka.clients.producer.ProducerRecord;
import org.apache.kafka.clients.producer.RecordMetadata;
import org.springframework.kafka.support.ProducerListener;
import org.springframework.stereotype.Component;

/**
 * @author: jzq
 * @date: 2020/9/1 16:52
 * @description:
 */
@Component
public class KafkaSendResultHandler implements ProducerListener {

    @Override
    public void onSuccess(ProducerRecord producerRecord, RecordMetadata recordMetadata) {
        System.err.println("success");
    }

    @Override
    public void onError(ProducerRecord producerRecord, Exception exception) {
        System.err.println("fail");
    }
}