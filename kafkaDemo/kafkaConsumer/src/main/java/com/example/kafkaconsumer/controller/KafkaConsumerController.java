package com.example.kafkaconsumer.controller;

import org.springframework.kafka.annotation.KafkaListener;
import org.springframework.kafka.annotation.PartitionOffset;
import org.springframework.kafka.annotation.TopicPartition;
import org.springframework.web.bind.annotation.RestController;

/**
 * @author: jzq
 * @date: 2020/9/1 11:13
 * @description:
 */
@RestController
public class KafkaConsumerController {
    @KafkaListener(id = "webGroup3", topics = "topic1")
    public void  consumerLister(Object object){
        System.out.println("KafkaConsumerController,consumerLister:webGroup3:"+object);
    }
    @KafkaListener(id = "webGroup1",  topics = "topic2")
    public void  consumerListerNotId(Object object){
        System.out.println("KafkaConsumerController,consumerListerNotId:webGroup1:"+object);
    }
    @KafkaListener(id = "webGroup4", topics = "topic2")
    public void  consumerListerConsumer(Object object){
        System.out.println("KafkaConsumerController,consumerListerConsumer:"+object);
    }
    @KafkaListener(id = "group12", topics = "yTopic",groupId = "*")
    public void listenGroupId12(String input) {
        System.out.println("consumerKafka,listenGroupId12:"+input);
    }
}