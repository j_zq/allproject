package com.example.kafkasecondconsumer;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.kafka.annotation.EnableKafka;

@SpringBootApplication
@EnableKafka
public class KafkaSecondConsumerApplication {

    public static void main(String[] args) {
        SpringApplication.run(KafkaSecondConsumerApplication.class, args);
    }

}
