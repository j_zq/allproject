package com.example.kafkasecondconsumer.handle;

import org.apache.kafka.clients.admin.NewTopic;
import org.springframework.context.annotation.Bean;
import org.springframework.kafka.listener.KafkaListenerErrorHandler;
import org.springframework.kafka.listener.ListenerExecutionFailedException;
import org.springframework.messaging.Message;
import org.springframework.stereotype.Service;

/**
 * @author: jzq
 * @date: 2020/9/1 10:45
 * @description:  kafka 监听类错误处理方法
 */
@Service("myErrorHandler")
public class MyKafkaListenerHandler implements KafkaListenerErrorHandler {
    @Override
    public Object handleError(Message<?> message, ListenerExecutionFailedException e) {
        System.out.println("MyKafkaListenerHandler!!!!!!!");
        return null;
    }
}