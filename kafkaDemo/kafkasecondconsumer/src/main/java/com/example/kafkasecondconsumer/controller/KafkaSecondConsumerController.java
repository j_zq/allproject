package com.example.kafkasecondconsumer.controller;

import org.springframework.kafka.annotation.KafkaListener;
import org.springframework.kafka.annotation.PartitionOffset;
import org.springframework.kafka.annotation.TopicPartition;
import org.springframework.web.bind.annotation.RestController;

/**
 * @author: jzq
 * @date: 2020/9/1 11:13
 * @description:
 */
@RestController
public class KafkaSecondConsumerController {
    @KafkaListener(id = "webGroup3", topics = "topic1")
    public void  consumerLister(Object object){
        System.out.println("KafkaConsumerController,consumerLister:webGroup3"+object);
    }
    @KafkaListener(id = "webGroup1",topics = "topic2")
    public void  consumerListerNotId(Object object){
        System.out.println("KafkaConsumerController,consumerListerNotId:webGroup1:"+object);
    }
    @KafkaListener(id = "webGroup4", topics = "topic2")
    public void  consumerListerConsumer(Object object){
        System.out.println("KafkaConsumerController,consumerListerConsumer:webGroup4:"+object);
    }
//    @KafkaListener(id = "webGroup", topics = "topic_input")
//    public void listen(String input) {
//        System.out.println("input:"+input);
//    }
    @KafkaListener(id = "webGroup7", topicPartitions = {
            @TopicPartition(topic = "topic.quick.initial", partitions = {"1","2", "3"})
    },concurrency = "6",errorHandler = "myErrorHandler")
    public void listen023(String input) {
        //System.out.println(1/0);
        System.out.println("myErrorHandler:listen023:webGroup7"+input);
    }
    @KafkaListener(id = "webGroup99", topics = "ack")
    public void listenAck(String input) {
        System.out.println("input:"+input);
    }
    @KafkaListener(id = "group11", topics = "yTopic",groupId = "*")// 01 23
    public void listenGroupId11(String input) {
        System.out.println("listenGroupId11:"+input);
    }
    @KafkaListener(id = "group12", topics = "yTopic",groupId = "*")  //789
    public void listenGroupId12(String input) {
        System.out.println("listenGroupId12:"+input);
    }

    @KafkaListener(id = "group14",topicPartitions = {
            @TopicPartition(topic = "yTopic", partitions = {"0", "1"})
    },concurrency = "6",errorHandler = "myErrorHandler")  //6 7 8
    public void listenGroupId14(String input) {
        System.out.println("listenGroupId14:"+input);
    }

}