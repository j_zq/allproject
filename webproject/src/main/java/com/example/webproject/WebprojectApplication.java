package com.example.webproject;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.cache.annotation.EnableCaching;
import org.springframework.data.redis.core.RedisTemplate;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

import javax.annotation.Resource;

@EnableConfigurationProperties
@SpringBootApplication
@MapperScan("com.example.webproject.com.mapper")
@EnableSwagger2
@EnableCaching
public class WebprojectApplication {
    public static void main(String[] args) {
        SpringApplication.run(WebprojectApplication.class, args);

    }

        @Resource
    private RedisTemplate<String, Object> redisTemplate;

    /**
     * 普通缓存获取
     * @param key 键
     * @return 值
     */
    public Object get(String key) {
        return key == null ? null : redisTemplate.opsForValue().get(key);
    }

}
