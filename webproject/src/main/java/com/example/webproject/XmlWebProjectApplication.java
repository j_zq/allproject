package com.example.webproject;

import com.example.webproject.com.interfaceDemo.XmlDemoInterface;
import com.example.webproject.com.service.entityService.XmlDemoInterfaceImpl;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

public class XmlWebProjectApplication {
        public static void main(String[] args) {
        // 用我们的配置文件来启动一个 ApplicationContext
        ApplicationContext context = new ClassPathXmlApplicationContext("classpath:xmlApplication.xml");

//        System.out.println("context 启动成功");

        // 从 context 中取出我们的 Bean，而不是用 new MessageServiceImpl() 这种方式
        XmlDemoInterface xmlDemoInterface = context.getBean(XmlDemoInterfaceImpl.class);
        // 这句将输出: hello world
        xmlDemoInterface.XmlDemoInterfaceTest();
    }
}
