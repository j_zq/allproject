package com.example.webproject.com.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

/**
 * @author : jzq
 * @date: 2020/4/9 16:37
 * @description: 商品信息
 */
@Data
@Accessors(chain = true)
@EqualsAndHashCode(callSuper = false)
public class Goods {
    @TableId(value = "id", type = IdType.AUTO)
    private  Integer  id;
    /**
     * 编号
     */
    private   Integer  no;
    private   Double price;
    private   Integer num ;
    private   String  url;

    }