package com.example.webproject.com.task.runner;

import com.example.webproject.com.entity.task.ScheduledTaskBean;
import com.example.webproject.com.task.intf.ScheduledTaskService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.ApplicationArguments;
import org.springframework.boot.ApplicationRunner;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;

/**
 * @author: jzq
 * @date: 2020/8/26 16:53
 * @description:
 */
@Component
public class TaskRunner implements ApplicationRunner {
    @Autowired
    private ScheduledTaskService scheduledTaskService;

    @Override
    public void run(ApplicationArguments args) throws Exception {
        //暂时关闭
//        List<ScheduledTaskBean> scheduledTasks = new ArrayList<>();
//        ScheduledTaskBean scheduledTaskBean = new ScheduledTaskBean();
//        scheduledTaskBean.setId(1);
//        scheduledTaskBean.setInitStartFlag(1);
//        scheduledTaskBean.setCron("0/5 * * * * *");
//        scheduledTaskBean.setClassName("com.example.webproject.com.task.runner.TestRunner");
//        scheduledTasks.add(scheduledTaskBean);
//        scheduledTaskService.initAllTask(scheduledTasks);
    }
}