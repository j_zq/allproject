package com.example.webproject.com.config;


import org.redisson.Redisson;
import org.redisson.api.RedissonClient;
import org.redisson.config.ClusterServersConfig;
import org.redisson.config.Config;
import org.redisson.config.SingleServerConfig;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.condition.ConditionalOnClass;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.util.StringUtils;

@Configuration
@ConditionalOnClass(Config.class)
@EnableConfigurationProperties(RedissonProperties.class)
public class RedissonAutoConfiguration {

    @Autowired
    private RedissonProperties properties;

    @Bean
    @ConditionalOnProperty(name="redisson.address")
    RedissonClient redissonSingle() {
        Config config = new Config();
        String address=properties.getAddress();
        if(address==null||"".equals(address)){
            address="redis://localhost:6379";
        }
        SingleServerConfig serverConfig = config.useSingleServer()
                .setAddress(address)
                .setTimeout(properties.getTimeout())
                .setConnectionPoolSize(properties.getConnectionPoolSize())
                .setConnectionMinimumIdleSize(properties.getConnectionMinimumIdleSize());
        if(!StringUtils.isEmpty(properties.getPassword())) {
            serverConfig.setPassword(properties.getPassword());
        }

        return Redisson.create(config);
    }


    @Bean
    @ConditionalOnProperty(name="redisson.cluster.nodes")
    RedissonClient redissonClusters() {
        Config config = new Config();
        ClusterServersConfig clusterServersConfig = config.useClusterServers();
        clusterServersConfig
                .setTimeout(properties.getTimeout())
                // 地址 redis:// + host: port
                .addNodeAddress(properties.getNodes().toArray(new String[properties.getNodes().size()]))
                // 连接池大小
                .setMasterConnectionPoolSize(properties.getConnectionPoolSize())
                // 连接数空闲大小
                .setMasterConnectionMinimumIdleSize(properties.getConnectionMinimumIdleSize());
        if(!StringUtils.isEmpty(properties.getPassword())) {
            clusterServersConfig.setPassword(properties.getPassword());
        }

        return Redisson.create(config);
    }




    @Bean
    RedisLockUtil redissLockUtil( RedissonClient redissonClient) {
        RedisLockUtil redissLockUtil = new RedisLockUtil();
        redissLockUtil.setRedissonClient(redissonClient);
        System.out.println("redissLockUtil"+redissLockUtil);
        return redissLockUtil;
    }

}