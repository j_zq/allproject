package com.example.webproject.com.util;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.RedisTemplate;
/**
 * @author: jzq
 * @date: 2020/7/17 10:49
 * @description:  操作redis的工具类
 */
public class MyRedisUtils {
    @Autowired
    private  RedisTemplate redisTemplate;
    public void  boundHashOpsPut(String hashKey,String mapKey,Object value){
      //  redisTemplate.boundStreamOps();
        redisTemplate.boundHashOps(hashKey).put(mapKey,value);
        System.out.println("==========存入");
    }
    public  Object    boundHashOpsGet(String hashKey,String mapKey){
        return  redisTemplate.boundHashOps(hashKey).get(mapKey);
    }
    public void  boundListOpsAdd(String listKey,Object value){
        //  redisTemplate.boundStreamOps();
        redisTemplate.boundListOps(listKey).leftPush(value);
        System.out.println("boundListOps==========存入");
    }
    public Object  boundListOpstGet(String listKey){
        //  redisTemplate.boundStreamOps();
       return redisTemplate.boundListOps(listKey).getKey();
    }
    public void  boundValueOpsAdd(String valueKey,Object value){
        //  redisTemplate.boundStreamOps();
        redisTemplate.boundValueOps(valueKey).set(value);
        System.out.println("boundListOps==========存入");
    }
    public Object  boundValueOpsGet(String valueKey){
        //  redisTemplate.boundStreamOps();
        return redisTemplate.boundValueOps(valueKey);
    }
}