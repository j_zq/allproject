package com.example.webproject.com.controller.redisController;

import com.example.webproject.com.service.entityService.RedisTemplateService;
import com.example.webproject.com.util.MyRedisUtils;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.concurrent.TimeUnit;

/**
 * @author: jzq
 * @date: 2020/7/17 13:54
 * @description:
 */

@RestController
@RequestMapping("/redis")
@Api(tags = "RedisTemplateController", value = "redisTest")
public class RedisTemplateController {
    MyRedisUtils utils= new MyRedisUtils();
    @Autowired
    private RedisTemplate redisTemplate;
    @Autowired
    private RedisTemplateService redisTemplateService;
    @PostMapping("/boundHashOpsPut")
    public  Object boundHashOpsPut(String hashKey,String mapKey,String value){
        try{
            redisTemplate.boundHashOps(hashKey).put(mapKey,value);
            return redisTemplate.boundHashOps(hashKey).get(mapKey);
        }catch (Exception e) {
            e.printStackTrace();
            return "失败啦！";
        }
    }
    @PostMapping("/boundListOps")
    public  Object boundListOps(String listKey,Object value){
        try{
            utils.boundListOpsAdd(listKey,value);
            return   utils.boundListOpstGet(listKey);
        }catch (Exception e) {
            e.printStackTrace();
            return "失败啦！";
        }
    }
    @PostMapping("/boundValueOps")
    public  Object boundValueOps(String valueKey,String value){
        try{
            Object o = redisTemplate.opsForValue().get(valueKey);
            System.out.println("o:"+o);
            return o  ;
        }catch (Exception e) {
            e.printStackTrace();
            return "失败啦！";
        }
    }
    @PostMapping("/boundHashPut")
    @ApiOperation(value = "设置hash值")
    public  Object boundHashPut(String hashKey,String mapKey,String value){
        try{
            redisTemplateService.hashPutExpire(hashKey,mapKey,value,10L,TimeUnit.SECONDS);
        }catch (Exception e) {
            e.printStackTrace();
            return "失败啦！";
        }
        return "ok";
    }
    @PostMapping("/boundHashPutExpire")
    @ApiOperation(value = "设置hash值")
    public  Object boundHashPutExpire(String hashKey,String mapKey,String value){
        try{
            redisTemplateService.hashPutExpire(hashKey,mapKey,value,10L,TimeUnit.SECONDS);
        }catch (Exception e) {
            e.printStackTrace();
            return "失败啦！";
        }
        return  "ok";
    }
    @PostMapping("/boundHashGet")
    @ApiOperation(value = "获取hash值")
    public  Object boundHashGet(String hashKey,String mapKey){
        try{
            return redisTemplateService.hashGet(hashKey, mapKey);
        }catch (Exception e) {
            e.printStackTrace();
            return "失败啦！";
        }
    }
    @PostMapping("/redisExpire")
    @ApiOperation(value = "设置过期时间")
    public  void redisExpire(String hashKey){
        try{
            redisTemplateService.redisExpire(hashKey, 10L,TimeUnit.SECONDS);
        }catch (Exception e) {
            e.printStackTrace();
        }
    }
}