package com.example.webproject.com.restTemplate;

import com.example.webproject.com.entity.Goods;
import io.swagger.annotations.Api;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;

import java.util.ArrayList;
import java.util.List;

/**
 * @author: jzq
 * @date: 2020/7/16 10:11
 * @description:   测试rest风格
 */
@RestController
@RequestMapping("/restTemplate")
@Api(tags = "rest")
public class RestTemplateController {
    @Autowired
    private  RestTemplate restTemplate;
    @PostMapping("/testGetForObject")
    public void testGetForObject(){
        List<Goods> list = restTemplate.getForObject("http://localhost:8089/goods/testSelect", List.class);
        System.out.println(list);
    }
    @PostMapping("/getForEntity")
    public Goods getForEntity(Integer id){
        ResponseEntity<Goods> responseEntity = restTemplate.getForEntity("http://localhost:8089/Goods/findById?id="+id,
                Goods.class);
        Goods Goods = responseEntity.getBody();
        System.out.println(Goods);
        //	1. Create an API to validate the categories under Tier1,2 and megamenu from backend logic. The API will take input as category and check whether this category is valid or not. It could return true or false based on the availability in Megamenu/ Tier1/2. Based on the returned value, the category will be validated and displayed. If true, then the category is valid and will be displayed , else it will not be displayed.
        //  B2bCategoryCheckController
        //	2. The above validation will apply to the categories under every tier. Ex : If the API fetches false for all links under University of 	California Davis Contracted Catalog, then 'University of California Davis Contracted Catalog' will not be displayed with all the links under it. Similarly, If the API fetches false for '2-in-1 laptops' under University of California Davis Contracted Catalog -> Explore laptops, then '2-in-1 laptops' will not be displayed
        return Goods;
    }
    @PostMapping("/getForEntityForEntity")
    public String getForEntityForEntity(Integer id){
        Goods cou = new Goods();
        cou.setId(11).setUrl("测试专业");
        ResponseEntity<String> responseEntity = restTemplate.getForEntity("http://localhost:8089/Goods/restEntity",
                String.class,cou);
        String Goods = responseEntity.getBody();
        System.out.println(Goods);
        return Goods;
    }
    //post 方法接收一个实体类的
    @PostMapping("/postForEntity")
    public String postForEntity(Integer id){
        Goods cou = new Goods();
        cou.setId(222).setUrl("测试专业");
        ResponseEntity<String> c = restTemplate.postForEntity("http://localhost:8089/Goods/postForEntity",
                cou, String.class);
        return c.getBody();
    }
    //post  请求方法接收2个参数的 +实体类的
    @PostMapping("/postForEntityTwoParams")
    public String postForEntity(){
        Goods cou = new Goods();
        cou.setId(33).setUrl("测试专业");
        String Goods = restTemplate.postForObject("http://localhost:8089/Goods/postForEntityTwoParams?cId=1&cName='1'", cou, String.class);
        return Goods;
    }
    @PostMapping("/postForList")
    public String postForList(){
        Goods cou = new Goods();
        cou.setId(44).setUrl("测试专业");
        Goods cou222 = new Goods();
        cou222.setId(55).setUrl("测试专业2");
        ArrayList<Goods> list = new ArrayList<>();
        list.add(cou222);
        list.add(cou);
        String Goods = restTemplate.postForObject("http://localhost:8089/Goods/postForList",
                list, String.class);
        return Goods;
    }
}