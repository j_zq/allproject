package com.example.webproject.com.util;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

public class TimeUtil {
    public static   SimpleDateFormat   getFormat(){
        return new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
    }
    public static String   formatNewDate(Date  date){
        return getFormat().format(date);
    }
     public  static Date  parseDateString(String dateStr)  {

         try {
             return    getFormat().parse(dateStr);
         } catch (ParseException e) {
             e.printStackTrace();
         }
         return null;
     }
}
