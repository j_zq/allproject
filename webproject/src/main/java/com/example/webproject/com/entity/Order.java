package com.example.webproject.com.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

import java.time.LocalDateTime;

/**
 * @author: jzq
 * @date: 2020/6/2 11:37
 * @description:
 */
@Data
@Accessors(chain = true)
@EqualsAndHashCode(callSuper = false)
public class Order {
    @TableId(type = IdType.INPUT)
    private   Long id;
    private  Integer goodsId;
    private  String  username;
    private  Integer num;
    private  LocalDateTime time;
    @TableField(exist = false)
    public   String   testName;
}