package com.example.webproject.com.entity.task;

import com.baomidou.mybatisplus.annotation.TableField;
import lombok.Data;

/**
 * @author: jzq
 * @date: 2020/8/26 16:47
 * @description:
 */
@Data
public class ScheduledTaskBean {

    private Integer id;

    /**
     * 类名
     */
    private String className;
    /**
     * 描述
     */
    private String taskDesc;
    /**
     * 表达式
     */
    private String cron;

    /**
     * 程序初始化是否启动 1 是 0 否
     */
    private Integer initStartFlag;

    /**
     * 当前是否已启动
     */
    @TableField(exist = false)
    private boolean startFlag;
}