package com.example.webproject.com.task.service;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.example.webproject.com.entity.task.ScheduledTaskBean;
import com.example.webproject.com.mapper.ScheduledTaskMapper;
import com.example.webproject.com.task.intf.ScheduledTask;
import com.example.webproject.com.task.intf.ScheduledTaskService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeansException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;
import org.springframework.scheduling.Trigger;
import org.springframework.scheduling.TriggerContext;
import org.springframework.scheduling.concurrent.ThreadPoolTaskScheduler;
import org.springframework.scheduling.support.CronTrigger;
import org.springframework.stereotype.Service;

import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ScheduledFuture;
import java.util.concurrent.locks.ReentrantLock;

/**
 * @author: jzq
 * @date: 2020/8/26 16:48
 * @description:
 */
@Service
public class ScheduledTaskServiceImpl extends ServiceImpl<ScheduledTaskMapper, ScheduledTaskBean>
        implements ScheduledTaskService, ApplicationContextAware {

    private static final Logger logger = LoggerFactory.getLogger(ScheduledTaskServiceImpl.class);

    private ApplicationContext applicationContext;

    @Override
    public void setApplicationContext(ApplicationContext applicationContext) throws BeansException {
        this.applicationContext = applicationContext;
    }

    /**
     * 定时任务线程池
     */
    @Autowired
    private ThreadPoolTaskScheduler threadPoolTaskScheduler;
    /**
     * 存放已经启动的任务map
     */
    private Map<Integer, ScheduledFuture> scheduledFutureMap = new ConcurrentHashMap<>();
    /**
     * 可重入锁
     */
    private ReentrantLock lock = new ReentrantLock();


    @Override
    public List<ScheduledTaskBean> getList() {
        List<ScheduledTaskBean> scheduledTasks = baseMapper.selectList(null);
        scheduledTasks.forEach(item -> {
            item.setStartFlag(this.isStart(item.getId()));
        });
        return scheduledTasks;
    }

    @Override
    public Boolean start(Integer taskId) {
        lock.lock();

        try{
            if(this.isStart(taskId)){
                logger.info("当前任务已经启动，taskId：{}", taskId);
                return false;
            }
            ScheduledTaskBean scheduledTaskBean = baseMapper.selectById(taskId);
            this.doStartTask(scheduledTaskBean);
        } catch (Exception e) {
            logger.info("任务启动失败，taskId：{}", taskId);
            e.printStackTrace();
        } finally {
            lock.unlock();
        }
        logger.info("任务启动成功，taskId：{}", taskId);
        return true;
    }

    @Override
    public Boolean stop(Integer taskId) {

        boolean isExist = scheduledFutureMap.containsKey(taskId);

        if(isExist){
            ScheduledFuture scheduledFuture = scheduledFutureMap.get(taskId);
            scheduledFuture.cancel(true);
            logger.info("任务停止，taskId：{}", taskId);
            return true;
        }else {
            logger.info("该任务不存在");
            return false;
        }
    }

    @Override
    public Boolean restart(Integer taskId) {
        return this.stop(taskId) && this.start(taskId);
    }

    @Override
    public void initAllTask(List<ScheduledTaskBean> scheduledTaskBeans) {
        scheduledTaskBeans.forEach(item -> {
            Integer taskId = item.getId();
            if(!this.isStart(taskId))
                this.doStartTask(item);

        });
        logger.info("任务初始启动成功，任务数：{}", scheduledTaskBeans.size());
    }

    @Override
    public List<ScheduledTaskBean> getOnScheduledTasks() {
        int isInitStart = 1;//初始启动
        List<ScheduledTaskBean> scheduledTaskBeans = baseMapper.selectList(
                new QueryWrapper<ScheduledTaskBean>()
                        .eq("init_start_flag", isInitStart));
        scheduledTaskBeans.forEach(item -> {
            item.setStartFlag(this.isStart(item.getId()));
        });
        return scheduledTaskBeans;
    }

    private void doStartTask(ScheduledTaskBean scheduledTaskBean){
        Integer taskId = scheduledTaskBean.getId();
        String cron = scheduledTaskBean.getCron();
        String className = scheduledTaskBean.getClassName();
        try {
            ScheduledTask scheduledTask = (ScheduledTask)this.applicationContext.getBean(Class.forName(className));
            ScheduledFuture scheduledFuture = threadPoolTaskScheduler.schedule(scheduledTask,
                    new Trigger() {
                        @Override
                        public Date nextExecutionTime(TriggerContext triggerContext) {
                            CronTrigger cronTrigger = new CronTrigger(cron);
                            return cronTrigger.nextExecutionTime(triggerContext);
                        }
                    }
            );
            //将启动的任务放入 map
            scheduledFutureMap.put(taskId, scheduledFuture);
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }
    }

    private boolean isStart(Integer taskId){
        if(scheduledFutureMap.containsKey(taskId))
            return !scheduledFutureMap.get(taskId).isCancelled();
        return false;
    }

}
