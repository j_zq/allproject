package com.example.webproject.com.service.entityService;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.example.webproject.com.entity.Goods;
import com.example.webproject.com.mapper.GoodsMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 * @author jzq
 * @description: 测试mybatisPlus自带功能
 * @date 2020/7/13
 **/
@Service
//@Scope("prototype")
public class GoodsServiceNew extends ServiceImpl<GoodsMapper, Goods> {
    @Autowired
    private GoodsMapper goodsMapper;
    @Autowired B  b;
    public  Goods  testMybatisPlusLambda(){
        //等效  下面的
        Goods goods = baseMapper.selectById(2);
        QueryWrapper<Goods> wrapper = new QueryWrapper<>();
        LambdaQueryWrapper<Goods> last = wrapper.lambda().eq(Goods::getNo, 2).last("limit 1");
        Goods one = baseMapper.selectOne(last);
        return one;
    }
    public  Goods  testMybatisPlusAndNew(){
        LambdaQueryWrapper<Goods> wrapper=new LambdaQueryWrapper<>();
       // wrapper.allEq(g)
        return  new Goods();
    }
    @Transactional
    public  void   add(){
        Goods goods = new Goods().setNo(9999).setUrl("99999").setNum(999).setPrice(999.99);
        super.save(goods);
        System.out.println(goods);
    }
    @Transactional
    public  void   updateFor(){
        LambdaQueryWrapper<Goods> wrapper=new LambdaQueryWrapper<>();
        LambdaQueryWrapper<Goods> eq = wrapper.eq(Goods::getNo, 9999).last(" limit 1");
        Goods one = baseMapper.selectOne(eq);
        one.setPrice(null).setUrl(null).setNum(222);
        saveOrUpdate(one);
    }

    public Object getList() {
        LambdaQueryWrapper<Goods> wrapper = new LambdaQueryWrapper<>();
        return goodsMapper.testForUpdate(wrapper.eq(Goods::getNo,9999));
    }
    public Object getList(String  id,String no) {
        LambdaQueryWrapper<Goods> wrapper = new LambdaQueryWrapper<>();
        wrapper.eq(Goods::getId,id).eq(Goods::getNo,no);
        return goodsMapper.selectList(wrapper);
    }
}