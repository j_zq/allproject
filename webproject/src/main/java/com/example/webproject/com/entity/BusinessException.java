package com.example.webproject.com.entity;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

/**
 * @author : jzq
 * @date: 2020/3/25 11:28
 * @description: 异常信息处理
 */
@Data
@Accessors(chain = true)
@EqualsAndHashCode(callSuper = false)
public class BusinessException extends RuntimeException {
    private String code;
    private String msg;
    private  Object value;
    private  String  className;
    private  String  methodName;

    public BusinessException(String code, String msg, Object value, String className, String methodName) {
        this.code = code;
        this.msg = msg;
        this.value = value;
        this.className = className;
        this.methodName = methodName;
    }
}