package com.example.webproject.com.entity;

import com.alibaba.excel.annotation.ExcelProperty;
import com.alibaba.excel.metadata.BaseRowModel;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

import java.time.LocalDateTime;

/**
 * @author: jzq
 * @date: 2020/7/6 10:32
 * @description: Excel读取
 */
@Data
@Accessors(chain = true)
@EqualsAndHashCode(callSuper = false)
public class ReadModel extends BaseRowModel {
    @ExcelProperty(value = "订单编号",index = 0)
    private String orderNo;
    @ExcelProperty(value = "下单时间",index = 1)
    private LocalDateTime orderTime;
    @ExcelProperty(value = "订单状态",index = 2)
    private  String orderState;

    /**
     * 订单金额
     */
    @ExcelProperty(value = "订单金额",index = 3)
    private Double orderAmount;
    @ExcelProperty(value = "订单渠道",index = 4)
    private String orderChannel;
    @ExcelProperty(value = "订单类型",index = 5)
    private String orderType;
    @ExcelProperty(value = "评价状态",index = 6)
    private String evaluateState;
    /**
     * 实付金额
     */
    @ExcelProperty(value = "订单实付金额",index = 7)
    private Double actualPrice;
    @ExcelProperty(value = "商品地址",index = 8)
    private  String goodsUrl;
    @ExcelProperty(value = "商品名称",index = 9)
    private  String goodsName;
    @ExcelProperty(value = "商品数量",index = 10)
    private  String goodsNum;
    @ExcelProperty(value = "商品金额",index = 11)
    private  String goodsPrice;
    /**
     * 优惠金额
     */
    @ExcelProperty(value = "优惠金额",index = 12)
    private Double discountPrice;
    /**
     * 实付金额
     */
    @ExcelProperty(value = "商品实付金额",index = 13)
    private Double goodsActualPrice;

    @ExcelProperty(value = "用户id",index = 14)
    private  Integer userId;
    @ExcelProperty(value = "用户昵称",index = 15)
    private  String nickName;
    @ExcelProperty(value = "用户来源",index = 16)
    private  String userResources;
    @ExcelProperty(value = "用户姓名",index =17)
    private  String userName;
    @ExcelProperty(value = "用户类型",index = 18)
    private  String userType;
    @ExcelProperty(value = "用户联系方式",index = 19)
    private  String phone;



    /**
     * 物流公司名称
     */
    @ExcelProperty(value = "物流公司名称",index = 20)
    private String logisticsName;

    /**
     * 仓库名称
     */
    @ExcelProperty(value = "仓库名称",index = 21)
    private String storeName;

    /**
     *  仓库 编号
     */
    @ExcelProperty(value = "仓库编号",index = 22)
    private String storeNo;
    /**
     * 收货人
     */
    @ExcelProperty(value = "收货人",index = 23)
    private String receiver;



    /**
     * 地址
     */

    @ExcelProperty(value = "地址",index = 24)
    private String address;

    /**
     *  物流单号
     */
    @ExcelProperty(value = "物流单号",index = 25)
    private String logisticsNo;

    /**
     * 仓库区域
     */

    @ExcelProperty(value = "仓库区域",index = 26)
    private String storeArea;


    /**
     * 发货地址
     */

    @ExcelProperty(value = "发货地址",index = 27)
    private String deliveryAddress;

    /**
     * 联系电话
     */

    @ExcelProperty(value = "联系电话",index = 28)
    private String receiverPhone;

    /**
     * 发货时间
     */

    @ExcelProperty(value = "发货时间",index = 29)
    private LocalDateTime deliveryDate;

    /**
     * 收货时间
     */

    @ExcelProperty(value = "收货时间",index = 30)
    private LocalDateTime receiveDate;
}