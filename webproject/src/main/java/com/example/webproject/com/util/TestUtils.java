package com.example.webproject.com.util;

import com.example.webproject.com.entity.Order;
import com.example.webproject.com.entity.User;
import org.apache.commons.lang3.ArrayUtils;
import org.springframework.beans.BeanUtils;

import java.beans.IntrospectionException;
import java.beans.PropertyDescriptor;
import java.lang.reflect.Field;
import java.lang.reflect.Method;

public class TestUtils {
    public static void main(String[] args) throws IntrospectionException {
        Class   clazz=Order.class;
        Field[] fields = clazz.getFields();
        Field[] declaredFields = clazz.getDeclaredFields();
        fields= ArrayUtils.addAll(fields,declaredFields);
//        fields.
        for (Field field :fields ) {
            PropertyDescriptor pd = new PropertyDescriptor(field.getName(), clazz);
            Method writeMethod = pd.getWriteMethod();
        }
        User user = new User().setId(1).setUsername("1");
        Order order = new Order();
        BeanUtils.copyProperties(user,order);
        System.out.println(order);
    }
}
