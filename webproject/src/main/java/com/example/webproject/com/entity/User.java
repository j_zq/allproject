package com.example.webproject.com.entity;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.Accessors;

import java.io.Serializable;
/**
 * @author : jzq
 * @date: 2020/3/25 11:28
 * @description: 用户信息
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
@Accessors(chain = true)

public class User  implements Serializable {
    //@TableId
    private Integer id;

    private String username;

    private String password;

    private Integer age;
}