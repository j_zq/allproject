package com.example.webproject.com.common;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class ResponseResult <T> implements Serializable {
    private Integer status;

    private T data;

    private String msg;

    private final long timestamps = System.currentTimeMillis();

    public  static <T> ResponseResult<T> e(ResponseCode statusEnum) {
        return e(statusEnum, null);
    }

    public  static <T> ResponseResult<T> e(ResponseCode statusEnum, T data) {
        ResponseResult<T> res = new ResponseResult<>();
        res.setStatus(statusEnum.code);
        res.setMsg(statusEnum.msg);
        res.setData(data);
        return res;
    }

    private static final long serialVersionUID = 8992436576262574064L;
}
