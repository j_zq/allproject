package com.example.webproject.com.task.runner;

import com.example.webproject.com.task.intf.ScheduledTask;
import org.springframework.stereotype.Component;

import java.time.LocalDateTime;

/**
 * @author: jzq
 * @date: 2020/8/26 16:51
 * @description:
 */
@Component
public class TestRunner implements ScheduledTask {
    @Override
    public void run() {
        System.out.println("run 执行辣"+ LocalDateTime.now());
    }
}