package com.example.webproject.com.config;

import com.example.webproject.com.entity.BusinessException;
import com.example.webproject.com.entity.ResponseEntity;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;

/**
 * @author: jzq
 * @date: 2020/3/25 11:29
 * @description: 123
 */
@ControllerAdvice
public class GlobalExceptionHandler {
    private static final Logger logger = LoggerFactory.getLogger(GlobalExceptionHandler.class);

    /**
     * 处理 Exception 异常
     *
     * @param e                  异常
     * @return ResponseEntity
     */
    @ResponseBody
    @ExceptionHandler(value = Exception.class)
    public ResponseEntity exceptionHandler(Exception e) {
        logger.error("服务错误:", e);
        return new ResponseEntity(false, "服务出错",null);
    }

    /**
     * 处理 BusinessException 异常
     *
     * @param e 异常
     * @return ResponseEntity
     */
    @ResponseBody
    @ExceptionHandler(value = BusinessException.class)
    public ResponseEntity businessExceptionHandler( BusinessException e) {
        logger.info("业务异常。code:" + e.getCode() + "msg:" + e.getMsg()+"value:"+e.getValue()+e.getClassName()+e.getMethodName());
        return new ResponseEntity(false, e.getMsg(),null);
    }
}