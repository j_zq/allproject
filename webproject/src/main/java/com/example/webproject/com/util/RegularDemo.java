package com.example.webproject.com.util;

import org.junit.Test;

import java.util.UUID;

/**
 * 正则测试
 */
public class RegularDemo {
    @Test
    public void test1(){
        String  str="123f1231";//正则
        boolean b = str.matches("^1\\w+31$");
        System.out.println(b);
    }
    //匹配电话号码
    @Test
    public void matchPhone(){
        String  str="18702807637";//正则
        boolean b = str.matches("^1[3456789]\\d{9}$");
        System.out.println(b);
    }
    //  ^ 代表以什么开头  $代表以什么结尾  \w 代表
    @Test
    public void test2(){
        String  str="1231";//正则
        boolean b = str.matches("^1\\w(?i)31$");
        System.out.println(b);
        for (int i = 0; i < 10; i++) {
            long node = UUID.randomUUID().getMostSignificantBits();
            System.out.println(node);
        }
        System.out.println("5184850520829244387".length());

    }
}
