package com.example.webproject.com.config;

import org.springframework.context.annotation.Configuration;
import org.springframework.data.redis.core.RedisTemplate;

import javax.annotation.Resource;

@Configuration
public class RedisCacheUtil {
     private static RedisTemplate<String, Object> redisTemplate;
     /**
  6      * 普通缓存获取
  7      * @param key 键
  8      * @return 值
  9      */
             public static Object get(String key) {
                return key == null ? null:redisTemplate.opsForValue().get(key); //redisTemplate对象一直为null
             }
}