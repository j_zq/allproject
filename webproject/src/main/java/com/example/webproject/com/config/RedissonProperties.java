package com.example.webproject.com.config;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;
import org.springframework.boot.context.properties.ConfigurationProperties;

import java.util.List;

@ConfigurationProperties(prefix = "redisson")

@Data
@Accessors(chain = true)
@EqualsAndHashCode(callSuper = false)
public class RedissonProperties {

    private int timeout = 3000;

    private String address;

    private List<String> nodes;

    private String password;

    private int connectionPoolSize = 64;

    private int connectionMinimumIdleSize=10;

    private int slaveConnectionPoolSize = 250;

    private int masterConnectionPoolSize = 250;

    private String[] sentinelAddresses;

    private String masterName;
}
