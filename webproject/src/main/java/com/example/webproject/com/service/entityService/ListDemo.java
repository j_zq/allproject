package com.example.webproject.com.service.entityService;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.concurrent.TimeUnit;

/**
 * @author: jzq
 * @date: 2020/6/29 17:17
 * @description:
 */
public class ListDemo {
    static List<Integer> list= Collections.synchronizedList(new ArrayList<>());

    public static void main(String[] args) throws InterruptedException {
//        Runnable runnable = () -> {
//            for (int i = 0; i < 10000; i++) {
//                list.add(i);
//            }
//        };
//        Thread one = new Thread(runnable);
//        Thread two = new Thread(runnable);
//        one.start();
//        two.start();
//        one.join();
//        two.join();
//        System.out.println(list.size());
        test();
        TimeUnit.MILLISECONDS.sleep(3000);
        System.out.println(list.size());
    }
    public static void testThread(){
        for (int i = 0; i < 10000; i++) {
            list.add(i);
        }
    }
    public  static void   test(){
        for (int i = 0; i < 5; i++) {
            new Thread(ListDemo::testThread).start();
        }
    }
}