package com.example.webproject.com.mapper;

import com.baomidou.mybatisplus.core.conditions.Wrapper;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.example.webproject.com.entity.Goods;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;
import org.apache.ibatis.annotations.Update;

import java.util.List;

@Mapper
public interface GoodsMapper extends BaseMapper<Goods> {
    @Update("update goods  set num = #{num}  where  goodsId = #{goodsId} ")
    void     updateGoodsById(Integer num, Integer goodsId);
    @Select("select  *  from     goods  for update ")
    List<Goods> updateGoodsByIdForUpdate();
//    @Select("select  *  from     goods  ")
    List<Goods> testForUpdate(@Param("ew") Wrapper wrapper);
}
