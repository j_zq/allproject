package com.example.webproject.com.mapper;

import com.example.webproject.com.entity.User;
import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Select;
import org.apache.ibatis.annotations.Update;

import java.util.List;

@Mapper
public interface UserMapper {
    @Select("select * from  user ")
    List<User> findAll();
    @Select("select * from  user  where  id  = 1")
    User findOne();
    @Insert(" insert into user ( username, password,\n" +
            "      age)\n" +
            "    values ( #{username,jdbcType=VARCHAR}, #{password,jdbcType=VARCHAR},\n" +
            "      #{age,jdbcType=INTEGER})")
    int  addUser(User user);
    @Select("select * from  user  where  id  =  #{id,jdbcType=INTEGER} ")
    User findById(String id);

    /**
     * 通过teacher的id查询出所有对应的user
     */
    @Select("select * from  user  where  teacherId  =  #{teacherId,jdbcType=INTEGER} ")
    List<User> findUserByTeacherId(Integer teacherId);

    User  selectByPrimaryKey(Integer id);

    @Update("update `user` set username = 'jzq' where id = 1 ;update `user` set username = 'jzq2' where id = 1")
    int   updateUser();
}