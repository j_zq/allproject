package com.example.webproject.com.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.example.webproject.com.entity.Order;
import org.apache.ibatis.annotations.Mapper;

@Mapper
public interface OrderMapper extends BaseMapper<Order> {
}
