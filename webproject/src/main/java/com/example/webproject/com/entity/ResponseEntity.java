package com.example.webproject.com.entity;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

/**
 * @author : jzq
 * @date: 2020/3/25 11:28
 * @description: 返回结果处理类
 */
@Data
@Accessors(chain = true)
@EqualsAndHashCode(callSuper = false)
public class ResponseEntity {
    private boolean  statue;
    private   String msg;
    private   Object  value;

    public ResponseEntity(boolean statue, String msg, Object value) {
        this.statue = statue;
        this.msg = msg;
        this.value = value;
    }

    public ResponseEntity() {
    }
}