package com.example.webproject.com.controller.entityController;

import com.example.webproject.com.entity.UrlTest;
import com.example.webproject.com.service.entityService.GoodsServiceNew;
import io.swagger.annotations.Api;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/goods")
@CrossOrigin
@Api(tags = "goodsController", value = "goods管理")
public class GoodsController {
    @Autowired
    private GoodsServiceNew goodsServiceNewNew;
    @PostMapping(value = {"/add"})
    public void add( ) {

       goodsServiceNewNew.add();
    }
    @PostMapping(value = {"/urlTest"})
    public void urlTest(@RequestBody @Validated UrlTest urlTest) {
        System.out.println(urlTest);
    }
    @PostMapping(value = {"/updateFor"})
    public void update( ) {
        goodsServiceNewNew.updateFor();
    }
    @GetMapping("/getList")
    public  Object getList(){
      return   goodsServiceNewNew.getList();
    }
    //测试获取属性
    @GetMapping("/getParamValue/{str1}/{str2}")
    public  Object getParamValue(@PathVariable("str1") String  str2,@PathVariable("str2") String str1){
        return   str1+str2;
    }

}
