package com.example.webproject.com.task.intf;

import com.example.webproject.com.entity.task.ScheduledTaskBean;

import java.util.List;

public interface ScheduledTaskService {

    /**
     * 获取所有任务列表
     */
    List<ScheduledTaskBean> getList();

    /**
     * 启动任务
     */
    Boolean start(Integer taskId);

    /**
     * 停止任务
     */
    Boolean stop(Integer taskId);

    /**
     * 重启任务
     */
    Boolean restart(Integer taskId);


    /**
     * 程序启动时初始化启动所有正常状态的任务
     */
    void initAllTask(List<ScheduledTaskBean> scheduledTaskBeanList);

    /**
     * 获取项目启动是需要启动的任务
     * @return
     */
    List<ScheduledTaskBean> getOnScheduledTasks();
}
