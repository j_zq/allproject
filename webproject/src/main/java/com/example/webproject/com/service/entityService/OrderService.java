package com.example.webproject.com.service.entityService;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.example.webproject.com.entity.Order;
import com.example.webproject.com.mapper.OrderMapper;

public class OrderService  extends ServiceImpl<OrderMapper, Order> {
        public   void    saveOrder(Order order){
            super.save(order);
        }
}
