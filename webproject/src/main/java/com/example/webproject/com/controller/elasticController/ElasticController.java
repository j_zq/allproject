//package com.example.webproject.com.controller.elasticController;
//
//import com.example.webproject.com.entity.SysUser;
//import com.example.webproject.com.service.elasticService.UserRepository;
//import io.swagger.annotations.Api;
//import io.swagger.annotations.ApiOperation;
//import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.data.domain.Sort;
//import org.springframework.data.elasticsearch.core.ElasticsearchTemplate;
//import org.springframework.web.bind.annotation.PostMapping;
//import org.springframework.web.bind.annotation.RequestBody;
//import org.springframework.web.bind.annotation.RequestParam;
//import org.springframework.web.bind.annotation.RestController;
//
//import java.util.List;
//
//@RestController("/elasticTest")
//@Api(tags = "elasticController")
//public class ElasticController {
//    @Autowired
//    private ElasticsearchTemplate elasticsearchTemplate;
//    @Autowired
//    private UserRepository repository;
//    @PostMapping("/createIndex")
//    @ApiOperation("创建索引")
//    public Boolean createIndex(@RequestParam String indexName){
//        return  elasticsearchTemplate.createIndex(indexName);
//    }
//
//    @PostMapping("/createIndex")
//    @ApiOperation("创建索引")
//    public Boolean createIndex(){
//        return  elasticsearchTemplate.createIndex(SysUser.class);
//    }
//    @PostMapping("/deleteIndex")
//    @ApiOperation("删除索引")
//    public Boolean deleteIndex(@RequestParam String indexName){
//        // return elasticsearchTemplate.deleteIndex(SysUser.class);
//        return  elasticsearchTemplate.deleteIndex(indexName);
//    }
//    @PostMapping("/save")
//    @ApiOperation("新增")
//    public SysUser save(@RequestBody SysUser user){
//        return repository.save(user);
//    }
//
//    @PostMapping("/saveAll")
//    @ApiOperation("批量新增")
//    public Iterable<SysUser> saveAll(@RequestBody List<SysUser> users){
//        return  repository.saveAll(users);
//    }
//    @PostMapping("/findAllAndSort")
//    @ApiOperation("查询全部并根据密码排序")
//    public Iterable<SysUser> findAllAndSort(){
//        return repository.findAll(Sort.by("password").ascending());
//    }
//
//    @PostMapping("/findAll")
//    @ApiOperation("查询全部")
//    public Iterable<SysUser> findAll(){
//        return repository.findAll();
//    }
//}
