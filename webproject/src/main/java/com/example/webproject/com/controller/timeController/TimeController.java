package com.example.webproject.com.controller.timeController;

import io.swagger.annotations.Api;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import java.time.LocalDateTime;
/**
 * @author: jzq
 * @date: 2020/8/14 15:40
 * @description:
 */
@RestController
@RequestMapping("/time")
@Api(tags = "TimeController")
public class TimeController {
    @GetMapping("/test")
    public  Object  test(){
        System.out.println(LocalDateTime.now());
        return LocalDateTime.now();
    }
}