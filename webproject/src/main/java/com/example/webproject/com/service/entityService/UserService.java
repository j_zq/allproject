package com.example.webproject.com.service.entityService;

import com.example.webproject.com.entity.User;
import com.example.webproject.com.mapper.UserMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class UserService {
    @Autowired
    private UserMapper userMapper;
    public List<User> findAll(){
        return userMapper.findAll();
    }
    public User  findById(String id){
        User user=   userMapper.findById(id);
        System.out.println("user"+user);
        return  user;
    }
    public User  findOne(){
        return   userMapper.findOne();
    }
    public void   addUser(User user){
           userMapper.addUser(user);
    }

    public   User  selectByPrimaryKey(Integer id){
        return   userMapper.selectByPrimaryKey(id);
    }
    public   void   updateUser(){
        userMapper.updateUser();
    }


}
