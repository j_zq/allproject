package com.example.webproject.com.common;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import lombok.AllArgsConstructor;
import lombok.NoArgsConstructor;
import org.springframework.util.StringUtils;

import java.io.Serializable;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

@NoArgsConstructor
@AllArgsConstructor
public class QueryDate<T> implements Serializable {
    private static final long serialVersionUID = 1L;

    private long page;

    private long pageSize;

    private Boolean asc;

    private T object;

    private String startDate;

    private String endDate;


    public Page page(long total){
        return new Page<T>(this.page,this.pageSize,total);

    }

    public long getPage() {
        return page;
    }

    /**
     * xml 中分页使用
     * @return
     */
    public long getLimit(){
        if((this.page ) > 1)
            return (page - 1) * this.pageSize;
        return (this.page - 1);
    }

    public Date getStartDate() {
        if(StringUtils.isEmpty(this.startDate))
            return null;
        this.startDate = this.startDate.replace("Z", " UTC");
        SimpleDateFormat sdf1 = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS Z");
        try {
            return sdf1.parse(this.startDate);
        } catch (ParseException e) {
            return null;
        }

    }

    public void setStartDate(String startDate) {
        this.startDate = startDate;
    }

    public Date getEndDate() {
        if(StringUtils.isEmpty(this.endDate))
            return null;
        this.endDate = this.endDate.replace("Z", " UTC");
        SimpleDateFormat sdf1 = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS Z");
        try {
            Date date = sdf1.parse(this.endDate);
            if(date != null){
                Calendar cal = Calendar.getInstance();
                cal.setTime(date);
                cal.add(Calendar.DATE, 1);
                return cal.getTime();
            }
        } catch (ParseException e) {
            return null;
        }
        return null;
    }

    public void setEndDate(String endDate) {
        this.endDate = endDate;
    }

    public void setPage(long page) {
        this.page = page;
    }

    public long getPageSize() {
        return pageSize;
    }

    public void setPageSize(long pageSize) {
        this.pageSize = pageSize;
    }

    public Boolean getAsc() {
        return asc;
    }

    public void setAsc(Boolean asc) {
        this.asc = asc;
    }

    public T getObject() {
        return object;
    }

    public void setObject(T object) {
        this.object = object;
    }
}
