package com.example.webproject.com.controller.sentinelController;

import com.alibaba.csp.sentinel.annotation.SentinelResource;
import com.alibaba.csp.sentinel.slots.block.RuleConstant;
import com.alibaba.csp.sentinel.slots.block.flow.FlowRule;
import com.alibaba.csp.sentinel.slots.block.flow.FlowRuleManager;
import io.swagger.annotations.Api;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.ArrayList;
import java.util.List;

/**
 * @author: jzq
 * @date: 2020/8/21 10:11
 * @description:
 */
@RestController
@RequestMapping("/sentinel")
@Api(tags = "sentinelController")
public class SentinelController {
    private static Logger log = LoggerFactory.getLogger(SentinelController.class);
    private static void initFlowRules(){
        List<FlowRule> rules = new ArrayList<>();
        FlowRule rule = new FlowRule();
        rule.setResource("HelloWorld");
        rule.setGrade(RuleConstant.FLOW_GRADE_QPS);
        // Set limit QPS to 20.
        rule.setCount(20);
        rules.add(rule);
        FlowRuleManager.loadRules(rules);
    }
    @GetMapping("/HelloWorlds")
    public void helloWorlds() {
        log.info("HelloWorlds");
        // 资源中的逻辑
        System.out.println("hello world");
    }
    @SentinelResource("HelloWorld")
    @GetMapping("HelloWorld")
    public void helloWorld() {
        // 资源中的逻辑
        System.out.println("hello world");
    }
}