package com.example.webproject.com.util;

import com.alibaba.excel.ExcelWriter;
import com.alibaba.excel.metadata.Sheet;
import com.alibaba.excel.metadata.Table;
import com.alibaba.excel.support.ExcelTypeEnum;
import org.junit.Test;

import javax.servlet.http.HttpServletResponse;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.OutputStream;
import java.util.*;

/**
 * @author: jzq
 * @date: 2020/7/3 11:28
 * @description:   测试easyExcel
 */
public class Excel {
    //C:\Users\jzq\Desktop
    @Test
    public void  test() throws FileNotFoundException {
        String separator = File.separator;
        OutputStream out = new FileOutputStream("C:"+separator+"Users"+separator+"jzq"+separator+"withoutHead1.xlsx");
        ExcelWriter writer = new ExcelWriter(out, ExcelTypeEnum.XLSX);
        // 设置SHEET
        Sheet sheet = new Sheet(1, 0);
        sheet.setSheetName("sheet1");

        // 设置标题
        Table table = new Table(1);
        List<List<String>> titles = new ArrayList<List<String>>();
        titles.add(Collections.singletonList("订单编号"));
        titles.add(Collections.singletonList("下单时间"));
        titles.add(Collections.singletonList("订单状态"));
        titles.add(Collections.singletonList("订单金额"));
        titles.add(Collections.singletonList("订单来源"));
        titles.add(Collections.singletonList("订单类型"));
        titles.add(Collections.singletonList("评价状态"));
        titles.add(Collections.singletonList("订单实付金额"));

        //商品信息
        titles.add(Collections.singletonList("商品图"));
        titles.add(Collections.singletonList("商品名称"));
        titles.add(Collections.singletonList("商品数量"));
        titles.add(Collections.singletonList("单价"));
        titles.add(Collections.singletonList("优惠金额"));
        titles.add(Collections.singletonList("商品实付金额"));
        table.setHead(titles);
        //会员信息
        titles.add(Collections.singletonList("会员ID"));
        titles.add(Collections.singletonList("会员昵称"));
        titles.add(Collections.singletonList("会员来源"));
        titles.add(Collections.singletonList("会员名称"));
        titles.add(Collections.singletonList("会员类型"));
        titles.add(Collections.singletonList("联系电话"));
        //物流信息
        titles.add(Collections.singletonList("物流公司/配送公司"));
        titles.add(Collections.singletonList("配送仓库名称"));
        titles.add(Collections.singletonList("仓库编号"));
        titles.add(Collections.singletonList("收货人姓名"));
        titles.add(Collections.singletonList("收货地址"));
        titles.add(Collections.singletonList("物流单号/配送单号"));
        titles.add(Collections.singletonList("仓库区域"));
        titles.add(Collections.singletonList("发货地址"));
        titles.add(Collections.singletonList("收货人联系方式"));

        // 查询数据导出即可 比如说一次性总共查询出100条数据
        List<List<String>> userList = new ArrayList<>();
        for (int i = 0; i < 100; i++) {
            userList.add(Arrays.asList("ID_" + i, "小明" + i, String.valueOf(i),
                    new Date().toString()));
        }
        HttpServletResponse response;
        writer.write0(userList, sheet, table);

        writer.finish();

    }
    @Test
    public  void uploadExcel(){
        // 读取 excel 表格的路径

    }
}