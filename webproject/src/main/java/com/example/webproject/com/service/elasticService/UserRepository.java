//package com.example.webproject.com.service.elasticService;
//
//import com.example.webproject.com.entity.SysUser;
//import org.springframework.data.elasticsearch.repository.ElasticsearchRepository;
//
//import java.util.List;
//
///**
// * @ClassName ElasticSerarchRepository
// * @Description TODO
// * @Author gaoleijie
// * @Date 2019/7/18 10:15
// **/
//public interface UserRepository extends ElasticsearchRepository<SysUser,Long> {
//    /**
//     * 根据昵称查找用户
//     * @param nickName
//     * @return
//     */
//    List<SysUser> findByNickname(String nickName);
//
//    /**
//     * 根据昵称或者用户名进行查找
//     * @param nickName
//     * @param Password
//     * @return
//     */
//    List<SysUser> findByNicknameOrPassword(String nickName, String Password);
//}