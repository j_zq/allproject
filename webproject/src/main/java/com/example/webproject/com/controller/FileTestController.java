package com.example.webproject.com.controller;

import com.alibaba.excel.ExcelWriter;
import com.alibaba.excel.metadata.Sheet;
import com.alibaba.excel.support.ExcelTypeEnum;
import com.example.webproject.com.entity.Goods;
import com.example.webproject.com.entity.TestExcel;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.*;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.List;

/**
 * @author: jzq
 * @date: 2020/7/6 09:41
 * @description:
 */
@RestController
@RequestMapping("/file")
@Api(tags = "fileController")
public class FileTestController {
    @PostMapping("exportVehicleInfo")
    public void testUploadFile(HttpServletRequest req, HttpServletResponse resp) throws UnsupportedEncodingException {
        req.setCharacterEncoding("utf-8");
       /// String filename = req.getParameter("filename");
        DataInputStream in = null;
        OutputStream out = null;
        try{
            resp.reset();// 清空输出流
            String resultFileName = "剑南春订单明细数据" + ".xlsx";
            //resultFileName = URLEncoder.encode(resultFileName,"UTF-8");
            resp.setCharacterEncoding("UTF-8");
            resp.setHeader("Content-disposition", "attachment; filename=" + resultFileName);// 设定输出文件头
            resp.setContentType("application/msexcel");// 定义输出类型
            //输入流：本地文件路径
            in = new DataInputStream(
                    //new FileInputStream(new File("D:\\withoutHead1.xlsx")));
                    new FileInputStream(new File("D:\\打卡报表_3月.xlsx")));
            //输出流
            out = resp.getOutputStream();
            //输出文件
            int bytes = 0;

            byte[] bufferOut = new byte[1024];
            while ((bytes = in.read(bufferOut)) != -1) {
                out.write(bufferOut, 0, bytes);
            }
        } catch(Exception e){
            e.printStackTrace();
            resp.reset();
            try {
                OutputStreamWriter writer = new OutputStreamWriter(resp.getOutputStream(), "UTF-8");
                String data = "<script language='javascript'>alert(\"\\u64cd\\u4f5c\\u5f02\\u5e38\\uff01\");</script>";
                writer.write(data);
                writer.close();
            } catch (IOException e1) {
                e1.printStackTrace();
            }
        }finally {
            if(null != in) {
                try {
                    in.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
            if(null != out) {
                try {
                    out.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }


    }
    @GetMapping("test")
    @ApiOperation(value = "导出清单", notes = "export", produces = "application/octet-stream")
    public void  test(Goods goods, HttpServletRequest req, HttpServletResponse response) throws Exception {
       // req.setCharacterEncoding("utf-8");
       // response.setHeader("Content-Disposition", "attachment;fileName=" + fileName);

        response.setContentType("application/vnd.ms-excel; charset=utf-8");
        response.setCharacterEncoding("utf-8");
        String cFileName = URLEncoder.encode("订单信息", "utf-8");
        response.setHeader("Content-disposition", "attachment;filename="+cFileName+".xls");
        ExcelWriter writer = new ExcelWriter(response.getOutputStream(), ExcelTypeEnum.XLS,true);
        Sheet sheet = new Sheet(1,1, TestExcel.class);
        sheet.setSheetName("学生信息");
        List<TestExcel> datas = new ArrayList<>();
        TestExcel readModel1 = new TestExcel();
        TestExcel readModel2 = new TestExcel();
        readModel1.setOrderNo("张三");
        readModel1.setName("张三name");
        readModel2.setOrderNo("李四");
        readModel2.setName("李四name");
        datas.add(readModel1);
        datas.add(readModel2);
        writer.write(datas,sheet);
        writer.finish();
    }
}