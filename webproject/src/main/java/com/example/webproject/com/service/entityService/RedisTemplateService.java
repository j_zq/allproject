package com.example.webproject.com.service.entityService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.BoundHashOperations;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.stereotype.Service;

import java.util.concurrent.TimeUnit;

/**
 * @author: jzq
 * @date: 2020/7/17 16:20
 * @description:
 */
@Service
public class RedisTemplateService {
    @Autowired
    private RedisTemplate redisTemplate;
    public void  hashPut(String hashKey,String mapKey,String value){
        redisTemplate.boundHashOps(mapKey).put(mapKey,value);
    }
    public void  hashPutExpire(String hashKey,String mapKey,String value,Long timeOut,TimeUnit timeUnit){
        redisTemplate.boundHashOps(hashKey).put(mapKey,value);
        redisTemplate.expire(mapKey,timeOut,timeUnit);
    }
    public void  redisExpire(String hashKey,Long timeOut,TimeUnit timeUnit){
        redisTemplate.expire(hashKey,timeOut,timeUnit);
    }

    public  Object    hashGet(String hashKey,String mapKey){
        return  redisTemplate.boundHashOps(hashKey).get(mapKey);
    }
    public void  boundListAdd(String listKey,Object value){
        redisTemplate.boundListOps(listKey).leftPush(value);
    }
    public Object listGet(String listKey){
        return redisTemplate.boundListOps(listKey).getKey();
    }
    public void  valueAdd(String valueKey,Object value){
        redisTemplate.boundValueOps(valueKey).set(value);
    }
    public void  valueAdd(String valueKey,Object value,Long  time){
        redisTemplate.boundValueOps(valueKey).set(value,time, TimeUnit.SECONDS);
    }
    public Object  valueGet(String valueKey){
        //  redisTemplate.boundStreamOps();
        return redisTemplate.opsForValue().get(valueKey);
    }
    public Object  valueDelete(String hashKey,String mapKey){
        BoundHashOperations hash = redisTemplate.boundHashOps(hashKey);
        if(hash != null&&hash.hasKey(mapKey)){
            return hash.delete(mapKey);
        }
       return null;
    }
}