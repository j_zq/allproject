package com.example.webproject.com.util;

import com.example.webproject.com.entity.Goods;
import com.example.webproject.com.entity.TestGoods;
import org.junit.Test;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 * @author: jzq
 * @date: 2020/7/14 14:27
 * @description:
 */
public class utilTest {

    @Test
    public void test(){
        MyCommonToolUtils<Goods> util = new MyCommonToolUtils<>();
        Goods goods = new Goods();
        goods.setId(1).setNum(1).setNo(1).setPrice(1.1).setUrl("1");
        Goods o =util.nullToEmpty(goods);
        System.out.println(o);
        Goods good = new Goods();
        good.setId(1).setNum(1).setNo(1).setPrice(1.1);
        good = util.nullToEmpty(good);
        System.out.println(good);
    }


    @Test
    public void test1(){
        String  str="123f1231";//正则
        boolean b = str.matches("^1\\w+31$");
        System.out.println(b);

    }
    @Test
    public void testString(){
        List<Goods> list = new ArrayList<>();
        Goods goods = new Goods();
        goods.setId(1);
        list.add(goods);
        Goods good2 = new Goods();
        good2.setId(2);
        list.add(good2);
        Goods good3 = new Goods();
        good3.setId(1);
        list.add(good3);
        Goods good4 = new Goods();
        good4.setId(4);
        list.add(good4);
        MyCommonToolUtils<Goods> util= new MyCommonToolUtils<>();
        Map<Object, Integer> id = util.listStatistical(list, "id");
        System.out.println(id);
    }
    @Test
    public void testGodds(){
        List<TestGoods> list = new ArrayList<>();
        TestGoods goods = new TestGoods();
        goods.setId(1);
        list.add(goods);
        TestGoods good2 = new TestGoods();
        good2.setId(2);
        list.add(good2);
        TestGoods good3 = new TestGoods();
        good3.setId(1);
        list.add(good3);
        TestGoods good4 = new TestGoods();
        good4.setId(4);
        list.add(good4);
        MyCommonToolUtils<TestGoods> util= new MyCommonToolUtils<>();
        Map<Object, Integer> id = util.listStatistical(list, "id");
        System.out.println(id);
    }

}