package com.example.webproject.com.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.example.webproject.com.entity.task.ScheduledTaskBean;
import org.apache.ibatis.annotations.Mapper;

@Mapper
public interface ScheduledTaskMapper extends BaseMapper<ScheduledTaskBean> {
}
