package com.example.webproject.com.entity;

import com.alibaba.excel.annotation.ExcelProperty;
import com.alibaba.excel.metadata.BaseRowModel;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

/**
 * @author: jzq
 * @date: 2020/7/6 11:10
 * @description: Excel测试工具类
 */
@Data
@Accessors(chain = true)
@EqualsAndHashCode(callSuper = false)
public class TestExcel extends BaseRowModel {
    @ExcelProperty(value = "订单编号",index = 0)
    private String orderNo;
    @ExcelProperty(value = "姓名",index = 1)
    private String name;
}