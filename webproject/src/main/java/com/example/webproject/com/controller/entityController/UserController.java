package com.example.webproject.com.controller.entityController;

import com.example.webproject.com.entity.User;
import com.example.webproject.com.service.entityService.UserService;
import io.swagger.annotations.Api;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;

import java.util.List;

@RestController
@RequestMapping("/user")
@Api("user")
public class UserController {
    @Autowired
    private UserService userService;
    @GetMapping(value = {"/test"})
    public String hello(){
        return "/HelloWord";
    }
    @GetMapping("/getView")
    public ModelAndView getView(){
        System.out.println("getView执行啦!");
        ModelAndView view = new ModelAndView("/HelloWord.html");;
        return view;
    }
    @GetMapping("/findAll")
    public List<User> findAll(){

        return userService.findAll();
    }
    @GetMapping("/findById/{id}")
    public User  selectAll(@PathVariable("id") String id){
        System.out.println("id:"+id);
        return  userService.findById(id);
    }
    @GetMapping("/findOne")
    public User  findOne(){
        return   userService.findOne();
    }
    @GetMapping("/addUser")
    public void  addUser(){
        User user = new User();
        user.setAge(1111);
        user.setUsername("校长");
        user.setPassword("123456");
        userService.addUser(user);
    }
    @GetMapping("/selectByPrimaryKey/{id}")
    public   User  selectByPrimaryKey(@PathVariable("id") Integer id){

        return   userService.selectByPrimaryKey(id);
    }

    @GetMapping("/updateUser")
    public   void   updateUser(){

           userService.updateUser();
    }
    @PostMapping("/testUserVo")
    public   void    testUserVo(@RequestBody User  user){
    }

}
