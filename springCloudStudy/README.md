# springCloudStudy

#### 介绍
筑梦师博客，springCloud项目学习源码，包含服务治理，服务发现，网关路由，微服务监控，日志上传，微服务链路跟踪等等

#### 软件架构
springBoot+springCloud


#### 安装教程

1. clone项目代码
2. 不同分支代表不同的文章源码
3. master分支是完整的

#### 使用说明
所有其他分支的代码最终都会合并到master分支
共同学习，共同成长
