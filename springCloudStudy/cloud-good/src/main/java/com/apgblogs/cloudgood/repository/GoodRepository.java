package com.apgblogs.cloudgood.repository;

import com.apgblogs.cloudgood.entity.CGoodEntity;
import org.springframework.data.jpa.repository.JpaRepository;

/**
 * @author xiaomianyang
 * @description
 * @date 2019-07-04 12:05
 */
public interface GoodRepository extends JpaRepository<CGoodEntity,String> {
}
