package com.apgblogs.cloudgood;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.data.jpa.repository.config.EnableJpaAuditing;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

/**
 * @author xiaomianyang
 * @description
 * @date 2019-07-04 11:57
 */
@SpringBootApplication
@EnableJpaAuditing
@EnableSwagger2
public class GoodApplication {
    public static void main(String[] args) {
        SpringApplication.run(GoodApplication.class,args);
    }
}
