package com.apgblogs.cloudgood.controller;

import com.apgblogs.cloudgood.entity.CGoodEntity;
import com.apgblogs.cloudgood.service.GoodServiceImpl;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

/**
 * @author xiaomianyang
 * @description
 * @date 2019-07-04 12:08
 */
@RestController
@RequestMapping("good")
@Api(tags = "goodController")
public class GoodController {
    
    @Autowired
    private GoodServiceImpl goodService;
    
    /**
     * @description 查询商品
     * @author xiaomianyang
     * @date 2019-07-04 12:16
     * @return com.apgblogs.cloudgood.entity.CGoodEntity
     */
    @GetMapping("{id}")
    @ApiOperation(value = "根据id查询数据")
    public CGoodEntity getGood(@PathVariable String id){
        return goodService.getGood(id);
    }
    
    /**
     * @description 保存商品
     * @author xiaomianyang
     * @date 2019-07-04 12:11
     * @return com.apgblogs.cloudgood.entity.CGoodEntity
     */
    @PostMapping
    public CGoodEntity saveGood(@RequestBody CGoodEntity cGoodEntity){
        return goodService.saveGood(cGoodEntity);
    }
}
