package com.apgblogs.clouduser.controller;

import com.apgblogs.clouduser.entity.UserEntity;
import com.apgblogs.clouduser.service.UserServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

/**
 * @author xiaomianyang
 * @description
 * @date 2019-07-02 20:13
 */
@RestController
public class UserController {
    
    @Autowired
    private UserServiceImpl userService;
    
    /**
     * @description 获取用户
     * @author xiaomianyang
     * @date 2019-07-02 20:17
     * @param id   id
     * @return com.apgblogs.clouduser.entity.UserEntity
     */
    @GetMapping("{id}")
    public UserEntity getUser(@PathVariable String id){
        return userService.getUser(id);
    }
    
    /**
     * @description 保存用户
     * @author xiaomianyang
     * @date 2019-07-02 20:20
     * @param userEntity  用户实体
     * @return com.apgblogs.clouduser.entity.UserEntity
     */
    @PostMapping
    public UserEntity saveUser(@RequestBody UserEntity userEntity){
        return userService.saveUser(userEntity);
    }
}
