package com.apgblogs.clouduser.repository;

import com.apgblogs.clouduser.entity.UserEntity;
import org.springframework.data.jpa.repository.JpaRepository;

/**
 * @author xiaomianyang
 * @description
 * @date 2019-07-02 20:12
 */
public interface UserRepository extends JpaRepository<UserEntity,String> {
}
