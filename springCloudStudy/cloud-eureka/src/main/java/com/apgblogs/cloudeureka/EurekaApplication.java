package com.apgblogs.cloudeureka;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.netflix.eureka.server.EnableEurekaServer;

/**
 * @author xiaomianyang
 * @description
 * @date 2019-06-28 16:30
 */
@SpringBootApplication
@EnableEurekaServer   ///EurekaServerAutoConfiguration   开启这个bean的注入   //ApplicationResource 发布注册信息
//@EnableBinding(value = KafkaService.class)
public class EurekaApplication {

    public static void main(String[] args) {
        SpringApplication.run(EurekaApplication.class, args);
    }

}