package com.apgblogs.usercenter.service;

import com.apgblogs.usercenter.dto.UserDto;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;

/**
 * @author xiaomianyang
 * @description
 * @date 2019-07-20 15:26
 */
@FeignClient(name="cloud-user")
public interface UserService {

    @GetMapping("{id}")
    UserDto getUser(@PathVariable String id);
}
