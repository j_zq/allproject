package com.apgblogs.usercenter.controller;

import com.apgblogs.usercenter.dto.OrderDto;
import com.apgblogs.usercenter.dto.UserDto;
import com.apgblogs.usercenter.service.OrderService;
import com.apgblogs.usercenter.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @author xiaomianyang
 * @description
 * @date 2019-07-20 12:07
 */
@RestController
public class UserCenterController {

    @Autowired
    private OrderService orderService;

    @Autowired
    private UserService userService;

    /**
     * @description 获取当前用户的订单列表
     * @author xiaomianyang
     * @date 2019-07-19 13:05
     * @param []
     * @return java.util.List<com.apgblogs.clouduser.dto.OrderDto>
     */
    @GetMapping("userOrderList/{userId}")
    public Object getUserOrderList(@PathVariable String userId){
        Map<String,Object> resultMap=new HashMap<>();
        List<OrderDto> orderDtoList=orderService.getUserOrderList(userId);
        UserDto userDto=userService.getUser(userId);
        resultMap.put("user",userDto);
        resultMap.put("orderList",orderDtoList);
        return resultMap;
    }
}
