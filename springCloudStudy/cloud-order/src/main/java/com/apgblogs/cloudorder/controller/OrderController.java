package com.apgblogs.cloudorder.controller;

import com.apgblogs.cloudorder.entity.COrderEntity;
import com.apgblogs.cloudorder.service.OrderServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * @author xiaomianyang
 * @description
 * @date 2019-07-03 12:33
 */
@RestController
public class OrderController {
    
    @Autowired
    private OrderServiceImpl orderService;
    
    /**
     * @description 获取订单
     * @author xiaomianyang
     * @date 2019-07-03 12:34
     * @return com.apgblogs.cloudorder.entity.COrderEntity
     */
    @GetMapping("{id}")
    public COrderEntity getOrder(@PathVariable String id){
        return orderService.getOrder(id);
    }
    
    /**
     * @description 新增订单
     * @author xiaomianyang
     * @date 2019-07-03 13:09
     * @return com.apgblogs.cloudorder.entity.COrderEntity
     */
    @PostMapping
    public COrderEntity saveOrder(@RequestBody COrderEntity COrderEntity){
        return orderService.saveOrder(COrderEntity);
    }


    /**
     * @description 通过用户Id查询用户订单列表
     * @author xiaomianyang
     * @date 2019-07-19 12:09
     * @return java.util.List<com.apgblogs.cloudorder.entity.COrderEntity>
     */
    @GetMapping("userOrderList/{userId}")
    public List<COrderEntity> getOrderListByUser(@PathVariable String userId){
        return orderService.getOrderListByUser(userId);
    }
}
