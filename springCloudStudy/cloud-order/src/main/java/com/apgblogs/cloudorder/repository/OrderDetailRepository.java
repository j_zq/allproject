package com.apgblogs.cloudorder.repository;

import com.apgblogs.cloudorder.entity.COrderDetailEntity;
import org.springframework.data.jpa.repository.JpaRepository;

/**
 * @author xiaomianyang
 * @description
 * @date 2019-07-03 11:38
 */
public interface OrderDetailRepository extends JpaRepository<COrderDetailEntity,String> {
}
