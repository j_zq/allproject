package com.example.springdemo.ofpsearch.dao.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.example.springdemo.ofpsearch.dao.entity.ManualConfigItems;
import org.apache.ibatis.annotations.Param;

import java.util.List;

public interface ManualConfigItemsMapper extends BaseMapper<ManualConfigItems> {
    void batchInsert(@Param("items") List<ManualConfigItems> items);


}
