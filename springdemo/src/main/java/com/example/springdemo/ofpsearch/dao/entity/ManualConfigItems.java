package com.example.springdemo.ofpsearch.dao.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Date;

@Data
@AllArgsConstructor
@NoArgsConstructor
@TableName("manual_config_items")
public class ManualConfigItems {

    @TableId(value = "id", type = IdType.AUTO)
    private Long id;

    /**
     * boost  pointTo  bury hide
     **/
    private String operation;

    /**
     * operation value
     **/
    private String value;

    /**
     * 产品编号
     **/
    private String productCode;
    private Long manualConfigId;

    /**
     * 版本号
     */
    private  Integer version;

    /**
     * 修改时间
     */
    private Date updateTime;
    /**
     * id字符串
     */
    @TableField(exist = false)
    private String  idStr;

}
