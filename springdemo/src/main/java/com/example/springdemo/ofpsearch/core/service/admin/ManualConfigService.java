package com.example.springdemo.ofpsearch.core.service.admin;


import com.example.springdemo.ofpsearch.common.PageDto;
import com.example.springdemo.ofpsearch.core.vo.sortconfig.ManualConfigVo;
import com.example.springdemo.ofpsearch.dao.entity.ManualConfig;
import com.example.springdemo.ofpsearch.dao.entity.ManualConfigItems;
import com.example.springdemo.ofpsearch.dao.entity.SortConfigQueryVo;
import com.example.springdemo.ofpsearch.dao.entity.SortConfiguration;

import java.util.List;

public interface ManualConfigService {
    boolean insert(ManualConfigVo manualConfigVo);

    /**
     * 更新 manualConfig
     */
    boolean update(ManualConfigVo manualConfigVo);

    /**
     *
     * 删除
     */
    boolean delete(ManualConfigVo manualConfigVo);


    /**
     * 查询站点下的ManualConfig列表(包含items)
     * @param geoId 站点id  可以为空
     */
    List<ManualConfig> queryManualConfigListByGeoId(String geoId, String pageKeyId);

    /**
     * 根据产品查询对应的设置规则
     */
    ManualConfigItems queryManualConfig(String geoId, String testKeyId, List<String> productCodes);

    PageDto getList(ManualConfigVo vo);

    List<ManualConfigItems> getManualItemsList(String manualConfigId);

    PageDto<SortConfiguration> getAllList(SortConfigQueryVo sortConfigQueryVo);
}
