package com.example.springdemo.ofpsearch.dao.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.example.springdemo.ofpsearch.dao.entity.ManualConfig;
import com.example.springdemo.ofpsearch.dao.entity.ManualConfigItems;
import com.example.springdemo.ofpsearch.dao.entity.SortConfiguration;
import org.apache.ibatis.annotations.Param;

import java.util.List;

public interface ManualConfigMapper extends BaseMapper<ManualConfig> {
    /**
     * 查询对应产品的规则
     */
    ManualConfigItems queryManualConfig(String geoId, String testKeyId, List<String > productCodes);

    List<ManualConfig> queryManualConfigListByGeoId(String geoId ,String  pageKeyId);
    Page<SortConfiguration> getSortConfigList(@Param("pageKeyName") String pageKeyName,@Param("keyId") Long keyId,@Param("stores") List<String> stores,Page page);
//    Page<SortConfiguration> getSortConfigList(@Param("stores") List<String> stores,Page page);

}
