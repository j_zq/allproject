package com.example.springdemo.ofpsearch.core.service.admin.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.example.springdemo.ofpsearch.common.PageDto;
import com.example.springdemo.ofpsearch.core.DateUtil;
import com.example.springdemo.ofpsearch.core.service.admin.ManualConfigService;
import com.example.springdemo.ofpsearch.core.vo.sortconfig.ManualConfigVo;
import com.example.springdemo.ofpsearch.dao.entity.ManualConfig;
import com.example.springdemo.ofpsearch.dao.entity.ManualConfigItems;
import com.example.springdemo.ofpsearch.dao.entity.SortConfigQueryVo;
import com.example.springdemo.ofpsearch.dao.entity.SortConfiguration;
import com.example.springdemo.ofpsearch.dao.mapper.ManualConfigItemsMapper;
import com.example.springdemo.ofpsearch.dao.mapper.ManualConfigMapper;
import com.google.common.collect.ImmutableList;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.StringUtils;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

@Service
@Slf4j
public class ManualConfigServiceImpl implements ManualConfigService {
    @Autowired
    private ManualConfigMapper manualConfigMapper;
    @Autowired
    private ManualConfigItemsMapper manualConfigItemsMapper;

    @Override
    @Transactional
    public boolean insert(ManualConfigVo manualConfigVo) {
        //fps 35129
        ManualConfig manualConfig = new ManualConfig();
        BeanUtils.copyProperties(manualConfigVo, manualConfig);
        manualConfig.setCreateTime(DateUtil.getNow());
        manualConfig.setStatus(0);
        int insert = manualConfigMapper.insert(manualConfig);
        if (manualConfig.getId() != null && manualConfig.getId() != 0L) {
            List<ManualConfigItems> items = manualConfigVo.getManualConfigItems();
            items = items.stream().peek(item -> {
                item.setUpdateTime(DateUtil.getNow());
                item.setManualConfigId(manualConfig.getId());
            }).collect(Collectors.toList());
            //items 是否需要处理
            manualConfigItemsMapper.batchInsert(items);
        }

        return true;
    }

    @Override
    @Transactional
    public boolean update(ManualConfigVo manualConfigVo) {
        if (StringUtils.isEmpty(manualConfigVo.getId())) {
            return false;
        }
        ManualConfig config = new ManualConfig();
        BeanUtils.copyProperties(manualConfigVo, config);
        config.setId(Long.parseLong(manualConfigVo.getId()));
        config.setUpdateTime(DateUtil.getNow());

        List<ManualConfigItems> items = manualConfigVo.getManualConfigItems();
        manualConfigItemsMapper.delete(new QueryWrapper<ManualConfigItems>().eq("manual_config_id", config.getId()));
        if (items.size() > 0) {
            //items 是否需要处理  版本
            items = items.stream().peek(item -> {
                item.setUpdateTime(DateUtil.getNow());
                item.setManualConfigId(config.getId());
            }).collect(Collectors.toList());
            manualConfigItemsMapper.batchInsert(items);
        }
        //查询出已有的items    //或者先删除或者全部新增
//        QueryWrapper<ManualConfigItems> itemsWrapper = new QueryWrapper<>();
//        itemsWrapper = itemsWrapper.select(
//                        "id","operation","value","product_code","manual_config_id")
//                .eq("manual_config_id", config.getId());
//        List<String> oldItems = manualConfigItemsMapper.selectList(itemsWrapper).stream().map(item -> String.valueOf(item.getId())).collect(Collectors.toList());
//        if (!CollectionUtils.isEmpty(items)) {
//            if (!CollectionUtils.isEmpty(oldItems)) {
//                //不被移除的  再次判断是否需要修改  ------后续  取交集
//                List<ManualConfigItems> notRemoveItems = items.stream().filter(item -> oldItems.contains(item.getIdStr())).collect(Collectors.toList());
//                if (notRemoveItems.size() > 0) {
//                    //不被移除的id
//                    List<String> notRemoveItemsIds = notRemoveItems.stream().map(ManualConfigItems::getIdStr).collect(Collectors.toList());
//                    oldItems.removeAll(notRemoveItemsIds);
//                    if (oldItems.size() > 0) {
//                        manualConfigItemsMapper.deleteBatchIds(oldItems);
//                    }
//                    //items 代表需要新增的
//                    items.removeAll(notRemoveItems);
//                }else {
//                    manualConfigItemsMapper.delete(new QueryWrapper<ManualConfigItems>().eq("manual_config_id", config.getId()));
//                }
//            }
//            if (items.size() > 0) {
//                //新增items
//                items = items.stream().peek(item -> {
//                    item.setUpdateTime(DateUtil.getNow());
//                    item.setManualConfigId(config.getId());
//                }).collect(Collectors.toList());
//                //items 是否需要处理
//                manualConfigItemsMapper.batchInsert(items);
//            }
//        } else {
//            if (oldItems.size() > 0) {
//                manualConfigItemsMapper.delete(new QueryWrapper<ManualConfigItems>().eq("manual_config_id", config.getId()));
//            }
//        }

        return manualConfigMapper.updateById(config) > 0;
    }

    public boolean delete(ManualConfigVo manualConfigVo) {
        manualConfigItemsMapper.delete(new QueryWrapper<ManualConfigItems>().eq("manual_config_id", manualConfigVo.getId()));
        return manualConfigMapper.deleteById(manualConfigVo.getId()) > 1;
    }

    @Override
    public List<ManualConfig> queryManualConfigListByGeoId(String geoId, String pageKeyId) {

        return manualConfigMapper.queryManualConfigListByGeoId(geoId, pageKeyId);
    }

    @Override
    public ManualConfigItems queryManualConfig(String geoId, String testKeyId, List<String> productCodes) {
        //后面加缓存
        return manualConfigMapper.queryManualConfig(geoId, testKeyId, productCodes);
    }

    @Override
    public PageDto getList(ManualConfigVo vo) {
        return PageDto.toPage(manualConfigMapper.selectPage(new Page<>(vo.getPage(), vo.getPageSize()),
                Wrappers.<ManualConfig>lambdaQuery()
                        .in(ManualConfig::getGeoId, ImmutableList.of("2", "3", "4", "5"))
                        .orderByDesc(ManualConfig::getUpdateTime)
        ));
    }

    @Override
    public List<ManualConfigItems> getManualItemsList(String manualConfigId) {

        return manualConfigItemsMapper.selectList( Wrappers.<ManualConfigItems>lambdaQuery()
                .eq(ManualConfigItems::getManualConfigId,manualConfigId));
    }

    @Override
    public PageDto<SortConfiguration> getAllList(SortConfigQueryVo sortConfigQueryVo) {
        List<String> stores= new ArrayList<>();
        stores.add("2");
        stores.add("0e49db5e-0834-41ba-84fe-f796497ba9fc");
        Page page =new Page<>(1,3);
//        return PageDto.toPage(manualConfigMapper.getSortConfigList(stores,page));
        return PageDto.toPage(manualConfigMapper.getSortConfigList( sortConfigQueryVo.getPageKeyName(), sortConfigQueryVo.getKeyId(),stores,page));
    }
}
