package com.example.springdemo.ofpsearch.config;

import com.baomidou.mybatisplus.core.parser.ISqlParser;
import com.baomidou.mybatisplus.extension.parsers.BlockAttackSqlParser;
import com.baomidou.mybatisplus.extension.plugins.PaginationInterceptor;
import com.baomidou.mybatisplus.extension.plugins.SqlExplainInterceptor;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import java.util.ArrayList;

//
//@EnableTransactionManagement(proxyTargetClass = true)
@Configuration
public class MybatisPlusConfig {
    /**
     * mybatis-plus SQL执行效率插件【生产环境可以关闭】
     */
    @Bean
    public PaginationInterceptor performanceInterceptor() {
        return new PaginationInterceptor();
    }


    @Bean
    public SqlExplainInterceptor sqlExplainInterceptor() {
        //sql 执行分析插件   禁止全表更新和全表删除
        SqlExplainInterceptor interceptor = new SqlExplainInterceptor();
        ArrayList<ISqlParser> list = new ArrayList<>();
        list.add(new BlockAttackSqlParser());
        interceptor.setSqlParserList(list);
        return interceptor;
    }
}
