package com.example.springdemo.ofpsearch.core.vo.sortconfig;

import lombok.Data;
import com.example.springdemo.ofpsearch.dao.entity.ManualConfigItems;

import java.util.List;

@Data
public class ManualConfigVo {
    private String id;
    private String terminal;
    private String pageType;
    private String pageKeyType;
    private String pageKeyName;
    private String pageKeyId;
    private String testKey;
    private String testKeyId;
    private String geoId;
    private Integer  page;
    private  Integer pageSize;
    List<ManualConfigItems> manualConfigItems;
}
