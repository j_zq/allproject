package com.example.springdemo.ofpsearch.dao.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Date;
import java.util.List;

@Data
@AllArgsConstructor
@NoArgsConstructor
@TableName("manual_config")
public class ManualConfig {
    @TableId(value = "id", type = IdType.AUTO)
    private Long id;
    /**
     * 终端类型  1:pc  2:mobile  3:tablet
     * Terminal  枚举
     */
    private String terminal;

    /**
     * page类型
     * PageType  枚举
     */
    private String  pageType;
    /**
     * page filter key type
     */
    private String pageKeyType;
    /**
     * page filter key name
     */
    private String pageKeyName;

    /**
     * page filter id
     */
    private String pageKeyId;
    private String testKey;
    private String testKeyId;
    /**
     * storeId
     **/
    private String geoId;
    /**
     * 是否启用  0:启用 1:启用
     */
    private Integer status;
    /**
     * 创建时间
     */
    private Date createTime;

    /**
     * 修改时间
     */
    private Date updateTime;
    /**
     * 修改人
     */
    private String  updateBy;

    /**
     * 创建人
     */
    private String createBy;

    /**
     * 版本
     */
    private Integer  version;
    /**
     * 配置类型
     */
    private  String  configType;
    /**
     * 继承父类
     */
    private  String  inheritedFrom;

    @TableField(exist = false)
    List<ManualConfigItems> manualConfigItems;
}
