package com.example.springdemo.ofpsearch.dao.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Date;

@Data
@AllArgsConstructor
@NoArgsConstructor
@TableName("sort_configuration")
public class SortConfiguration {

    @TableId(value = "id", type = IdType.AUTO)
    private Long id;

    /**
     * 终端类型，Mobile=手机端 Desktop=台式端 Tablet=平板端
     */
    private String terminal;

    /**
     * page类型
     */
    private String pageType;

    /**
     * page filter key type
     */
    private String pageKeyType;

    /**
     * geoId
     */
    private String geoId;

    /**
     * 是否开启 1:enable，0:disable
     */
    private Byte status;

    /**
     * 是否可以更新 1:是，0:否
     */
    private Byte canUpdate;

    /**
     * page filter key name
     */
    private String pageKeyName;

    /**
     * page filter id
     */
    private String pageKeyId;

    /**
     * 继承的父级
     */
    private String inheritedFrom;

    /**
     * 添加时间
     */
    private Date createTime;

    /**
     * 修改时间
     */
    private Date updateTime;

    /**
     * 修改人
     */
    private String updateBy;

    /**
     * 创建人
     */
    private String createBy;
    /**
     * 配置类型
     */
    private  String  configType;

}
