package com.example.springdemo.ofpsearch.dao.entity;

import lombok.Data;

import javax.validation.constraints.NotNull;

@Data
public class SortConfigQueryVo {
    /**
     * keyId
     */
    private Long keyId;

    @NotNull(message = "geo id not be empty.")
    private String geoId;

    /**
     * page filter key name
     */
    private String pageKeyName;

    private Integer page = 1;

    private Integer pageSize = 10;

}