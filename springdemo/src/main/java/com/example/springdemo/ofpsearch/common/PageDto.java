package com.example.springdemo.ofpsearch.common;

import com.baomidou.mybatisplus.core.toolkit.CollectionUtils;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import lombok.Getter;
import lombok.Setter;

import java.util.ArrayList;
import java.util.List;

@Setter
@Getter
public class PageDto<T> {
    private Long count;

    private Long page;

    private List<T> data;

    private Long pageSize;

    public static <T> PageDto<T> toPage(Page<T> page){
        PageDto<T> pageDto = new PageDto<>();
        pageDto.setCount(page.getTotal());
        pageDto.setPage(page.getCurrent());
        pageDto.setPageSize(page.getSize());
        pageDto.setData(page.getRecords());
        return pageDto;
    }
    public  static<T,R> PageDto<R> toPage(Page<T> page, ObjectConverter<T,R> oc){
        PageDto<R> pageDto = new PageDto<>();
        pageDto.setCount(page.getTotal());
        pageDto.setPage(page.getCurrent());
        pageDto.setPageSize(page.getSize());
        List<T> dataU = page.getRecords();
        if(CollectionUtils.isNotEmpty(dataU)){
            List<R> dataRList = new ArrayList<>();
            dataU.forEach(t->{
                R rd = oc.converter(t);
                dataRList.add(rd);
            });
            pageDto.setData(dataRList);
        }else {
            pageDto.setData(new ArrayList<>());
        }
        return pageDto;
    }
}
