package com.example.springdemo.ofpsearch.admin.controller;

import com.example.springdemo.ofpsearch.core.service.admin.ManualConfigService;
import com.example.springdemo.ofpsearch.core.vo.sortconfig.ManualConfigVo;
import com.example.springdemo.ofpsearch.dao.entity.SortConfigQueryVo;
import io.swagger.annotations.Api;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

@Slf4j
@RestController
@RequestMapping("/adminApi/v1/sort/manual")
@Api(tags = "SortManualConfigController")
public class SortManualConfigController {
    @Autowired
    ManualConfigService manualConfigService;

    @PostMapping("/saveOrUpdate")
    public Object saveOrUpdate(@Valid @RequestBody ManualConfigVo manualConfigVo) {
        if (StringUtils.isEmpty(manualConfigVo.getId())) {
          return   manualConfigService.insert(manualConfigVo);
        }
        return  manualConfigService.update(manualConfigVo);
    }

    @GetMapping("list")
    public Object list(@RequestParam("geoId") String geoId, @RequestParam("pageKeyId") String pageKeyId) {
        return manualConfigService.queryManualConfigListByGeoId(geoId, pageKeyId);
    }
    @PostMapping("/delete")
    public Object delete(@Valid @RequestBody ManualConfigVo manualConfigVo) {
        return manualConfigService.delete(manualConfigVo);
    }
    @PostMapping("/getManualList")
    public Object getManualList(@RequestBody @Valid ManualConfigVo manualConfigVo) {
        return manualConfigService.getList(manualConfigVo);
    }


    @GetMapping("/getManualItemsList")
    public Object list(String  manualConfigId) {
        return manualConfigService.getManualItemsList(manualConfigId);
    }
    @GetMapping("/test")
    public Object list() {
        return "ok";
    }

    @GetMapping("/getList")
    public Object getAllList(SortConfigQueryVo sortConfigQueryVo) {

        return manualConfigService.getAllList(sortConfigQueryVo);
    }


}
