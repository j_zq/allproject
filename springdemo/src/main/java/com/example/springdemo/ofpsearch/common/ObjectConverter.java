package com.example.springdemo.ofpsearch.common;

/**
 * @author kangsj1
 * @version 1.0
 * @since 2020-07-15
 */
@FunctionalInterface
public interface ObjectConverter<T,R> {

    public R converter(T t);

}

