package com.example.springdemo.config;

import org.springframework.context.annotation.ComponentScan;

@ComponentScan("com.example")
public class AppConfig {
}
