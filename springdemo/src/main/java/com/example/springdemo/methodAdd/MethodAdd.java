package com.example.springdemo.methodAdd;

import com.example.springdemo.service.TwoService;
import org.springframework.beans.BeansException;
import org.springframework.beans.factory.config.BeanFactoryPostProcessor;
import org.springframework.beans.factory.config.ConfigurableListableBeanFactory;
import org.springframework.beans.factory.support.GenericBeanDefinition;
//对方法进行增强      在bean装载过程中 更改   oneService bean名字对应的  springBean
//@Component
public class MethodAdd  implements BeanFactoryPostProcessor {
    @Override
    public void postProcessBeanFactory(ConfigurableListableBeanFactory configurableListableBeanFactory) throws BeansException {
        GenericBeanDefinition oneService = (GenericBeanDefinition)configurableListableBeanFactory
                .getBeanDefinition("oneService");
        oneService.setBeanClass(TwoService.class);
    }
}
