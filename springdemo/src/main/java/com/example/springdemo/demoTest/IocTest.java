package com.example.springdemo.demoTest;

import com.example.springdemo.config.AppConfig;
import com.example.springdemo.service.TwoService;
import org.springframework.beans.factory.support.AbstractBeanFactory;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;

public class IocTest {
    public static void main(String[] args) {
        AnnotationConfigApplicationContext ac=new AnnotationConfigApplicationContext(AppConfig.class);
        AbstractBeanFactory factory = (AbstractBeanFactory) ac.getBeanFactory();
        TwoService one = (TwoService)ac.getBean("oneService");
        System.out.println(one);
    }
}
