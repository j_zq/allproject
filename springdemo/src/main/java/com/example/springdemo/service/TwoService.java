package com.example.springdemo.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class TwoService {
    @Autowired
    private  OneService oneService;
    public TwoService() {
        System.out.println("TwoService 执行啦！！！");
    }
    public  void   testTwo(){
        System.out.println("TwoService 执行啦！！！");
    }
}
