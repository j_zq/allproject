package com.example.springdemo.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
//TODO    spring在AbstractApplicationContext#finishBeanFactoryInitialization方法中完成了bean的实例化。
@Component
public class OneService {
    @Autowired
    private  TwoService twoService;
    public OneService() {
        System.out.println("OneService 执行啦！！！");
    }
    //生命周期初始化回调方法
    @PostConstruct
    public void zinit(){
        System.out.println("call z lifecycle init callback");
    }
    //ApplicationContextAware 回调方法
    public void setApplicationContext(ApplicationContext ac) {
        System.out.println("call aware callback");
    }
    public  void   testTwo(){
        System.out.println("OneService 执行啦！！！");
    }
}
