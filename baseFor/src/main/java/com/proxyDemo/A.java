package com.proxyDemo;

public class A  implements  D{
    public void hello(String hello){
        System.out.println(hello);
    }

    @Override
    public void say(String hello) {
        System.out.println("say");
    }
}
