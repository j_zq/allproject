package com.proxyDemo;

import java.lang.reflect.InvocationHandler;
import java.lang.reflect.Method;

public class C implements InvocationHandler {
    //需要代理的目标对象
    private Object target;

    /**
     * 将目标对象存入，一定得是具体的对象，而不是class
     * @param target
     */
    public C(Object target) {
        this.target = target;
    }

    @Override
    public Object invoke(Object proxy, Method method, Object[] args) throws Throwable {
        //前置增强
        System.out.println("洗水壶，准备茶叶，茶点\n");

        //生成织入类，作为代理
        Object obj = method.invoke(target,args);

        //后置增强
        System.out.println("喝完，洗茶杯，收拾……\n");

        //将包含织入方法的代理对象返回
        return obj;
    }
}
