package com.proxyDemo;

import java.lang.reflect.Proxy;

public class B {
    public static void main(String[] args) {
        //JDK 动态代理  只能增强接口方法
        A a = new A();
        C c = new C(a);
        D d = (D) Proxy.newProxyInstance(a.getClass().getClassLoader(), a.getClass().getInterfaces(),
                c);
        d.hello("hen");
        System.out.println("====");
        d.say("say");
    }
}
