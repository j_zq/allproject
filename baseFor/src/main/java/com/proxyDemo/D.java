package com.proxyDemo;

public interface D {
     void hello(String hello);
     void say(String hello);
}
