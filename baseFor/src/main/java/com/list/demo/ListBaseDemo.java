package com.list.demo;

import org.junit.Test;
import org.springframework.util.StopWatch;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.concurrent.CopyOnWriteArrayList;

/**
 * @author: jzq
 * @date: 2020/7/15 15:52      满了即扩容    (1+0.5)n  默认是10 -> 15  ->22   1.8默认是0 add->10
 * @description:
 */
public class ListBaseDemo {
    private List<Integer> list = new ArrayList<>();

    {
        list.add(1);
        list.add(21);
        list.add(31);
        list.add(41);
    }

    //取出两个集合中相同的交集
    @Test
    public void testRetainAll() {
        ArrayList<Integer> list = new ArrayList<>();
        ArrayList<Integer> newList = new ArrayList<>();
        list.add(1);
        list.add(2);
        list.add(3);
        newList.add(2);
        newList.add(3);
        newList.add(4);
        list.retainAll(newList);
        System.out.println(list);  //2,3
    }

    @Test
    public void testCopyOnWriteArrayList() {
        ArrayList<Integer> li = new ArrayList<>();
        for (int i = 0; i < 100000; i++) {
            li.add(i);
        }
        //写能力很差
        CopyOnWriteArrayList<Integer> list = new CopyOnWriteArrayList<>(li);
        long startTimes = System.currentTimeMillis();
        for (Integer integer : list) {
            if (integer % 1000 == 0)
                System.out.println(integer);
        }
        long endTimes = System.currentTimeMillis();
        System.out.println("CopyOnWriteArrayList时间" + (endTimes - startTimes));
    }

    @Test
    public void testSynchronizedList() {
        List<Integer> li = new ArrayList<>();
        for (int i = 0; i < 100000; i++) {
            li.add(i);
        }
        List<Integer> list = Collections.synchronizedList(li);
        long startTimes = System.currentTimeMillis();
        for (Integer integer : list) {
            if (integer % 1000 == 0)
                System.out.println(integer);
        }
        long endTimes = System.currentTimeMillis();
        System.out.println("synchronizedList时间" + (endTimes - startTimes));
    }

    @Test
    public void testList() {
        ArrayList<Integer> list = new ArrayList<>();
        list.add(1);
        testList(list);
        System.out.println(list);
    }

    public void testList(List<Integer> list) {
        list.add(22);
    }

    @Test
    public void test() {

        List<Integer> list = new ArrayList<Integer>();
        list.add(1);
        list.add(22);
        ArrayList<Integer> list1 = new ArrayList<>();
        list1.add(3);
        list1.add(4);
        list1.add(22);
        for (Integer o : list) {
            for (int i = 0; i < list1.size(); i++) {
                if (list1.get(i).equals(o)) {
                    Integer remove = list1.remove(i);
                    System.out.println(remove);
                    i--;
                }
            }
        }
        System.out.println(list1);
    }



    /**
     * watch  记录日志
     */
    @Test
    public void testWatch() {
        //
        StopWatch watch = new StopWatch("testList");
        String  str= "1";
//        //millis
        int listSize = list.size();
        watch.start();
        for (int j = 0; j < 100; j++) {
            for (int i = 0; i < 10000; i++) {
                System.out.println(str);
            }
        }

        watch.stop();
        System.out.println(watch.prettyPrint());
    }
    @Test
    public void testWatch1() {
      List<String> list=new ArrayList<>();
      list.add("1");
        for (String s : list) {
            System.out.println(s);
        }
        System.out.println(1);
        List<String> newList= null;

        list.addAll(newList);
        for (String s : list) {
            System.out.println(s);
        }
    }
    @Test
    public void testArrays() {
       String[] arr= new String[]{"1","3","4","56","7","9"};
        for (String i : arr) {
            System.out.println(i);
        }
        System.out.println("+++++++++++++");
        List<String> list = new ArrayList<>(Arrays.asList(arr));
        for (String s : list) {
            System.out.println(s);
        }
    }
}