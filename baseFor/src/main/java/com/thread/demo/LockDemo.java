package com.thread.demo;

import org.junit.Test;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;
import java.util.concurrent.locks.ReentrantLock;

public class LockDemo {
    //https://www.bilibili.com/video/BV1gJ411D7p9?p=11  教程地址
    private ReentrantLock lock = new ReentrantLock(true);
    private List<String> list = new ArrayList<>();

    @Test
    public void testLock() {
        Thread thread1 = new Thread(() -> {
            try {
                tryToLock();
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        });
        thread1.setName("t1");
        Thread thread2 = new Thread(() -> {
            try {
                tryToLock();
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        });
        thread2.setName("t2");
        thread1.start();
        System.out.println("t1===end");
        thread2.start();
        System.out.println("t2===end");

    }

    public void tryToLock() throws InterruptedException {
        try {
            lock.lock();
            System.out.println("当前线程名称：" + Thread.currentThread().getName());
            Thread.sleep(1000);
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            lock.unlock();
        }
    }

    @Test
    public void testThread() throws InterruptedException {
        for (int i1 = 0; i1 < 10; i1++) {
            Thread thread = new Thread(() -> {
                printLn();
            });
            thread.setName("i1:" + i1);
            thread.start();
        }
        Thread.sleep(1111);
        for (String s : list) {
            System.out.println(s);
        }
    }

    private void printLn() {
        ReentrantLock lock = new ReentrantLock();
        lock.lock();
        int i = new Random().nextInt(10) + 10;
        list.add("Thread.currentThread().getName()" +i );
        System.out.println("当前线程名称：" + Thread.currentThread().getName());
        lock.unlock();
    }
}
