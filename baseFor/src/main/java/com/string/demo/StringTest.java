package com.string.demo;

import org.junit.Test;

/**
 * @author: jzq
 * @date: 2020/7/23 14:59
 * @description:
 */
@SuppressWarnings("all")
public class StringTest {
    @Test
    public void test() {
        String str = "江州区";
        if (str == null || "".equals(str)) {
            str = "";
        }
        str = str.
                replaceAll("([\\u4E00-\\u9FA5])([\\u4E00-\\u9FA5]){2}", "$1**");
        String phone = "18702807637";
        phone = phone.replaceAll("(\\d{3})\\d{4}(\\d{4})", "$1***$2");
        System.out.println(str);
        System.out.println("phone :" + phone);
    }

    @Test
    public void test2() {
        String str = "歪道";
        str = str.
                replaceAll("([\\u4E00-\\u9FA5])([\\u4E00-\\u9FA5,\\w\\W]){" + (str.length() - 1) + "}", "$1**");
        //匹配任意字符
//         str=str.replaceAll("([\\w\\w]*)","*");
        String phone = "18702807637";
        phone = phone.replaceAll("(\\d{3})\\d{4}(\\d{4})", "$1***$2");
        System.out.println(str);
        System.out.println("phone :" + phone);
    }

    @Test
    public void test3() {
        String str = " 18702807637";
        if (str != null && str.contains(" ") && !" ".equals(str)) {
            String[] strings = str.split(" ");
            if (strings.length > 1) {
                System.out.println("0" + strings[0]);
                str = strings[0] + " " + strings[1].replaceAll("(\\d{3})\\d{4}(\\d{4})", "$1***$2");
            } else {
                str = strings[0].replaceAll("(\\d{3})\\d{4}(\\d{4})", "$1***$2");
            }
        }
        System.out.println(str);
    }

    @Test
    public void test333() {
        String str = "{\n" +
                "\t\"data\":\"{\\\"采购申请传OA流程表申请日期\\\":\\\"20200830\\\",\\\"采购订单编号\\\":\\\"NC012312\\\",\\\"采购申请附件\\\":\\\"5485532337809214230\\\",\\\"sub\\\":[{\\\"行号\\\":\\\"001\\\",\\\"行主键\\\":\\\"001\\\",\\\"物料编码\\\":\\\"001\\\",\\\"行号\\\":\\\"001\\\",\\\"行号\\\":\\\"001\\\",\\\"行号\\\":\\\"001\\\",\\\"行号\\\":\\\"001\\\",\\\"型号\\\":\\\"001\\\",\\\"规格\\\":\\\"001\\\",\\\"主单位\\\":\\\"001\\\",\\\"主单位\\\":\\\"001\\\",\\\"单位\\\":\\\"001\\\",\\\"数量\\\":\\\"001\\\",\\\"主数量\\\":\\\"001\\\",\\\"无税单价\\\":\\\"001\\\",\\\"含税单价\\\":\\\"001\\\",\\\"无税金额\\\":\\\"001\\\",\\\"税额\\\":\\\"001\\\",\\\"价税合计\\\":\\\"001\\\",\\\"税率\\\":\\\"001\\\",\\\"需求库存组织\\\":\\\"001\\\",\\\"补充说明\\\":\\\"001\\\",\\\"备注\\\":\\\"001\\\"}],\\\"表头主键\\\":\\\"四川科伦药业股份有限公司销售组织\\\",\\\"订单类型\\\":\\\"1010\\\",\\\"采购人员编码\\\":\\\"国内\\\",\\\"采购员\\\":\\\"范念琳\\\",\\\"采购部门\\\":\\\"李健\\\",\\\"采购方式\\\":\\\"李健\\\",\\\"申请类型\\\":\\\"李健\\\",\\\"订单日期\\\":\\\"\\\",\\\"供应商\\\":\\\"李健\\\",\\\"收货联系人\\\":\\\"李健\\\",\\\"收货联系人电话\\\":\\\"李健\\\",\\\"收货公司\\\":\\\"李健\\\",\\\"收货地址\\\":\\\"JG00000808\\\"}\",\n" +
                "   \"senderLoginName\":\"test8\",\n" +
                "   \"templateCode\":\"oanc_001\",\n" +
                "   \"transfertype\":\"json\"\n";
        System.out.println(str);
    }

    @Test
    public void test111() {

    }

    @Test
    public void te() {
        String str = "{ [\n" +
                "            {\n" +
                "                \"_index\": \"platform_188_20211228_1946\",\n" +
                "                \"_type\": \"common_type\",\n" +
                "                \"_id\": \"82K20001US_446e929abb0b8-49d7-89da-5e2bfbe550ea\",\n" +
                "                \"_score\": 0,\n" +
                "                \"_source\": {\n" +
                "                    \"modifyTime\": 1656953867241,\n" +
                "                    \"modifySource\": \"产品增量级联更新\",\n" +
                "                    \"id\": \"82K20001US_446e929abb0b8-49d7-89da-5e2bfbe550ea\",\n" +
                "                    \"inventory\": {\n" +
                "                        \"scopes_s\": [],\n" +
                "                        \"have_stock\": \"\",\n" +
                "                        \"updateTime\": 1656293172805,\n" +
                "                        \"config\": []\n" +
                "                    },\n" +
                "                    \"country\": \"GB\",\n" +
                "                    \"product\": {\n" +
                "                        \"type_s\": 0,\n" +
                "                        \"general\": {\n" +
                "                            \"marketing_status\": \"Available\",\n" +
                "                            \"customModelFlag\": \"NO\",\n" +
                "                            \"optionSpecialBid\": \"\",\n" +
                "                            \"isInWhiteList\": 0,\n" +
                "                            \"phantomFlag\": \"\",\n" +
                "                            \"always_show\": \"\",\n" +
                "                            \"user_group_always_show\": {\n" +
                "                                \"scopes_s\": [],\n" +
                "                                \"config\": []\n" +
                "                            },\n" +
                "                            \"salesStatus\": \"\",\n" +
                "                            \"user_group_marketing_status\": {\n" +
                "                                \"scopes_s\": [],\n" +
                "                                \"config\": []\n" +
                "                            },\n" +
                "                            \"tangibleFlag\": \"I\"\n" +
                "                        },\n" +
                "                        \"urls\": [],\n" +
                "                        \"isDcgSubseries\": false,\n" +
                "                        \"billingCycle\": \"\",\n" +
                "                        \"code_s\": \"82K20001US\",\n" +
                "                        \"isMedion\": false,\n" +
                "                        \"seriesCategoryName\": \"\",\n" +
                "                        \"status_s\": \"Final\",\n" +
                "                        \"serialNumber\": \"\",\n" +
                "                        \"isOutlet\": false,\n" +
                "                        \"baseWarrantyKey\": \"N01\"\n" +
                "                    },\n" +
                "                    \"hierarchy\": {\n" +
                "                        \"scopes_s\": [\n" +
                "                            \"88IPG30158982K2\",\n" +
                "                            \"88IPG301589\",\n" +
                "                            \"88IPG30\",\n" +
                "                            \"88IP\",\n" +
                "                            \"8\"\n" +
                "                        ],\n" +
                "                        \"path\": [],\n" +
                "                        \"config\": [\n" +
                "                            {\n" +
                "                                \"code\": \"88IPG30158982K2\",\n" +
                "                                \"name\": \"82K2 - IdeaPad Gaming 3 15ACH6\",\n" +
                "                                \"language\": \"en\"\n" +
                "                            },\n" +
                "                            {\n" +
                "                                \"code\": \"88IPG301589\",\n" +
                "                                \"name\": \"IdeaPad Gaming 3 15ACH06\",\n" +
                "                                \"language\": \"en\"\n" +
                "                            },\n" +
                "                            {\n" +
                "                                \"code\": \"88IPG30\",\n" +
                "                                \"name\": \"IdeaPad Gaming 3 series\",\n" +
                "                                \"language\": \"en_us\"\n" +
                "                            },\n" +
                "                            {\n" +
                "                                \"code\": \"88IP\",\n" +
                "                                \"name\": \"ideapad\",\n" +
                "                                \"language\": \"en_us\"\n" +
                "                            },\n" +
                "                            {\n" +
                "                                \"code\": \"8\",\n" +
                "                                \"name\": \"Consumer Notebook\",\n" +
                "                                \"language\": \"en_us\"\n" +
                "                            }\n" +
                "                        ]\n" +
                "                    },\n" +
                "                    \"rating\": {\n" +
                "                        \"star\": 0,\n" +
                "                        \"commentCount\": 0\n" +
                "                    },\n" +
                "                    \"store\": \"446e929abb0b8-49d7-89da-5e2bfbe550ea\",\n" +
                "                    \"media\": [\n" +
                "                        {\n" +
                "                            \"data\": {\n" +
                "                                \"thumbnail\": {\n" +
                "                                    \"imageName\": \"23498737439_Gaming 3 15ACH6_20210430114109.png\"\n" +
                "                                },\n" +
                "                                \"heroImage\": {\n" +
                "                                    \"en\": {\n" +
                "                                        \"imageName\": \"lenovo-laptop-ideapad-gaming-3-gen-6-15-amd-subseries-hero.png\",\n" +
                "                                        \"imageAddress\": \"//p1-ofp.static.pub/medias/bWFzdGVyfHJvb3R8MjIwNzY5fGltYWdlL3BuZ3xoNWMvaGEwLzExNjcwNjY2NTc1OTAyLnBuZ3w1M2Q4ZjYyMDEwMzY4NWY3ZTQyYjY1ZGZhMmQ3MjE1MWNjZDM0MTZlMjVhMDYxZmEyMWU4MDZlZGRiODZiY2U3/lenovo-laptop-ideapad-gaming-3-gen-6-15-amd-subseries-hero.png\"\n" +
                "                                    }\n" +
                "                                },\n" +
                "                                \"listImage\": {\n" +
                "                                    \"en\": {\n" +
                "                                        \"imageName\": \"lenovo-laptop-ideapad-gaming-3-gen-6-15-amd-series-thumbnail.png\",\n" +
                "                                        \"imageAddress\": \"//p1-ofp.static.pub/medias/bWFzdGVyfHJvb3R8OTQ4MzN8aW1hZ2UvcG5nfGhlMS9oMzEvMTE2NzA2NjY5NjkxMTgucG5nfDcyNTIwY2EwMGEyMGNiNzBhYzI2NzgyNTg2ZmJjMzlkOTI4ODk0MDM2ZTRkY2Y1NzQ5NGY2NDFiZDM1NjMzODc/lenovo-laptop-ideapad-gaming-3-gen-6-15-amd-series-thumbnail.png\"\n" +
                "                                    }\n" +
                "                                },\n" +
                "                                \"gallery\": {\n" +
                "                                    \"en\": [\n" +
                "                                        {\n" +
                "                                            \"imageName\": \"flash-lenovo-laptop-ideapad-gaming-3-gen-6-15-amd-gallery-1.png\",\n" +
                "                                            \"imageAddress\": \"//p4-ofp.static.pub/fes/cms/2021/09/17/37n3wsaafhbznkthyo41ul190o035z326891.png\"\n" +
                "                                        },\n" +
                "                                        {\n" +
                "                                            \"imageName\": \"lenovo-laptop-ideapad-gaming-3-gen-6-15-amd-subseries-gallery-2.png\",\n" +
                "                                            \"imageAddress\": \"//p3-ofp.static.pub/fes/cms/2021/09/17/f4p81jvjgoktv14j4r8jjemfb6mozq914984.png\"\n" +
                "                                        },\n" +
                "                                        {\n" +
                "                                            \"imageName\": \"lenovo-laptop-ideapad-gaming-3-gen-6-15-amd-subseries-gallery-3.png\",\n" +
                "                                            \"imageAddress\": \"//p2-ofp.static.pub/fes/cms/2021/09/17/yqkej82uvssmnwtvwztglack29os2w704011.png\"\n" +
                "                                        },\n" +
                "                                        {\n" +
                "                                            \"imageName\": \"lenovo-laptop-ideapad-gaming-3-gen-6-15-amd-subseries-gallery-4.png\",\n" +
                "                                            \"imageAddress\": \"//p1-ofp.static.pub/fes/cms/2021/09/17/za6x0jusrmckvyre2e2vmhbssnw8xl680623.png\"\n" +
                "                                        },\n" +
                "                                        {\n" +
                "                                            \"imageName\": \"lenovo-laptop-ideapad-gaming-3-gen-6-15-amd-subseries-gallery-5.png\",\n" +
                "                                            \"imageAddress\": \"//p1-ofp.static.pub/fes/cms/2021/09/17/98bo0mapj62bo5tx0ob4f0v7ywwxaw174260.png\"\n" +
                "                                        },\n" +
                "                                        {\n" +
                "                                            \"imageName\": \"lenovo-laptop-ideapad-gaming-3-gen-6-15-amd-subseries-gallery-6.png\",\n" +
                "                                            \"imageAddress\": \"//p2-ofp.static.pub/fes/cms/2021/09/17/8ctlqzzebtyq1294g1iofsvpta058s232613.png\"\n" +
                "                                        },\n" +
                "                                        {\n" +
                "                                            \"imageName\": \"lenovo-laptop-ideapad-gaming-3-gen-6-15-amd-subseries-gallery-7.png\",\n" +
                "                                            \"imageAddress\": \"//p3-ofp.static.pub/fes/cms/2021/09/17/9o9g1s9c3kzby3s73xma5uqfdqetsh628073.png\"\n" +
                "                                        },\n" +
                "                                        {\n" +
                "                                            \"imageName\": \"lenovo-laptop-ideapad-gaming-3-gen-6-15-amd-subseries-gallery-8.png\",\n" +
                "                                            \"imageAddress\": \"//p4-ofp.static.pub/fes/cms/2021/09/17/l05syod4b9m0wsu5edrh83812gf5ed865669.png\"\n" +
                "                                        },\n" +
                "                                        {\n" +
                "                                            \"imageName\": \"lenovo-laptop-ideapad-gaming-3-gen-6-15-amd-subseries-gallery-9.png\",\n" +
                "                                            \"imageAddress\": \"//p3-ofp.static.pub/fes/cms/2021/09/17/92s3okupcaie4phskd9w4rto2g7988533432.png\"\n" +
                "                                        },\n" +
                "                                        {\n" +
                "                                            \"imageName\": \"lenovo-laptop-ideapad-gaming-3-gen-6-15-amd-subseries-gallery-10.png\",\n" +
                "                                            \"imageAddress\": \"//p2-ofp.static.pub/fes/cms/2021/09/17/jv8qjop3gtwgm8suexeyqddqnx4agt703664.png\"\n" +
                "                                        }\n" +
                "                                    ]\n" +
                "                                }\n" +
                "                            },\n" +
                "                            \"type\": \"product\"\n" +
                "                        }\n" +
                "                    ],\n" +
                "                    \"pmi\": [\n" +
                "                        {\n" +
                "                            \"data\": {\n" +
                "                                \"systemPurchaseOnly\": {\n" +
                "                                    \"446e929abb0b8-49d7-89da-5e2bfbe550ea\": \"0\"\n" +
                "                                },\n" +
                "                                \"name\": {\n" +
                "                                    \"446e929abb0b8-49d7-89da-5e2bfbe550ea\": {\n" +
                "                                        \"en_gb\": \"NB IP Gaming 3 15ACH6 R5 8G 1T 256G 10H\"\n" +
                "                                    }\n" +
                "                                },\n" +
                "                                \"marketingShortDescription\": {\n" +
                "                                    \"446e929abb0b8-49d7-89da-5e2bfbe550ea\": {\n" +
                "                                        \"en_gb\": \"<ul><li><p>Versatile 15.6&quot; gaming laptop</p></li><li><p>Up to AMD Ryzen<sup>™</sup> 7 processor</p></li><li><p>NVIDIA<sup>®</sup> GeForce RTX<sup>™</sup> 30 series graphics</p></li></ul>\"\n" +
                "                                    }\n" +
                "                                },\n" +
                "                                \"marketingLongDescription\": {\n" +
                "                                    \"446e929abb0b8-49d7-89da-5e2bfbe550ea\": {\n" +
                "                                        \"en_gb\": \"<p>Step up to the top tier of elite gaming with leading-edge AMD Ryzen<sup>™</sup> 5000 H-Series Mobile Processors and the latest NVIDIA<sup>®</sup> GeForce RTX<sup>™</sup> GPUs. Delivering maximum value as an all-purpose computer, the IdeaPad Gaming 3 lets you game as fast and drive as hard as the pros, with the blazing-fast 165Hz FHD IPS display, 100% N-key rollover on your ultra-fast keyboard, faster thermals that dissipate 40% more heat, and Nahimic surround sound audio.</p>\"\n" +
                "                                    }\n" +
                "                                }\n" +
                "                            },\n" +
                "                            \"type\": \"product\"\n" +
                "                        }\n" +
                "                    ],\n" +
                "                    \"merchandising\": [],\n" +
                "                    \"sales\": {\n" +
                "                        \"count\": 0\n" +
                "                    },\n" +
                "                    \"onlineDate\": 0,\n" +
                "                    \"region\": \"3ed28cb5bf200-43e8-97b3-81a99bcf360a\",\n" +
                "                    \"businessType\": \"b03e4f9abae71-4dd8-8985-383efd097bfe\",\n" +
                "                    \"avr\": {\n" +
                "                        \"withdraw_dt\": 0,\n" +
                "                        \"announce_dt\": 0,\n" +
                "                        \"key\": \"\"\n" +
                "                    },\n" +
                "                    \"category\": {\n" +
                "                        \"scopes_s\": [\n" +
                "                            \"WMD00000479\",\n" +
                "                            \"ideapad\",\n" +
                "                            \"ROOTPMI\",\n" +
                "                            \"notebooks\",\n" +
                "                            \"ideapad-gaming-3-series\"\n" +
                "                        ],\n" +
                "                        \"path\": [],\n" +
                "                        \"default\": [],\n" +
                "                        \"manual_subseries_flag\": true,\n" +
                "                        \"override\": [\n" +
                "                            {\n" +
                "                                \"code\": \"WMD00000479\",\n" +
                "                                \"scope\": \"B2C\",\n" +
                "                                \"name\": \"Gaming 3 Gen 6 (15 AMD)\",\n" +
                "                                \"language\": \"en\",\n" +
                "                                \"scope_type\": \"geoTree\"\n" +
                "                            }\n" +
                "                        ]\n" +
                "                    },\n" +
                "                    \"display_rule\": {\n" +
                "                        \"display_to\": [\n" +
                "                            \"B2B\",\n" +
                "                            \"B2C\"\n" +
                "                        ]\n" +
                "                    },\n" +
                "                    \"shipping\": {\n" +
                "                        \"lead_time\": \"\",\n" +
                "                        \"readyToShip\": \"\",\n" +
                "                        \"message\": \"\"\n" +
                "                    },\n" +
                "                    \"bind\": {\n" +
                "                        \"promotedOption\": [],\n" +
                "                        \"preselectOption\": []\n" +
                "                    },\n" +
                "                    \"facets\": {}\n" +
                "                }\n" +
                "            }\n" +
                "        ]}";
        System.out.println(str.replaceAll("\\n","").
                replaceAll("  ",""));
    }
    //todo   将 map字符串  转为json 字符串

    /**
     * 将 map字符串  转为json 字符串   out of  stock   过渡  transition
     * Build step 'Execute shell' marked build as failure
     */
    @Test
    public   void  test1(){
        System.out.println("{\"query\":{\"query_json\":{\"function_score\":{\"query\":{\"bool\":{\"must\":[{\"bool\":{\"should\":[{\"nested\":{\"path\":\"price.config\",\"query\":{\"function_score\":{\"query\":{\"bool\":{\"should\":[{\"bool\":{\"must\":[{\"term\":{\"price.config.level\":\"store\"}},{\"exists\":{\"field\":\"price.config.final_price.price\"}}],\"must_not\":[]}},{\"bool\":{\"must\":[{\"term\":{\"price.config.level\":\"country\"}},{\"exists\":{\"field\":\"price.config.final_price.price\"}}],\"must_not\":[{\"terms\":{\"price.config.lower_level\":[\"store\"]}}]}},{\"bool\":{\"must\":[{\"term\":{\"price.config.level\":\"base\"}},{\"exists\":{\"field\":\"price.config.final_price.price\"}}],\"must_not\":[{\"terms\":{\"price.config.lower_level\":[\"store\",\"country\"]}}]}}]}},\"functions\":[{\"filter\":{\"range\":{\"price.config.final_price.price\":{\"gt\":\"-1\"}}},\"exp\":{\"price.config.final_price.price\":{\"origin\":\"0\",\"scale\":\"3000\",\"decay\":\"0.8\"}},\"weight\":\"1.0\"}],\"score_mode\":\"sum\",\"boost_mode\":\"replace\"}}}},{\"terms\":{\"product.type_s\":[\"2\",\"11\"]}},{\"bool\":{\"must\":[{\"terms\":{\"product.general.marketing_status\":[\"Coming Soon\"]}},{\"terms\":{\"product.type_s\":[5]}}]}}]}}],\"filter\":[{\"bool\":{\"must\":[{\"bool\":{\"should\":[{\"bool\":{\"must\":[{\"term\":{\"avr.key\":\"B2C_JP\"}},{\"range\":{\"avr.announce_dt\":{\"lte\":\"now\"}}},{\"range\":{\"avr.withdraw_dt\":{\"gte\":\"now\"}}}]}},{\"terms\":{\"product.type_s\":[\"2\",\"11\"]}}]}},{\"bool\":{\"must\":[{\"terms\":{\"display_rule.display_to\":[\"B2C\",\"af9af3b5b1fd9-4a64-84f0-3151ef1b92d4\",\"d16e9517bdaf6-4e55-9db1-b41a93e0e331\",\"cf524c11bc7f1-42ab-a037-729d0069660c\",\"31a7df05b1608-4740-8c95-c33ff48b16ab\",\"37683ef4-e2aa-11ea-8adc-fa163e7a920f\"]}}],\"must_not\":[{\"terms\":{\"display_rule.not_display_to\":[\"B2C\",\"af9af3b5b1fd9-4a64-84f0-3151ef1b92d4\",\"d16e9517bdaf6-4e55-9db1-b41a93e0e331\",\"cf524c11bc7f1-42ab-a037-729d0069660c\",\"31a7df05b1608-4740-8c95-c33ff48b16ab\",\"37683ef4-e2aa-11ea-8adc-fa163e7a920f\"]}}]}},{\"bool\":{\"should\":[{\"term\":{\"product.status_s\":\"Final\"}},{\"terms\":{\"product.type_s\":[\"2\",\"11\",\"5\",\"9\",\"10\"]}}]}},{\"bool\":{\"should\":[{\"bool\":{\"must\":[{\"terms\":{\"product.type_s\":[0,4,7]}},{\"bool\":{\"must\":[{\"exists\":{\"field\":\"category.path\"}},{\"bool\":{\"should\":[{\"exists\":{\"field\":\"hierarchy.path\"}},{\"term\":{\"category.manual_subseries_flag\":true}}]}}]}}]}},{\"bool\":{\"must\":[{\"terms\":{\"product.type_s\":[3,12,6,8,9]}},{\"exists\":{\"field\":\"hierarchy.path\"}}]}},{\"terms\":{\"product.type_s\":[\"2\",\"11\",\"5\",\"10\"]}}]}},{\"bool\":{\"should\":[{\"terms\":{\"product.type_s\":[\"1\",\"2\",\"4\",\"5\",\"8\",\"9\",\"10\",\"11\",\"14\"]}},{\"term\":{\"product.general.isInWhiteList\":1}},{\"bool\":{\"must\":[{\"terms\":{\"product.type_s\":[\"7\",\"12\"]}},{\"terms\":{\"product.general.customModelFlag\":[\"NO\",\"POR FP\",\"POR PRE\",\"POR SP\"]}}]}},{\"bool\":{\"must\":[{\"term\":{\"product.type_s\":0}},{\"terms\":{\"product.general.customModelFlag\":[\"NO\",\"FP\"]}}]}},{\"bool\":{\"must\":[{\"terms\":{\"product.type_s\":[\"3\",\"6\"]}},{\"terms\":{\"product.general.optionSpecialBid\":[\"NO\",\"FP\"]}}]}}]}},{\"bool\":{\"must_not\":[{\"bool\":{\"must\":[{\"terms\":{\"product.type_s\":[\"7\",\"12\"]}},{\"term\":{\"product.general.phantomFlag\":\"YES\"}}]}}]}},{\"bool\":{\"should\":[{\"terms\":{\"product.type_s\":[\"2\",\"11\",\"5\",\"10\"]}},{\"terms\":{\"product.general.salesStatus\":[\"12\",\"16\",\"17\",\"19\",\"20\",\"21\",\"22\",\"24\"]}}]}},{\"bool\":{\"must_not\":{\"bool\":{\"must\":[{\"terms\":{\"product.type_s\":[\"5\",\"10\"]}},{\"term\":{\"product.models_validation\":false}}]}}}}]}},{\"bool\":{\"should\":[{\"term\":{\"product.general.marketing_status\":\"Available\"}},{\"term\":{\"product.general.always_show\":\"1\"}}],\"must_not\":[{\"term\":{\"product.general.marketing_status\":\"End Of Life\"}}]}},{\"terms\":{\"store\":[\"af9af3b5b1fd9-4a64-84f0-3151ef1b92d4\"]}},{\"bool\":{\"should\":[{\"terms\":{\"product.type_s\":[\"2\",\"11\",\"4\",\"6\",\"7\",\"12\",\"1\",\"10\",\"9\"]}},{\"bool\":{\"must\":[{\"term\":{\"inventory.have_stock\":true}},{\"terms\":{\"product.type_s\":[\"5\",\"8\",\"3\"]}}]}},{\"term\":{\"product.general.always_show\":\"1\"}},{\"nested\":{\"path\":\"inventory.config\",\"query\":{\"bool\":{\"should\":[{\"bool\":{\"must\":[{\"term\":{\"inventory.config.warehouse\":\"A200\"}},{\"term\":{\"inventory.config.have_stock\":true}}]}},{\"bool\":{\"must\":[{\"term\":{\"inventory.config.warehouse\":\"J200\"}},{\"term\":{\"inventory.config.have_stock\":true}}]}},{\"bool\":{\"must\":[{\"term\":{\"inventory.config.warehouse\":\"JP\"}},{\"term\":{\"inventory.config.have_stock\":true}}]}}]}}}}]}},{\"bool\":{\"must\":[{\"bool\":{\"should\":[{\"bool\":{\"should\":[{\"terms\":{\"category.scopes_s\":[\"laptops\"]}},{\"terms\":{\"hierarchy.path\":[\"laptops\"]}}]}}]}},{\"terms\":{\"product.type_s\":[\"0\",\"1\",\"3\",\"4\",\"5\",\"6\",\"7\",\"8\",\"9\",\"10\",\"12\",\"14\"]}}]}}]}},\"functions\":[],\"score_mode\":\"sum\",\"boost_mode\":\"sum\"}}},\"spell_check\":false,\"filter\":null,\"aggs\":{},\"aggs_fields\":null,\"rescore\":null,\"return_fields\":{\"fields\":[\"id\",\"store\",\"region\",\"country\",\"businessType\",\"modifyTime\",\"product.general\",\"product.type_s\",\"product.code_s\",\"product.models_count\",\"product.isMedion\",\"product.seriesCategoryName\",\"product.isDcgSubseries\",\"product.billingCycle\",\"product.urls\",\"product.models_attribute\",\"product.models_codes_with_type\",\"product.isOutlet\",\"hierarchy.scopes_s\",\"hierarchy.path\",\"category.scopes_s\",\"category.path\",\"price\",\"shipping\",\"rating\",\"inventory\",\"onlineDate\",\"pmi\",\"media\",\"merchandising\",\"bind.promotedOption\",\"bind.preselectOption\",\"classification_final.ja_jp.LOIS_TS_VAMMECHPKGFF\",\"classification_final.ja_jp.MNL_NB_DOCKING\",\"classification_final.ja_jp.SD_Motion_Supported\",\"classification_final.ja_jp.LOIS_ISE_REARHDD\",\"classification_final.ja_jp.LOIS_SCA_OPSYS\",\"classification_final.ja_jp.MNL_NB_ACTIVEPEN\",\"classification_final.ja_jp.LOIS_EP_MOUNTING_STAND\",\"classification_final.ja_jp.MNL_ISE_CHASISOPT\",\"classification_final.ja_jp.LOIS_NB_AUDIO\",\"classification_final.ja_jp.LOIS_NB_BAT\",\"classification_final.ja_jp.MNL_DT_FLEX_IO_PORT\",\"classification_final.ja_jp.LOIS_SCA_PORT\",\"classification_final.ja_jp.LOIS_SCA_HDD\",\"classification_final.ja_jp.LOIS_TS_PRCDESC\",\"classification_final.ja_jp.LOIS_SCA_DPY\",\"classification_final.ja_jp.LOIS_TS_TOTDERIVEDSLOT\",\"classification_final.ja_jp.MNL_NB_BULBTYPE\",\"classification_final.ja_jp.LOIS_NB_MBGFPR\",\"classification_final.ja_jp.LOIS_DT_COOLINGSYSTEM\",\"classification_final.ja_jp.LOIS_NB_CAM2\",\"classification_final.ja_jp.LOIS_EP_POWERSUPPLY\",\"classification_final.ja_jp.LOIS_NB_FPR\",\"classification_final.ja_jp.LOIS_NB_BT\",\"classification_final.ja_jp.LOIS_DT_ETHERNET\",\"classification_final.ja_jp.LOIS_DT_POWERSUPPLY\",\"classification_final.ja_jp.ATTRIBUTE_TEST\",\"classification_final.ja_jp.LOIS_EP_ETHERNET\",\"classification_final.ja_jp.LOIS_DT_KYB\",\"classification_final.ja_jp.LOIS_SCA_MEM\",\"classification_final.ja_jp.LOIS_SCA_POWERSUPP\",\"classification_final.ja_jp.LOIS_NB_WLAN\",\"classification_final.ja_jp.LOIS_ISE_POWERSUPPLY\",\"classification_final.ja_jp.LOIS_DT_HDD3\",\"classification_final.ja_jp.MNL_SCA_COLOR\",\"classification_final.ja_jp.LOIS_ISE_MEMSUMMARY\",\"classification_final.ja_jp.LOIS_DT_PLATFORM\",\"classification_final.ja_jp.LOIS_DT_HDD2\",\"classification_final.ja_jp.LOIS_EP_PLATFORM\",\"classification_final.ja_jp.LOIS_DT_CAMERAMIC\",\"classification_final.ja_jp.LOIS_NB_PD\",\"classification_final.ja_jp.LOIS_TS_VAMMEMORYSTD\",\"classification_final.ja_jp.MNL_NB_BASETYPE\",\"classification_final.ja_jp.LOIS_EP_HDD2\",\"classification_final.ja_jp.LOIS_EP_KYB\",\"classification_final.ja_jp.MNL_ISE_TOTAVAILBAYS\",\"classification_final.ja_jp.LOIS_EP_MOUSE\",\"classification_final.ja_jp.MNL_NB_ADD_ONS\",\"classification_final.ja_jp.LOIS_EP_NETWORKSUPPORT\",\"classification_final.ja_jp.LOIS_ISE_RADIMI\",\"classification_final.ja_jp.LOIS_NB_WWAN\",\"classification_final.ja_jp.MNL_EP_MCR\",\"classification_final.ja_jp.LOIS_DT_MOUNTING_STAND\",\"classification_final.ja_jp.LOIS_SCA_CPU\",\"classification_final.ja_jp.LOIS_SCA_ODD\",\"classification_final.ja_jp.LOIS_DT_STORAGE\",\"classification_final.ja_jp.MNL_SCA_MICARRAY\",\"classification_final.ja_jp.LOIS_TS_VAMNOPROC\",\"classification_final.ja_jp.MNL_NB_LUMENS\",\"classification_final.ja_jp.LOIS_DT_MOUSE\",\"classification_final.ja_jp.LOIS_ISE_ETH1\",\"classification_final.ja_jp.LOIS_TS_MEMMKTDESC\",\"classification_final.ja_jp.MNL_EP_FLEX_IO_PORT\",\"classification_final.ja_jp.LOIS_TS_VAMPROCSPEED\",\"classification_final.ja_jp.LOIS_NB_CAM\",\"classification_final.ja_jp.MNL_SCA_SPEAKER\",\"classification_final.ja_jp.LOIS_NB_SIMCARD\",\"classification_final.ja_jp.LOIS_TS_DISKDESC\",\"classification_final.ja_jp.LOIS_SCA_VIDEO\",\"classification_final.ja_jp.LOIS_NB_HDD2\",\"classification_final.ja_jp.LOIS_TS_POWER\",\"classification_final.ja_jp.LOIS_DT_CARDREADER\",\"classification_final.ja_jp.MNL_NB_SMARTASSCOMPAT\",\"classification_final.ja_jp.LOIS_ISE_ODD1\",\"classification_final.ja_jp.LOIS_ISE_ODD2\",\"classification_final.ja_jp.CNET_ACC_M_MANUFACTURERWARRANTY\",\"classification_final.ja_jp.LOIS_NB_HDD3\",\"classification_final.ja_jp.LOIS_TS_VAMINTHDMAX\",\"classification_final.ja_jp.MNL_DCG_PERFORMANCE\",\"classification_final.ja_jp.LOIS_DT_WIFI\",\"classification_final.ja_jp.LOIS_SCA_WARRPERIOD\",\"classification_final.ja_jp.LOIS_EP_WLAN\",\"classification_final.ja_jp.NL_SCA_OLET_CONDTYPE\",\"classification_final.ja_jp.LOIS_NB_KYB\",\"classification_final.ja_jp.LOIS_NB_BODYCOLOR\",\"classification_final.ja_jp.MNL_DT_ADD_ONS\",\"classification_final.ja_jp.LOIS_ISE_HDD3\",\"classification_final.ja_jp.LOIS_TS_TOTAVAILBAY\",\"classification_final.ja_jp.LOIS_ISE_HDD2\",\"classification_final.ja_jp.LOIS_ISE_HDD5\",\"classification_final.ja_jp.LOIS_ISE_HDD4\",\"classification_final.ja_jp.LOIS_EP_STORAGE\",\"classification_final.ja_jp.MNL_EP_KEYLOCK\",\"classification_final.ja_jp.LOIS_ISE_S1\",\"classification_final.ja_jp.LOIS_TS_TOTDERIVEDBAY\",\"classification_final.ja_jp.LOIS_SCA_ESS\",\"classification_final.ja_jp.LOIS_ACC_N_EUENERGYRATING\"]},\"heightlight_fields\":null,\"sort\":null,\"lang\":\"ja_jp\",\"page\":1,\"size\":20},{\"query\":{\"query_json\":{\"function_score\":{\"query\":{\"bool\":{\"must\":[{\"bool\":{\"should\":[{\"nested\":{\"path\":\"price.config\",\"query\":{\"function_score\":{\"query\":{\"bool\":{\"should\":[{\"bool\":{\"must\":[{\"term\":{\"price.config.level\":\"store\"}},{\"exists\":{\"field\":\"price.config.final_price.price\"}}],\"must_not\":[]}},{\"bool\":{\"must\":[{\"term\":{\"price.config.level\":\"country\"}},{\"exists\":{\"field\":\"price.config.final_price.price\"}}],\"must_not\":[{\"terms\":{\"price.config.lower_level\":[\"store\"]}}]}},{\"bool\":{\"must\":[{\"term\":{\"price.config.level\":\"base\"}},{\"exists\":{\"field\":\"price.config.final_price.price\"}}],\"must_not\":[{\"terms\":{\"price.config.lower_level\":[\"store\",\"country\"]}}]}}]}},\"functions\":[{\"filter\":{\"range\":{\"price.config.final_price.price\":{\"gt\":\"-1\"}}},\"exp\":{\"price.config.final_price.price\":{\"origin\":\"0\",\"scale\":\"3000\",\"decay\":\"0.8\"}},\"weight\":\"1.0\"}],\"score_mode\":\"sum\",\"boost_mode\":\"replace\"}}}},{\"terms\":{\"product.type_s\":[\"2\",\"11\"]}},{\"bool\":{\"must\":[{\"terms\":{\"product.general.marketing_status\":[\"Coming Soon\"]}},{\"terms\":{\"product.type_s\":[5]}}]}}]}}],\"filter\":[{\"bool\":{\"must\":[{\"bool\":{\"should\":[{\"bool\":{\"must\":[{\"term\":{\"avr.key\":\"B2C_JP\"}},{\"range\":{\"avr.announce_dt\":{\"lte\":\"now\"}}},{\"range\":{\"avr.withdraw_dt\":{\"gte\":\"now\"}}}]}},{\"terms\":{\"product.type_s\":[\"2\",\"11\"]}}]}},{\"bool\":{\"must\":[{\"terms\":{\"display_rule.display_to\":[\"B2C\",\"af9af3b5b1fd9-4a64-84f0-3151ef1b92d4\",\"d16e9517bdaf6-4e55-9db1-b41a93e0e331\",\"cf524c11bc7f1-42ab-a037-729d0069660c\",\"31a7df05b1608-4740-8c95-c33ff48b16ab\",\"37683ef4-e2aa-11ea-8adc-fa163e7a920f\"]}}],\"must_not\":[{\"terms\":{\"display_rule.not_display_to\":[\"B2C\",\"af9af3b5b1fd9-4a64-84f0-3151ef1b92d4\",\"d16e9517bdaf6-4e55-9db1-b41a93e0e331\",\"cf524c11bc7f1-42ab-a037-729d0069660c\",\"31a7df05b1608-4740-8c95-c33ff48b16ab\",\"37683ef4-e2aa-11ea-8adc-fa163e7a920f\"]}}]}},{\"bool\":{\"should\":[{\"term\":{\"product.status_s\":\"Final\"}},{\"terms\":{\"product.type_s\":[\"2\",\"11\",\"5\",\"9\",\"10\"]}}]}},{\"bool\":{\"should\":[{\"bool\":{\"must\":[{\"terms\":{\"product.type_s\":[0,4,7]}},{\"bool\":{\"must\":[{\"exists\":{\"field\":\"category.path\"}},{\"bool\":{\"should\":[{\"exists\":{\"field\":\"hierarchy.path\"}},{\"term\":{\"category.manual_subseries_flag\":true}}]}}]}}]}},{\"bool\":{\"must\":[{\"terms\":{\"product.type_s\":[3,12,6,8,9]}},{\"exists\":{\"field\":\"hierarchy.path\"}}]}},{\"terms\":{\"product.type_s\":[\"2\",\"11\",\"5\",\"10\"]}}]}},{\"bool\":{\"should\":[{\"terms\":{\"product.type_s\":[\"1\",\"2\",\"4\",\"5\",\"8\",\"9\",\"10\",\"11\",\"14\"]}},{\"term\":{\"product.general.isInWhiteList\":1}},{\"bool\":{\"must\":[{\"terms\":{\"product.type_s\":[\"7\",\"12\"]}},{\"terms\":{\"product.general.customModelFlag\":[\"NO\",\"POR FP\",\"POR PRE\",\"POR SP\"]}}]}},{\"bool\":{\"must\":[{\"term\":{\"product.type_s\":0}},{\"terms\":{\"product.general.customModelFlag\":[\"NO\",\"FP\"]}}]}},{\"bool\":{\"must\":[{\"terms\":{\"product.type_s\":[\"3\",\"6\"]}},{\"terms\":{\"product.general.optionSpecialBid\":[\"NO\",\"FP\"]}}]}}]}},{\"bool\":{\"must_not\":[{\"bool\":{\"must\":[{\"terms\":{\"product.type_s\":[\"7\",\"12\"]}},{\"term\":{\"product.general.phantomFlag\":\"YES\"}}]}}]}},{\"bool\":{\"should\":[{\"terms\":{\"product.type_s\":[\"2\",\"11\",\"5\",\"10\"]}},{\"terms\":{\"product.general.salesStatus\":[\"12\",\"16\",\"17\",\"19\",\"20\",\"21\",\"22\",\"24\"]}}]}},{\"bool\":{\"must_not\":{\"bool\":{\"must\":[{\"terms\":{\"product.type_s\":[\"5\",\"10\"]}},{\"term\":{\"product.models_validation\":false}}]}}}}]}},{\"bool\":{\"should\":[{\"term\":{\"product.general.marketing_status\":\"Available\"}},{\"term\":{\"product.general.always_show\":\"1\"}}],\"must_not\":[{\"term\":{\"product.general.marketing_status\":\"End Of Life\"}}]}},{\"terms\":{\"store\":[\"af9af3b5b1fd9-4a64-84f0-3151ef1b92d4\"]}},{\"bool\":{\"should\":[{\"terms\":{\"product.type_s\":[\"2\",\"11\",\"4\",\"6\",\"7\",\"12\",\"1\",\"10\",\"9\"]}},{\"bool\":{\"must\":[{\"term\":{\"inventory.have_stock\":true}},{\"terms\":{\"product.type_s\":[\"5\",\"8\",\"3\"]}}]}},{\"term\":{\"product.general.always_show\":\"1\"}},{\"nested\":{\"path\":\"inventory.config\",\"query\":{\"bool\":{\"should\":[{\"bool\":{\"must\":[{\"term\":{\"inventory.config.warehouse\":\"A200\"}},{\"term\":{\"inventory.config.have_stock\":true}}]}},{\"bool\":{\"must\":[{\"term\":{\"inventory.config.warehouse\":\"J200\"}},{\"term\":{\"inventory.config.have_stock\":true}}]}},{\"bool\":{\"must\":[{\"term\":{\"inventory.config.warehouse\":\"JP\"}},{\"term\":{\"inventory.config.have_stock\":true}}]}}]}}}}]}},{\"bool\":{\"must\":[{\"bool\":{\"should\":[{\"bool\":{\"should\":[{\"terms\":{\"category.scopes_s\":[\"laptops\"]}},{\"terms\":{\"hierarchy.path\":[\"laptops\"]}}]}}]}},{\"terms\":{\"product.type_s\":[\"0\",\"1\",\"3\",\"4\",\"5\",\"6\",\"7\",\"8\",\"9\",\"10\",\"12\",\"14\"]}}]}}]}},\"functions\":[],\"score_mode\":\"sum\",\"boost_mode\":\"sum\"}}},\"spell_check\":false,\"filter\":null,\"aggs\":{\"991\":{\"nested\":{\"path\":\"facets.config\"},\"aggs\":{\"productAttribute\":{\"filter\":{\"bool\":{\"must\":[{\"term\":{\"facets.config.attribute\":\"991\"}},{\"terms\":{\"facets.config.language\":[\"ja_jp\"]}}]}},\"aggs\":{\"productAttribute\":{\"terms\":{\"field\":\"facets.config.value.original\",\"size\":300,\"exclude\":[]}}}}}},\"2316\":{\"nested\":{\"path\":\"loyalty\"},\"aggs\":{\"loyaltyMultiple\":{\"filter\":{\"bool\":{\"should\":[{\"bool\":{\"must\":[{\"term\":{\"loyalty.level\":\"store\"}}],\"must_not\":[]}}]}},\"aggs\":{\"loyaltyMultiple\":{\"range\":{\"field\":\"loyalty.multipleValue\",\"ranges\":[{\"from\":\"0.1\",\"to\":\"1.001\",\"key\":\"1X Rewards\"},{\"from\":\"1.1\",\"to\":\"2.001\",\"key\":\"2X Rewards logan test\"},{\"from\":\"2.1\",\"to\":\"3.001\",\"key\":\"3X Rewards\"},{\"from\":\"3.1\",\"to\":\"4.001\",\"key\":\"4X Rewards\"},{\"from\":\"4.1\",\"to\":\"5.001\",\"key\":\"5X Rewards\"},{\"from\":\"5.1\",\"to\":\"6.001\",\"key\":\"6X Rewards\"},{\"from\":\"6.1\",\"to\":\"7.001\",\"key\":\"7X Rewards\"},{\"from\":\"7.1\",\"to\":\"8.001\",\"key\":\"8X Rewards\"},{\"from\":\"8.1\",\"to\":\"9.001\",\"key\":\"9X Rewards\"},{\"from\":\"9.1\",\"to\":\"10.001\",\"key\":\"10X Rewards\"}]}}}}}},\"700\":{\"nested\":{\"path\":\"facets.config\"},\"aggs\":{\"productAttribute\":{\"filter\":{\"bool\":{\"must\":[{\"term\":{\"facets.config.attribute\":\"700\"}},{\"terms\":{\"facets.config.language\":[\"ja_jp\"]}}]}},\"aggs\":{\"productAttribute\":{\"terms\":{\"field\":\"facets.config.value.original\",\"size\":300,\"exclude\":[]}}}}}},\"count\":{\"value_count\":{\"field\":\"product.code_s\"}},\"701\":{\"nested\":{\"path\":\"facets.config\"},\"aggs\":{\"productAttribute\":{\"filter\":{\"bool\":{\"must\":[{\"term\":{\"facets.config.attribute\":\"701\"}},{\"terms\":{\"facets.config.language\":[\"ja_jp\"]}}]}},\"aggs\":{\"productAttribute\":{\"terms\":{\"field\":\"facets.config.value.original\",\"size\":300,\"exclude\":[]}}}}}},\"704\":{\"nested\":{\"path\":\"facets.config\"},\"aggs\":{\"productAttribute\":{\"filter\":{\"bool\":{\"must\":[{\"term\":{\"facets.config.attribute\":\"704\"}},{\"terms\":{\"facets.config.language\":[\"ja_jp\"]}}]}},\"aggs\":{\"productAttribute\":{\"terms\":{\"field\":\"facets.config.value.original\",\"size\":300,\"exclude\":[]}}}}}},\"maxSave\":{\"nested\":{\"path\":\"price.config\"},\"aggs\":{\"maxSave\":{\"filter\":{\"bool\":{\"should\":[{\"bool\":{\"must\":[{\"term\":{\"price.config.level\":\"store\"}}],\"must_not\":[]}},{\"bool\":{\"must\":[{\"term\":{\"price.config.level\":\"country\"}}],\"must_not\":[{\"terms\":{\"price.config.lower_level\":[\"store\"]}}]}},{\"bool\":{\"must\":[{\"term\":{\"price.config.level\":\"base\"}}],\"must_not\":[{\"terms\":{\"price.config.lower_level\":[\"store\",\"country\"]}}]}}]}},\"aggs\":{\"percent\":{\"max\":{\"field\":\"price.config.final_price.percentage\"}},\"amount\":{\"max\":{\"field\":\"price.config.final_price.discount\"}}}}}}},\"aggs_fields\":null,\"rescore\":null,\"return_fields\":{\"fields\":[\"id\",\"store\",\"region\",\"country\",\"businessType\",\"modifyTime\",\"product.general\",\"product.type_s\",\"product.code_s\",\"product.models_count\",\"product.isMedion\",\"product.seriesCategoryName\",\"product.isDcgSubseries\",\"product.billingCycle\",\"product.urls\",\"product.models_attribute\",\"product.models_codes_with_type\",\"product.isOutlet\",\"hierarchy.scopes_s\",\"hierarchy.path\",\"category.scopes_s\",\"category.path\",\"price\",\"shipping\",\"rating\",\"inventory\",\"onlineDate\",\"pmi\",\"media\",\"merchandising\",\"bind.promotedOption\",\"bind.preselectOption\",\"classification_final.ja_jp.LOIS_TS_VAMMECHPKGFF\",\"classification_final.ja_jp.MNL_NB_DOCKING\",\"classification_final.ja_jp.SD_Motion_Supported\",\"classification_final.ja_jp.LOIS_ISE_REARHDD\",\"classification_final.ja_jp.LOIS_SCA_OPSYS\",\"classification_final.ja_jp.MNL_NB_ACTIVEPEN\",\"classification_final.ja_jp.LOIS_EP_MOUNTING_STAND\",\"classification_final.ja_jp.MNL_ISE_CHASISOPT\",\"classification_final.ja_jp.LOIS_NB_AUDIO\",\"classification_final.ja_jp.LOIS_NB_BAT\",\"classification_final.ja_jp.MNL_DT_FLEX_IO_PORT\",\"classification_final.ja_jp.LOIS_SCA_PORT\",\"classification_final.ja_jp.LOIS_SCA_HDD\",\"classification_final.ja_jp.LOIS_TS_PRCDESC\",\"classification_final.ja_jp.LOIS_SCA_DPY\",\"classification_final.ja_jp.LOIS_TS_TOTDERIVEDSLOT\",\"classification_final.ja_jp.MNL_NB_BULBTYPE\",\"classification_final.ja_jp.LOIS_NB_MBGFPR\",\"classification_final.ja_jp.LOIS_DT_COOLINGSYSTEM\",\"classification_final.ja_jp.LOIS_NB_CAM2\",\"classification_final.ja_jp.LOIS_EP_POWERSUPPLY\",\"classification_final.ja_jp.LOIS_NB_FPR\",\"classification_final.ja_jp.LOIS_NB_BT\",\"classification_final.ja_jp.LOIS_DT_ETHERNET\",\"classification_final.ja_jp.LOIS_DT_POWERSUPPLY\",\"classification_final.ja_jp.ATTRIBUTE_TEST\",\"classification_final.ja_jp.LOIS_EP_ETHERNET\",\"classification_final.ja_jp.LOIS_DT_KYB\",\"classification_final.ja_jp.LOIS_SCA_MEM\",\"classification_final.ja_jp.LOIS_SCA_POWERSUPP\",\"classification_final.ja_jp.LOIS_NB_WLAN\",\"classification_final.ja_jp.LOIS_ISE_POWERSUPPLY\",\"classification_final.ja_jp.LOIS_DT_HDD3\",\"classification_final.ja_jp.MNL_SCA_COLOR\",\"classification_final.ja_jp.LOIS_ISE_MEMSUMMARY\",\"classification_final.ja_jp.LOIS_DT_PLATFORM\",\"classification_final.ja_jp.LOIS_DT_HDD2\",\"classification_final.ja_jp.LOIS_EP_PLATFORM\",\"classification_final.ja_jp.LOIS_DT_CAMERAMIC\",\"classification_final.ja_jp.LOIS_NB_PD\",\"classification_final.ja_jp.LOIS_TS_VAMMEMORYSTD\",\"classification_final.ja_jp.MNL_NB_BASETYPE\",\"classification_final.ja_jp.LOIS_EP_HDD2\",\"classification_final.ja_jp.LOIS_EP_KYB\",\"classification_final.ja_jp.MNL_ISE_TOTAVAILBAYS\",\"classification_final.ja_jp.LOIS_EP_MOUSE\",\"classification_final.ja_jp.MNL_NB_ADD_ONS\",\"classification_final.ja_jp.LOIS_EP_NETWORKSUPPORT\",\"classification_final.ja_jp.LOIS_ISE_RADIMI\",\"classification_final.ja_jp.LOIS_NB_WWAN\",\"classification_final.ja_jp.MNL_EP_MCR\",\"classification_final.ja_jp.LOIS_DT_MOUNTING_STAND\",\"classification_final.ja_jp.LOIS_SCA_CPU\",\"classification_final.ja_jp.LOIS_SCA_ODD\",\"classification_final.ja_jp.LOIS_DT_STORAGE\",\"classification_final.ja_jp.MNL_SCA_MICARRAY\",\"classification_final.ja_jp.LOIS_TS_VAMNOPROC\",\"classification_final.ja_jp.MNL_NB_LUMENS\",\"classification_final.ja_jp.LOIS_DT_MOUSE\",\"classification_final.ja_jp.LOIS_ISE_ETH1\",\"classification_final.ja_jp.LOIS_TS_MEMMKTDESC\",\"classification_final.ja_jp.MNL_EP_FLEX_IO_PORT\",\"classification_final.ja_jp.LOIS_TS_VAMPROCSPEED\",\"classification_final.ja_jp.LOIS_NB_CAM\",\"classification_final.ja_jp.MNL_SCA_SPEAKER\",\"classification_final.ja_jp.LOIS_NB_SIMCARD\",\"classification_final.ja_jp.LOIS_TS_DISKDESC\",\"classification_final.ja_jp.LOIS_SCA_VIDEO\",\"classification_final.ja_jp.LOIS_NB_HDD2\",\"classification_final.ja_jp.LOIS_TS_POWER\",\"classification_final.ja_jp.LOIS_DT_CARDREADER\",\"classification_final.ja_jp.MNL_NB_SMARTASSCOMPAT\",\"classification_final.ja_jp.LOIS_ISE_ODD1\",\"classification_final.ja_jp.LOIS_ISE_ODD2\",\"classification_final.ja_jp.CNET_ACC_M_MANUFACTURERWARRANTY\",\"classification_final.ja_jp.LOIS_NB_HDD3\",\"classification_final.ja_jp.LOIS_TS_VAMINTHDMAX\",\"classification_final.ja_jp.MNL_DCG_PERFORMANCE\",\"classification_final.ja_jp.LOIS_DT_WIFI\",\"classification_final.ja_jp.LOIS_SCA_WARRPERIOD\",\"classification_final.ja_jp.LOIS_EP_WLAN\",\"classification_final.ja_jp.NL_SCA_OLET_CONDTYPE\",\"classification_final.ja_jp.LOIS_NB_KYB\",\"classification_final.ja_jp.LOIS_NB_BODYCOLOR\",\"classification_final.ja_jp.MNL_DT_ADD_ONS\",\"classification_final.ja_jp.LOIS_ISE_HDD3\",\"classification_final.ja_jp.LOIS_TS_TOTAVAILBAY\",\"classification_final.ja_jp.LOIS_ISE_HDD2\",\"classification_final.ja_jp.LOIS_ISE_HDD5\",\"classification_final.ja_jp.LOIS_ISE_HDD4\",\"classification_final.ja_jp.LOIS_EP_STORAGE\",\"classification_final.ja_jp.MNL_EP_KEYLOCK\",\"classification_final.ja_jp.LOIS_ISE_S1\",\"classification_final.ja_jp.LOIS_TS_TOTDERIVEDBAY\",\"classification_final.ja_jp.LOIS_SCA_ESS\",\"classification_final.ja_jp.LOIS_ACC_N_EUENERGYRATING\"]},\"heightlight_fields\":null,\"sort\":null,\"lang\":\"ja_jp\",\"page\":1,\"size\":0}"
                );

//
//        Set<Object> set = new HashSet<>();
//        set.add(1);
//        set.add(2);
//        set.add(3);
//        set.add(4);
//        set.add(5);
//        set.add(6);
//        for (int i = 1; i <= set.size()/2+1; i++) {
//            Set<Object> currentSet = set.stream().skip((i-1)*2)
//                    .limit(2L).collect(Collectors.toSet());
//            currentSet.forEach(a-> System.out.println(a));
//            System.out.println("===");
//        }

    }

}