package com.enums.demo;

/**
 * @author: jzq
 * @date: 2020/8/27 09:18
 * @description:
 */
public class   EnumUtil {

    public  static  enum  numberEnum{
       ONE("ONE","1"),
       TWO("TWO","2");
        private   String  key;
        private  String  value;
        numberEnum(String key,String value){
            this.key=key;
            this.value=value;
        }
        public String getKey() {
            return key;
        }

        public String getValue() {
            return value;
        }
    }
    public static enum allotMainField{
        BSARTSSSS("BSART","凭证类型"),
        BATXT("BATXT","凭证类型描述"),
        ERNAM("ERNAM","创建者"),
        AEDAT("AEDAT","创建日期"),
        LIFNR("LIFNR","调出方"),
        NAME1_LIFNR("NAME1_LIFNR","调出方描述"),
        EKORG("EKORG","采购组织"),
        EKOTX("EKOTX","采购组织描述"),
        EKGRP("EKGRP","采购组"),
        EKNAM("EKNAM","采购组描述"),
        BUKRS("BUKRS","调入方"),
        BUTXT("BUTXT","调入方描述"),
        ZZBEDNR("ZZBEDNR","需求部门"),
        TT_TTWB("TT_TTWB","抬头文本"),
        TT_TTZS("TT_TTZS","抬头注释"),
        EBELN("EBELN","调拨单号");

        private String key;
        private String name;

        allotMainField(String key,String name) {
            this.key = key;
            this.name = name;
        }

        public String getKey() {
            return key;
        }
        public String getName() {
            return name;
        }

    }

    public static void main(String[] args) {
        for (numberEnum value : numberEnum.values()) {
            System.out.println(value);
        }
        for (allotMainField value : allotMainField.values()) {
            System.out.println(value);
            System.out.println(value.getKey()+":"+value.getName());
        }
    }
}