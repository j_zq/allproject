package com.sort.demo;

import org.junit.Test;

public class SortDemo {


    @Test
    public void test() {
        System.out.println(3 ^ 5);//0011 0101    3^5= 0110 =6
        int[] arr = new int[]{2, 3, 1, 4};
        for (int i = 0; i < arr.length - 1; i++) {
            for (int j = i + 1; j < arr.length; j++) {
                if (arr[i] > arr[j]) {
                    swap(arr, i, j);
                }
            }
        }
        for (int i : arr) {
            System.out.println(i);
        }
    }

    public void swap(int[] arr, int i, int j) {
        arr[i] = arr[i] ^ arr[j];
        arr[j] = arr[i] ^ arr[j];
        arr[i] = arr[i] ^ arr[j];
    }

    /**
     * 异或运算的妙用  查找出只出现一次的元素（其余元素要出现2次）
     *  0与任何数异或为其本身    两数相同异或为0
     */
    @Test
    public  void  testOr(){
        int[] arr = new int[]{2, 3, 1, 1,2,3,4};
        int   a = 0 ;
        for (int i : arr) {
            a^=i;
        }
        System.out.println(a);
    }

}
