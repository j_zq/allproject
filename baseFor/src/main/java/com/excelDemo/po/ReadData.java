package com.excelDemo.po;

import com.alibaba.excel.annotation.ExcelProperty;
import lombok.Data;
import lombok.experimental.Accessors;
import com.utils.MyCommonToolUtils;

@Data
@Accessors(chain = true)
public class ReadData {

    //序号

    @ExcelProperty(value = "序号", index = 0)

    private String sort;

    //所属公司名称必填
    @ExcelProperty(index = 1)
    private String companyName;

    //部门名称必填
    @ExcelProperty(index = 2)
    private String departmentName;
    //二级部门
    @ExcelProperty(index = 3)
    private String secondDepartment;
    //姓名必填
    @ExcelProperty(index = 4)
    private String personName;
    //登录名必填
    @ExcelProperty(index = 5)
    private String personLoginName;
    //主岗必填
    @ExcelProperty(index = 6)
    private String mainPost;
    //副岗
    @ExcelProperty(index = 7)
    private String deputyPost;

    //空
    @ExcelProperty(index = 8)
    private String no;
    //职务级别必填
    @ExcelProperty(index = 9)
    private String jobLevel;
    //是否部门主管必填
    @ExcelProperty(index = 10)
    private String whetherDepartmentHead;
    //权限备注
    @ExcelProperty(index = 11)
    private String permissionNotes;
    //手机号必填
    @ExcelProperty(index = 12)
    private String phone;
    //电子邮箱
    @ExcelProperty(index = 13)
    private String email;

    public String getSort() {
        return sort;
    }

    public void setSort(String sort) {
        this.sort = sort;
    }

    public String getCompanyName() {
        return companyName;
    }

    public void setCompanyName(String companyName) {
        this.companyName = companyName;
    }

    public String getDepartmentName() {
        return departmentName;
    }

    public void setDepartmentName(String departmentName) {
        this.departmentName = departmentName;
    }

    public String getSecondDepartment() {
        return secondDepartment;
    }

    public void setSecondDepartment(String secondDepartment) {
        this.secondDepartment = secondDepartment;
    }

    public String getPersonName() {
        return personName;
    }

    public void setPersonName(String personName) {
        this.personName = personName;
    }

    public String getPersonLoginName() {
        return personLoginName;
    }

    public void setPersonLoginName(String personLoginName) {
        this.personLoginName = personLoginName;
    }

    public String getMainPOst() {
        return mainPost;
    }

    public void setMainPost(String mainPost) {
        this.mainPost = mainPost;
    }

    public String getDeputyPost() {
        return deputyPost;
    }

    public void setDeputyPost(String deputyPost) {
        this.deputyPost = deputyPost;
    }

    public String getNo() {
        return no;
    }

    public void setNo(String no) {
        this.no = no;
    }

    public String getJobLevel() {
        return jobLevel;
    }

    public void setJobLevel(String jobLevel) {
        this.jobLevel = jobLevel;
    }

    public String getWhetherDepartmentHead() {
        return whetherDepartmentHead;
    }

    public void setWhetherDepartmentHead(String whetherDepartmentHead) {
        this.whetherDepartmentHead = whetherDepartmentHead;
    }

    public String getPermissionNotes() {
        return permissionNotes;
    }

    public void setPermissionNotes(String permissionNotes) {
        this.permissionNotes = permissionNotes;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        if (MyCommonToolUtils.isNotEmpty(phone)) {
            if (phone.contains("/")) {
                if (phone.equals("/")) {
                    this.phone = "";
                } else {
                    this.phone = phone.split("/")[0];
                }
            } else {
                this.phone = phone;
            }
        }
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        if (MyCommonToolUtils.isNotEmpty(email)) {
            if (email.contains("/")) {
                if (email.equals("/")) {
                    this.email = "";
                } else {
                    this.email = email.split("/")[0];
                }
            } else {
                this.email = email;
            }
        }

    }


}