package com.excelDemo.po;

import lombok.Data;
import lombok.experimental.Accessors;

/**
 * @description: 部门实体类
 * @author: jzq
 * @date: 2020-10-29 10:49
 **/

@Data
@Accessors(chain = true)
public class DepartmentPo {
    //1级部门
    private String firstDepartment;
    //2级部门
    private String twiceDepartment;
    //3级部门
    private String thirdDepartment;
    //4级部门
    private String fourthDepartment;
    //5级部门
    private String fifthDepartment;
    //部门代码
    private String departmentCode;
    //部门层级
    private String departmentLevel;
    //    排序号
    private String sort;
    //    描述
    private String description;
    //    部门主管
    private String departmentHeader;
    //    部门分管领导
    private String departmentInCharge;
    //    部门管理员
    private String departmentAdmin;
    //    部门公文收文员
    private String departmentalDocumentReceiver;
    //    部门公文送文员
    private String departmentDocumentsToClerk;

}
