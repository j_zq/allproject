package com.excelDemo.po;

import lombok.Data;
import lombok.experimental.Accessors;

/**
 * @description: 岗位 实体类
 * @author: jzq
 * @date: 2020-10-29 10:56
 **/

@Data
@Accessors(chain = true)
public class PostPo {
    //    岗位名称
    private String postName;
    //    岗位代码
    private String postCode;
    //    岗位类别
    private String postCategory;
    //    所属单位
    private String affiliation;
    //    状态
    private String status;

}
