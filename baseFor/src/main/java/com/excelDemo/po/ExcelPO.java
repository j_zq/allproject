package com.excelDemo.po;

import com.alibaba.excel.annotation.ExcelProperty;
import lombok.Data;
import lombok.experimental.Accessors;

/**
 * @description: 基础数据
 * @author: jzq
 * @date: 2020-10-28 15:18
 **/
@Data
@Accessors(chain = true)
public class ExcelPO {
    //设置表头名称
    @ExcelProperty(value = "学生编号",index = 1)
    private int sno;
    //设置表头名称
    @ExcelProperty(value = "学生姓名",index = 5)
    private String sname;
}
