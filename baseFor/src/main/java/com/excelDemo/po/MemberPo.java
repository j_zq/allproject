package com.excelDemo.po;

import com.alibaba.excel.annotation.ExcelProperty;
import lombok.Data;
import lombok.experimental.Accessors;

/**
 * @description: 人员实体类
 * @author: jzq
 * @date: 2020-10-29 11:00
 **/
@Data
@Accessors(chain = true)
public class MemberPo {
    @ExcelProperty(index = 0)
    private String name;
    //    姓名
//    登录名
    @ExcelProperty(index = 1)
    private String loginName;
    //    人员编号
    @ExcelProperty(index = 2)
    private String memberCode;
    //    人员排序号
    @ExcelProperty(index = 3)
    private String memberSort;
    //    所属单位
    @ExcelProperty(index = 4)
    private String affiliation;
    //    所属部门
    @ExcelProperty(index = 5)
    private String department;
    //    主岗
    @ExcelProperty(index = 6)
    private String mainPost;
    //    副岗
    @ExcelProperty(index = 7)
    private String deputyPost;
    //    职务级别
    @ExcelProperty(index = 8)
    private String jobLevel;
    //    移动电话号码
    @ExcelProperty(index = 9)
    private String mobile;
    //    电子邮件
    @ExcelProperty(index = 10)
    private String email;
    //    性别
    @ExcelProperty(index = 11)
    private String sex;
    //    出生日期
    @ExcelProperty(index = 12)
    private String dateOfBirth;
    //    办公电话
    @ExcelProperty(index = 13)
    private String phone;
    //    首选语言
    @ExcelProperty(index = 14)
    private String language;
    //    工作地
    @ExcelProperty(index = 15)
    private String workPlace;
    //    入职时间
    @ExcelProperty(index = 16)
    private String entryTime;
    //    汇报人
    @ExcelProperty(index = 17)
    private String reportBy;
    //    人员类型
    @ExcelProperty(index = 18)

    private String personType;
    //    备注
    @ExcelProperty(index = 19)
    private String remarks;

}
