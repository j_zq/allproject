package com.entity;


import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

import java.util.Map;

/**
 * @author: jzq
 * @date: 2020/8/3 15:35
 * @description:
 */
@Data
@Accessors(chain = true)
@EqualsAndHashCode(callSuper = false)
public class GoodsVo extends  Goods {
   private Map map;
}