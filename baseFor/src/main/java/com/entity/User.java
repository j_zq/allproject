package com.entity;

/**
 * @author: jzq
 * @date: 2020/8/10 10:01
 * @description:
 */
public class User {
    private   Integer id ;
    private  String name;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}