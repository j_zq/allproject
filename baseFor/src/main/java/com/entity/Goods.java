package com.entity;


import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

import java.time.LocalDateTime;

/**
 * @author: jzq
 * @date: 2020/8/3 15:35
 * @description:
 */
@Data
@Accessors(chain = true)
@EqualsAndHashCode(callSuper = false)
public class Goods {
    public   Integer id;
    /**
     * 编号
     */
    public  Integer no;
    /**
     * 价格  stop progress  block development  grooming stop PROGRESS
     * flash_deved
     */
    public  Double price;
    public   Long   sort;


    public  LocalDateTime createTime;
    public   Integer  getIdAndNo(){
        return id+no;
    }

}