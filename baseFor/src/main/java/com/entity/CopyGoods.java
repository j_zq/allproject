package com.entity;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

/**
 * @author: jzq
 * @date: 2020/8/3 15:48
 * @description:
 */
@Data
@Accessors(chain = true)
@EqualsAndHashCode(callSuper = false)
public class CopyGoods{
    private  Integer id;
    private  Integer no;
    private  Double price;
    private  String extra;
}