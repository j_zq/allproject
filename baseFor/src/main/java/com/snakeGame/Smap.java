package com.snakeGame;

import java.util.Random;

public class Smap {
    private boolean getFood = false;//如果得到食物，该指针设为true，并且在随后的autoChange里增加蛇的长度
    private boolean gameOver = false;
    private boolean directionChange = false;//这里标志的作用是保证在一次运动期间只会进行一次转向，使游戏更流畅
    private int MAP_SIZE;
    private Lattice[][] map;
    private int directionX = 0;//下一次头在当前位置的哪个方向上
    private int directionY = 1;//下一次头在当前位置的哪个方向上
    private int[] head = new int[2];//记录当前头的位置
    private int[] food = new int[2];//记录当前食物的位置

    public Smap(int size) {
        MAP_SIZE = size;
        map = new Lattice[MAP_SIZE][MAP_SIZE];
        for (int i = 0; i < size; i++) {
            for (int j = 0; j < size; j++) {
                map[i][j] = new Lattice();
            }
        }
        map[MAP_SIZE / 2][MAP_SIZE / 2].setHead(true, map[MAP_SIZE / 2][MAP_SIZE / 2 - 1]);//初始化设置一个头结点，以及他的尾节点
        head[0] = MAP_SIZE / 2;
        head[1] = MAP_SIZE / 2;
        map[MAP_SIZE / 2][MAP_SIZE / 2 - 1].setRear(true, null);
        this.randFood();
    }

    //模拟蛇的自动移动
    public void autoChange() {
        this.setHead();
        if (food[0] == head[0] && food[1] == head[1]) {//如果新的头部碰触到了食物，那么尾部增长
            getFood = true;
        }
        if (!gameOver) this.setRear();
        if (getFood) this.randFood();
        directionChange = false;
    }

    //根据键盘事件，改变头的下一次移动方向，注意 该移动方向是仅针对头部的
    //setDirection和setHead两个方法需要互斥进行，这里单线程，用synchronized即可
    //(否则，如果当前头部在边界位置，连续变幻方向可能导致在setHead里发生溢出)
    public synchronized void setDirection(int x, int y) {
        if (directionY != y && directionX != x && !directionChange) {
            directionX = x;
            directionY = y;
            directionChange = true;
        }
    }

    public boolean gameOver() {
        return gameOver;//头碰到身子，证明gameOver
    }

    private synchronized void setHead() {
        int i = head[0];
        int j = head[1];
        head[0] = (head[0] + directionX + MAP_SIZE) % MAP_SIZE;
        head[1] = (head[1] + directionY + MAP_SIZE) % MAP_SIZE;
        if (map[head[0]][head[1]].isBody()) gameOver = true;
        map[head[0]][head[1]].setHead(true, map[i][j]);
        map[i][j].setBody(true, null);
        map[i][j].setHead(false, null); //传入null表示不改变当前指向
    }

    //设置尾巴由于没法像头部那样直接设置，这里只能采用链表遍历的方式获取尾巴
    private void setRear() {
        if (!getFood) {
            Lattice temp = map[head[0]][head[1]];
            while (!temp.next.isRear()) temp = temp.next;
            temp.next().setRear(false, null);
            temp.setRear(true, null);
            temp.setBody(false, null);
        }
    }

    private void randFood() {
        getFood = false;
        map[food[0]][food[1]].setFood(false);//先把当前的食物取消掉
        boolean flag = false;//设置下一个食物
        Random random = new Random();
        int x = random.nextInt(MAP_SIZE);
        int y = random.nextInt(MAP_SIZE);
        while (!flag) {
            x = random.nextInt(MAP_SIZE);
            y = random.nextInt(MAP_SIZE);
            if (!map[x][y].isHead() && !map[x][y].isRear() && !map[x][y].isBody()) flag = true;
        }
        map[x][y].setFood(true);
        food[0] = x;
        food[1] = y;
    }

    public Lattice get(int row, int col) {
        return map[row][col];
    }

    public int getMAP_SIZE() {
        return MAP_SIZE;
    }
}