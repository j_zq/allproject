package com.snakeGame;

import javax.swing.*;
import java.awt.*;


public class View extends JPanel {
    private static final long serialVersionUID = -5258995676212660595L;
    private static final int GRID_SIZE = 32; //填充的像素数量
    private Smap thisMap;

    public View(Smap map) {
        thisMap = map;
    }

    @Override
    public void paint(Graphics g) {
        super.paint(g);
        int size = thisMap.getMAP_SIZE();
        for (int row = 0; row < size; row++) {
            for (int col = 0; col < size; col++) {
                Lattice lattice = thisMap.get(row, col);
                if (lattice != null) {
                    lattice.draw(g, col * GRID_SIZE, row * GRID_SIZE, GRID_SIZE);//对应的格子的显示
                }
            }
        }
    }

    @Override
    public Dimension getPreferredSize() {//创建该div大小
        return new Dimension(thisMap.getMAP_SIZE() * GRID_SIZE + 1, thisMap.getMAP_SIZE() * GRID_SIZE + 1);
    }
}