package com.snakeGame;

import java.awt.*;

import static java.awt.Color.black;
import static java.awt.Color.blue;
import static java.awt.Color.red;

public class Lattice {
    private boolean isBody = false;
    private boolean isHead = false;
    private boolean isFood = false;
    private boolean isRear = false;
    public Lattice next = null;


    public void setHead(boolean bool, Lattice next) {
        isHead = bool;
        if (next != null) this.next = next;
    }

    public void setBody(boolean bool, Lattice next) {
        isBody = bool;
        if (next != null) this.next = next; //传入参数为null时，不改变当前的next
    }

    public void setRear(boolean bool, Lattice next) {
        isRear = bool;
        this.next = next;
    }

    public void setFood(boolean bool) {
        isFood = bool;
    }

    public Lattice next() {
        return next;
    }

    public boolean isHead() {
        return isHead;
    }

    public boolean isFood() {
        return isFood;
    }

    public boolean isRear() {
        return isRear;
    }

    public boolean isBody() {
        return isBody;
    }


    public void refresh() {
        if (isHead) {
            isBody = true;
            isHead = false;
// 怎么设置下一个头呢？（考虑把DirectionX,Y放到Smap里，而不是这里）
        } else if (isBody) {
            if (next.isRear) {
                next.isRear = false;
                isRear = true;
                isBody = false;
            }
        }
    }

    // 在这里设置细胞可见
    public void draw(Graphics g, int x, int y, int size) {
        g.setColor(black);
        g.drawRect(x, y, size, size);
        if (isHead) {
            g.setColor(red);
            g.fillRect(x, y, size, size);
        } else if (isBody || isRear) {
            g.setColor(black);
            g.fillRect(x, y, size, size);
        } else if (isFood) {
            g.setColor(blue);
            g.fillRect(x, y, size, size);
        }
    }
}