package com.executor.demo;

import lombok.SneakyThrows;

import java.time.LocalDateTime;

/**
 * @author: jzq
 * @date: 2020/8/27 10:09
 * @description:
 */
public class ThreadTask implements Runnable {
    @SneakyThrows
    @Override
    public void run() {
        Thread.sleep(1000);
        System.out.println("时间："+ LocalDateTime.now());
    }
}