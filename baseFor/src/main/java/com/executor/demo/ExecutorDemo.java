package com.executor.demo;

import org.junit.Test;

import java.util.concurrent.*;

/**
 * @author: jzq
 * @date: 2020/8/27 09:59
 * @description:  创建线程池
 *  SynchronousQueue   默认队列  不存储元素的阻塞队列。直接提交给线程
 *  LinkedBlockingQueue 默认为Integer.MAX_VALUE，也就是无界队列
 *  DelayQueue：使用优先级队列实现的无界阻塞队列。
 *  ArrayBlockingQueue ：由数组结构组成的有界阻塞队列。
 *  LinkedTransferQueue：由链表结构组成的无界阻塞队列。
 *  LinkedBlockingDeque：由链表结构组成的双向阻塞队列
 *
 */
public class ExecutorDemo {
    @Test
    public      void  testThreadPoolExecutor(){
//        ThreadPoolExecutor executor = new ThreadPoolExecutor(5, 20, 50L,
//                TimeUnit.SECONDS, new SynchronousQueue<>(), Executors.defaultThreadFactory());
        ThreadPoolExecutor executor = new ThreadPoolExecutor(5, 15, 50L,
                TimeUnit.SECONDS, new LinkedBlockingQueue<>(), Executors.defaultThreadFactory()
                //设置 拒绝策略
                , new RejectedExecutionHandler() {
            @Override
            public void rejectedExecution(Runnable r, ThreadPoolExecutor executor) {
                System.out.println("执行拒绝策略");
            }
        });
        for (int i = 0; i < 20; i++) {
            ThreadTask task = new ThreadTask();
            executor.execute(task);
//            task.run();
        }
    }

    /**
     * 用来创建一个可以无限扩大的线程池，适用于负载较轻的场景，执行短期异步任务
     */
    @Test
    public  void  testCachedThreadPool(){
        ExecutorService executor = Executors.newCachedThreadPool();
        for (int i = 0; i < 20; i++) {
            executor.execute(new ThreadTask());
        }
    }

    /**
     * 创建一个单线程的线程池，适用于需要保证顺序执行各个任务。
     */
    @Test
    public  void  testSingleThreadExecutor(){
        ExecutorService executor = Executors.newSingleThreadExecutor();
        for (int i = 0; i < 20; i++) {
            executor.execute(new ThreadTask());
        }
    }

    /**
     * 创建一个固定大小的线程池，因为采用无界的阻塞队列，所以实际线程数量永远不会变化，适用于负载较重的场景，对当前线程数量进行限制
     */
    @Test
    public  void  testNewFixedThreadPool(){
        ExecutorService executor = Executors.newFixedThreadPool(5);
        for (int i = 0; i < 20; i++) {
            executor.execute(new ThreadTask());
        }
    }

    /**
     * 适用于执行延时或者周期性任务。
     */
    @Test
    public  void  testNewScheduledThreadPool(){
        ExecutorService executor = Executors.newScheduledThreadPool(5);
        for (int i = 0; i < 20; i++) {
            executor.execute(new ThreadTask());
        }
    }
}