package com.number.demo;


import org.junit.Test;

import java.math.BigDecimal;

/**
 * @author: jzq
 * @date: 2020/8/18 14:41
 * @description:
 */
public class NumberDemo {
    @Test
    public void test() {
        int i = 5;// 0000 0101
        int i1 = 20;// 0001 0100
        int j = -20;//   1001 0100   11       1000 1011
        int j1 = -5;//      1000  0101      10000 0011            1000 0000 0000 0000 0000 0000 0000 0101
        int j2 = -5 & 0xFF;//      1000  0101      10000 0011            1000 0000 0000 0000 0000 0000 0000 0101
        System.out.println(i >> 1);   //2
        System.out.println(i1 >> 1);  //10
        System.out.println(j >> 1);   //-10
        System.out.println(j1 >> 1);  //-3
        System.out.println(j1 >>> 1);  //-3
        System.out.println(j2);  //-3
        System.out.println(j2 >>> 1);  //-3
    }

    @Test
    public void testDouble() {
        double dou = 3.1437426;
        BigDecimal bigDecimal = new BigDecimal(dou).setScale(2, BigDecimal.ROUND_UP);
        double newDouble = bigDecimal.doubleValue();
        System.out.println(newDouble);
            //返回为字符串
//        double dou = 3.1487426;
//        DecimalFormat decimalFormat = new DecimalFormat("#.00");
//        String str = decimalFormat.format(dou);
//        System.out.println(str);
    }
    @Test
    public void testDivide() {
        int  i = 2,j=5;
        double ceil = Math.ceil(i / Double.valueOf(j));
        int k =  (int)ceil;
        System.out.println( k);
    }
}