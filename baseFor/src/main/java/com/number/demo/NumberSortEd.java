package com.number.demo;

import org.junit.Test;

import java.time.LocalDateTime;

/**
 * @description: 排序算法Demo
 * @author: jzq
 * @date: 2020-10-27 15:21
 **/
public class NumberSortEd {
    /**
    * @description: 冒泡排序算法
    * @param: []
    * @return: void
    * @date: 2020/10/27
    */
    @Test
    public  void     BUB(){
        int [] arr=new int[]{1,3,5,7,9,11,13,15,17,19,2};
        int  vt=0;
        LocalDateTime startTime = LocalDateTime.now();
        for (int i = 0; i < arr.length; i++) {
            for (int j = i; j < arr.length; j++) {
                if(arr[i]<arr[j]){
                    vt=arr[j];
                    arr[j]=arr[i];
                    arr[i]=vt;
                }
            }
        }
        System.out.println(LocalDateTime.now()+":"+startTime);
        for (int i : arr) {
            System.out.println(i);
        }
    }
}
