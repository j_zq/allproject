package com.time.demo;


import org.junit.Test;

import java.io.UnsupportedEncodingException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.*;
import java.time.format.DateTimeFormatter;
import java.util.Date;

/**
 * @author: jzq
 * @date: 2020/7/29 10:04
 * @description:
 */
public class LocalDateTimeDemo {
    @Test
    public void test() {
        LocalDateTime now = LocalDateTime.now();
        DateTimeFormatter inFormat = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss");
        System.out.println(inFormat.format(now));
        String time = "2020-09-25 11:42:22";
        LocalDateTime parse = LocalDateTime.parse(time, inFormat);
        System.out.println(parse);
        String sql  = " ";
        System.out.printf(sql);
        System.out.printf(" ");
//        DateTimeFormatter outFormat = DateTimeFormatter.ofPattern("EEE, MMM d yyyy, KK:mm a");
//        String outDateStr = now.format(inFormat);
//        LocalDateTime time = LocalDateTime.parse(outDateStr, inFormat);
//        System.out.println(outDateStr);
//        System.out.println(time);
    }

    @Test
    public void test1() throws UnsupportedEncodingException {
        //String  time ="2020-09-24 　16:02:33";
        String time = "2020-09-24 16:02:33";
        // String  time ="2020-09-24 16:02:33";
        // String  time ="2020-09-24 16:02:33";
        // String  time ="2020-02-05 19:02:02";
        LocalDateTime localDateTime = LocalDateTime.now();
        DateTimeFormatter inFormat = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss");
        DateTimeFormatter outFormat = DateTimeFormatter.ofPattern("EEE, MMM d yyyy, KK:mm a");
        LocalDateTime dateTime = LocalDateTime.parse(time, inFormat);
        System.out.println(dateTime);
    }

    @Test
    public void test222() {
        LocalDateTime localDateTime = LocalDateTime.now();
        localDateTime = localDateTime.minusDays(15);
        System.out.println(localDateTime);
    }

    @Test  ///localDate  获取当前时间戳
    public void testCurrentTime() {
        //LocalDateTime 转字符串
        //String currentTime = LocalDateTime.now(ZoneOffset.of("+8")).format(DateTimeFormatter.ofPattern("yyyyMMddHHmmssSSS"));
        //获取秒
        Long second = LocalDateTime.now().toEpochSecond(ZoneOffset.of("+8"));
        //获取毫秒数
        Long milliSecond = LocalDateTime.now().toInstant(ZoneOffset.of("+8")).toEpochMilli();
        long millis = System.currentTimeMillis();
        System.out.println(second + ":" + millis + ":" + milliSecond);
    }

    @Test  ///localDate  获取当前时间戳
    public void testSimpleDateformate() throws ParseException {
        Date date = new Date();
        System.out.println(date);
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("YYYY-MM-dd HH:mm:ss");
        String format = simpleDateFormat.format(date);
        System.out.println(format);
        System.out.println(simpleDateFormat.parse(format));
    }

    /**
     * Date  转 localDateTime
     */
    @Test
    public   void  dateToLocalDateTime(){
        Date date = new Date();
        ZoneId zoneId = ZoneId.systemDefault();
        Instant dateInstant = date.toInstant();
        ZonedDateTime dateZone = dateInstant.atZone(zoneId);
        LocalDateTime localDateTime = dateZone.toLocalDateTime();


        ZonedDateTime zonedDateTime = localDateTime.atZone(zoneId);
        Instant instant = zonedDateTime.toInstant();
        Date from = Date.from(instant);
    }
}