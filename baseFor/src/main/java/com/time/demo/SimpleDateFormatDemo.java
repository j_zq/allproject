package com.time.demo;

import java.text.SimpleDateFormat;

public class SimpleDateFormatDemo {
    public static void main(String[] args) {
        try{
            String format = new SimpleDateFormat("yyyy-MM-dd")
                    .format(new SimpleDateFormat("yyyyMMdd").parse("20201023"));
            System.out.println(format);
        }catch (Exception e){

        }
    }
}
