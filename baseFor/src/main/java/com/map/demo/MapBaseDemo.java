package com.map.demo;

import org.junit.Test;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.locks.ReentrantReadWriteLock;

/**
 * @author: jzq
 * @date: 2020/7/15 15:05   map的扩容因子  0.75   每次翻倍   容量只能是  2的幂次方   最多30
 * @description:
 * set  扩容和map基本一致
 * HashTable 的初始容量为11  扩容因子0.75    2n+1     11 ->23
 */
public class MapBaseDemo {
    @Test
    public  void testCompute(){
        //统计字母出现次数
        String str = "hello java, i am vary happy! nice to meet you";
        // jdk1.8的写法
        HashMap<Character, Integer> result2 = new HashMap<>(32);
        for (int i = 0; i < str.length(); i++) {
            char curChar = str.charAt(i);
            result2.compute(curChar, (k, v) -> {
                if (v == null) {
                    v = 1;
                } else {
                    v += 1;
                }
                return v;
            });
        }
        System.out.println(result2);
    }

    /**
     * 有了就不管，没得就放入
     */
    @Test
    public  void testPutIfAbsent() {
        HashMap<String, Object> map = new HashMap<>();
        map.put("1",11);
        map.put("2", 22);
        map.putIfAbsent("3",33);
        map.putIfAbsent("2",44);
        // map.computeIfAbsent("3", (value)->88);
//        Object o= map.computeIfAbsent("1L", (k) -> new ReentrantReadWriteLock(true));
        ReentrantReadWriteLock lock = new ReentrantReadWriteLock(false);
        Object o1= map.computeIfAbsent("1L", (k) -> {
            System.out.println(k);
            return lock ;
        });
        Object o2 = map.get("1L");

        System.out.println(o2==lock);
    }
    /**
     * 有了就不管，没得就放入
     */
    @Test
    public  void testComputeIfAbsent() {
        HashMap<String, Integer> map = new HashMap<>();
        map.put("1",11);
        map.put("2", 22);
        map.computeIfAbsent("3", (value) -> (map.get(value) == null ? 8 :( map.get(value) + 9 )));
       // map.computeIfAbsent("3", (value)->88);
        map.computeIfAbsent("2",(value) -> (map.get(value) == null ? 11 :( map.get(value) + 12 )));
        System.out.println(map);
    }

    /**
     * 没有的就不管
     */
    @Test
    public  void testComputeIfPresent(){
        HashMap<String, Integer> map = new HashMap<>();
        map.put("1",2);
        map.put("2", 3);
        //没有就不管
        map.computeIfPresent("3", (key,value) -> {
            System.out.println(key);
            System.out.println(value);
            return 3;
        });
        map.computeIfPresent("2",(key,value) -> value + 3);
        System.out.println(map);
        //{1=2, 2=6}
    }

    @Test
    public  void testMerge(){
        HashMap<String, Integer> map = new HashMap<>();
        map.put("1",11);
        map.put("2",1);
        map.merge("1",4,(o1,o2)->{
            System.out.println(o1);
            System.out.println(o2);
            return o1+o2;
        });
        System.out.println(map);
        //统计字母出现次数
        String str = "hello java, i am vary happy! nice to meet you";
        HashMap<Character, Integer> result2 = new HashMap<>(32);
        for (int i = 0; i < str.length(); i++) {
            //=9, !=1, a=5, c=1, e=4, h=2, i=2, j=1, ,=1, l=2, m=2, n=1, o=3, p=2, r=1, t=2, u=1, v=2, y=3
            result2.merge(str.charAt(i),2, Integer::sum);
        }
        System.out.println(result2);
    }
    @Test
    public  void testMergeList(){
        HashMap<String, List<TestEntity>> map = new HashMap<>();
        List<TestEntity> list1 = new ArrayList<>();
        TestEntity entity1 = new TestEntity();
        entity1.setId(1);
        entity1.setName("jzq");
        list1.add(entity1);
        TestEntity entity2 = new TestEntity();
        entity2.setId(2);
        entity2.setName("jzq2");
        list1.add(entity2);
        map.put("1",list1);

        List<TestEntity> list2 = new ArrayList<>();
        TestEntity entity3 = new TestEntity();
        entity3.setId(3);
        entity3.setName("jzq3");
        list2.add(entity3);
        TestEntity entity4 = new TestEntity();
        entity4.setId(4);
        entity4.setName("jzq4");
        list2.add(entity4);
        map.put("1",list1);
        map.put("2",list2);
        List<TestEntity> list3 = new ArrayList<>();
        TestEntity entity5 = new TestEntity();
        entity5.setId(5);
        entity5.setName("jzq5");
        list3.add(entity5);
        map.merge("3",list3,(o1,o2)->{
            o1.addAll(o2);
            return o1;
        });
        System.out.println(map);

    }
    @Test
    public  void testReplace(){
        HashMap<String, String> map = new HashMap<>();
        map.put("1","1");
        map.put("2","1");
        map.replace("1","****1");
        System.out.println(map);
        map.replaceAll((key,value)->key+value);
        System.out.println(map);
    }
    @Test
    public  void testFinal(){
        TestEntity.staticMap.put("4", "4");
        for (Map.Entry<String, String> entry : TestEntity.staticMap.entrySet()) {
            System.out.println(entry.getKey()+"+"+entry.getValue());
        }
    }

}