package com.map.demo;

import java.util.HashMap;
import java.util.Map;

public class TestEntity {
    private  Integer  id;
    private  String name;
    final   static Map<String ,String> staticMap=new HashMap<>();
    static {
        staticMap.put("1","1");
        staticMap.put("2","2");
        staticMap.put("3","3");
    }
    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Override
    public String toString() {
        return "TestEntity{" +
                "id=" + id +
                ", name='" + name + '\'' +
                '}';
    }
}
