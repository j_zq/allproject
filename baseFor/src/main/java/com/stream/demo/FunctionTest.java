package com.stream.demo;

import org.junit.Test;

import java.util.function.BiFunction;
import java.util.function.Function;

public class FunctionTest {
    private Function<Integer,Integer > fun1 = i->i*i;
    private Function<Integer,Integer > fun2 = i->i+i;
    private Function<String ,Integer > fun3 = Integer::parseInt;
    @Test
    public void testFunctionCompose(){
        //workstations deals  主机 desktop tablet  software monitors
        //apply -->compose(fun1) --->fun2  3-->3*3-->3*3+3*3
        System.out.println(fun2.compose(fun1).apply(3));
    }
    @Test
    public void testFunctionAndThen(){
        //apply -->fun2 --->fun1  3-->3+3-->(3+3)*(3+3) ops inc
        System.out.println(fun2.andThen(fun1).apply(3));
    }

    @Test
    public void testBiFunctionCompose(){
        Integer i=1,j=2;
        System.out.println(testBiFunctionApply(i,j,this::testBiFunctionApply));
    }
    private Integer testBiFunctionApply(Integer i,Integer j){
        return  i+j;
    }
    private Object  testBiFunctionApply(Integer i, Integer j, BiFunction<Integer,Integer,Integer> function){
        return     function.apply(i,j);
    }
}
