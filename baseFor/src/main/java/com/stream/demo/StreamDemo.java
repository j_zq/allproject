package com.stream.demo;

import com.entity.Goods;
import org.junit.Test;

import java.util.*;
import java.util.stream.Collectors;

/**
 * @author: jzq
 * @date: 2020/8/3 15:36
 * @description:   因为没有发现可用产品测试，现在未判断是否可用。
 */
public class StreamDemo {
    private  static  List<Goods> list= Collections.synchronizedList(new ArrayList<>());
    static {
        Goods goods = new Goods();
        goods.setPrice(1.0);
        goods.setNo(1);
        goods.setId(1);
        list.add(goods);

        Goods goods1 = new Goods();
        goods1.setPrice(2.0);
        goods1.setNo(2);goods1.setId(2);
        list.add(goods1);
        Goods goods2 = new Goods();
        goods2.setPrice(3.0);
        goods2.setNo(1);goods1.setId(2);
        list.add(goods2);
    }
    //peek 用法改变一个实体类 里面的部分属性
    @Test
    public void testStreamPeek(){
        list.stream().peek(o->{
            if(o.getId()==1){
                 o.setId(3);
            }
        }).forEach(System.out::println);
    }
    @Test
    public void testStreamListToMap(){
        Map<Integer, Integer> map = list.stream().
                collect(Collectors.toMap(Goods::getId, Goods::getNo));
        System.out.println(map);
    }
    //sort/min/max/distinct
    @Test
    public void testStreamMax(){
       double value = list.stream().mapToDouble(Goods::getPrice).max().getAsDouble();
        System.out.println(value);
    } @Test
    public void testStreamMax1(){
        OptionalInt anInt = list.stream().mapToInt(Goods::getNo).max();
        int value = anInt.getAsInt();
        System.out.println(value);
    }

    //匹配测试
    @Test
    public void testStreamMatch(){
        //任意一个匹配上都返回true
        boolean anyMatch = list.stream().anyMatch(o -> o.getNo() == 1);
        System.out.println(anyMatch);
        //全部匹配上才返回true
        boolean allMatch = list.stream().allMatch(o -> o.getNo() == 1);
        System.out.println(allMatch);
        //全部匹配不上才返回true
        boolean noneMatch = list.stream().noneMatch(o -> o.getNo() == 1);
        System.out.println(noneMatch);
    }
    //double数据处理
    @Test
    public void testStreamForDouble(){
        List<Goods> collect = list.stream().filter(o -> o.getId() >=1).collect(Collectors.toList());
        double sumPrice=0.0;
        for (Goods goods : collect) {
            sumPrice+=goods.getPrice();
        }
        System.out.println(sumPrice);
    }
    @Test
    public void testStreamToMap(){
        Map<Integer, Double> map = list.stream().collect(
                Collectors.toMap(Goods::getNo, Goods::getPrice, (k1, k2) -> k1));
        //mergeFunction   key 冲突合并处理 ddl  Megamenu
        System.out.println("1");

    }

}