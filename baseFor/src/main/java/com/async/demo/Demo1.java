package com.async.demo;

import com.sun.deploy.util.StringUtils;
import org.junit.Test;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

/**
 * @author: jzq
 * @date: 2020/7/15 14:34
 * @description:
 */
public class Demo1 extends BaseDemo{


    private final Object lock = new Object();

    @Override
    public void callback(long response) {
        System.out.println("得到结果");
        System.out.println(response);
        System.out.println("调用结束");
        synchronized (lock) {
            lock.notifyAll();
        }
    }

    public static void main(String[] args) {
        Demo1 demo1 = new Demo1();
        try {
            demo1.call();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }


        synchronized (demo1.lock){
            try {

                demo1.lock.notifyAll();
            } catch (Exception e) {
                e.printStackTrace();
            }
        }

        System.out.println("主线程内容");

    }
    @Test
    public void   ggg(){
        List<String> productCodes =new ArrayList<>();
        productCodes.add("0B47097");
        productCodes.add("11CD005WLS");
        String countryId ="4b69c70f-cd09-459a-a25f-39682c2a606c",
                storeId = "0e49db5e-0834-41ba-84fe-f796497ba9fc",
                soldTo = "1213083248";
        StringBuffer sb = new StringBuffer("soldto=");
        sb.append(soldTo).append("&storeId=").append(storeId).append("&countryId=").append(countryId).append("&mcode=")
                .append(StringUtils.join(productCodes,","));
        String params = sb.toString();
        System.out.println(params);
    }
    @Test
    public  void  test1(){
        List<A> list =new ArrayList<>();
        B b = new B();
        b.setName("1");
        list.add(b);
       list= list.stream().filter(ar -> {
                    B b1 = (B) ar;
                    if ("1".equals(b1.getName())) {
                        b1.setName("2");
                        return true;
                    }
                    return false;
                }
        ).collect(Collectors.toList());
//        list.add(a)
        for (A a : list) {
            B b1 = (B)a;
            System.out.println(b1);
        }
        List<A> collect = list.stream().skip(11L).limit(20).collect(Collectors.toList());
        for (A a : collect) {
            System.out.println(a);
        }
    }
    public  void  test2(){
        List<Integer>list=new ArrayList<>(30);
        for (int i = 0; i < 30; i++) {
            list.add(i);
        }
    }
}