package com.async.demo;

/**
 * @author: jzq
 * @date: 2020/7/15 14:33
 * @description:
 */
public abstract class BaseDemo {

    protected AsyncCall asyncCall = new AsyncCall();

    public abstract void callback(long response);

    public void call() throws InterruptedException {
        System.out.println("发起调用");
        asyncCall.call(this);
        System.out.println("调用返回");

    }

}