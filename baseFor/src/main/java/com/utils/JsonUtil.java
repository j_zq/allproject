package com.utils;

import org.json.JSONArray;
import org.json.JSONObject;
import org.junit.Test;

/**
 * @description: json转换工具类
 * @author: jzq
 * @date: 2020-10-28 14:55
 **/
public class JsonUtil {
    private  JSONObject jsonStringToJson(String jsonString)  {
        try {
            JSONObject jsonObject = new JSONObject(jsonString);
            return jsonObject;
        }catch (Exception e){
            e.printStackTrace();
            return null;
        }
    }
    private  JSONArray jsonStringArrayToJsonArray(String jsonArrayString,String arrayParam) throws Exception {
        // 判断json是否是一个数组，是：则跳过，否：则转换为数组

        JSONObject jsonArray2 = new JSONObject(jsonArrayString);
        try {
            // 将字符串转换为数组
            return jsonArray2.getJSONArray(arrayParam);
        }catch (Exception e){
            String arrayParamJson = jsonArray2.get(arrayParam).toString();
            if (!arrayParamJson.contains("[{")) {
                arrayParamJson = arrayParamJson.replaceAll("\\{", "[{").replaceAll("\\}", "}]");
            }
            return new JSONArray(arrayParamJson);
        }
    }
    @Test
    public  void testJson() throws Exception {
        String str= "{\n" +
                "\t\"header\": {\n" +
                "\t\t\n" +
                "\t\t\"message\": \"123\"\n" +
                "\t},\n" +
                "\t\"item\": {\n" +
                "\t\t\"mjahr\": \"2020\"\n" +
                "\t}\n" +
                "}";
        JSONArray array = jsonStringArrayToJsonArray(str, "item");
        System.out.println(array);

    }
    @Test
    public   void test22(){
        String  str= "SELECT\tf.FIELD0001 HTNAME,\tf.FIELD0027 HTCODE,\titem.SHOWVALUE HTTYPE,\tenum.SHOWVALUE HT,\t\n" +
                "sfk.SHOWVALUE SFKTYPE,\tstate.SHOWVALUE HTSTATE,\tf1.FIELD0130 WLCODE,\tf.FIELD0028 STARTDATETIME,\tf.FIELD0029 ENDDATETIME,\t\n" +
                "member.NAME MEMBERNAME,\tmember.CODE MEMBERCODE,\tf.FIELD0047 MONEYS,\tf.FIELD0084 KSLX,\tf.FIELD0148 LIFNR,\t\n" +
                "ad.AD_NAME ADNAME FROM\tFORMMAIN_0014 f\tLEFT JOIN CTP_ENUM_ITEM item ON f.FIELD0025 = item.ID\tLEFT JOIN CTP_ENUM_ITEM enum \n" +
                "ON f.FIELD0022 = enum.ID\tLEFT JOIN CTP_ENUM_ITEM sfk ON f.FIELD0045 = sfk.ID\tLEFT JOIN CTP_ENUM_ITEM state ON f.FIELD0023 = state.ID\t\n" +
                "LEFT JOIN formson_0019 f1 ON f.ID = f1.FORMMAIN_ID\tLEFT JOIN ORG_MEMBER member ON f.FIELD0013 = member.ID\t\n" +
                "LEFT JOIN ORG_PRINCIPAL_AD ad ON f.FIELD0013 = ad.MEMBER_ID WHERE\t\n" +
                "f.FIELD0027 IS NOT NULL \tAND f.FIELD0023 = '-2816359712461861108'   AND ad.ad_name = 'test2'";
        System.out.println(str);
    }
    @Test
    public   void test33(){
////2147483648
        System.out.println(Long.MAX_VALUE);
        System.out.println(Integer.MAX_VALUE);
    }
}
