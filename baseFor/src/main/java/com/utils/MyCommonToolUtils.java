package com.utils;

import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.math.BigDecimal;
import java.util.*;
import java.util.concurrent.ConcurrentHashMap;
import java.util.stream.Collectors;

/**
 * @Auther: jzq
 * @Date: 2020/4/17 11:30
 * @description: 常用工具类
 */
public class MyCommonToolUtils<T> {
    //全部不为空返回false
    public static boolean isEmpty(String... values) {
        if (values == null || values.length <= 0)
            return true;
        for (String value : values) {
            if (value == null || value.equals("") || value.equals("null"))
                return true;
        }
        return false;
    }

    //全部不为空返回true
    public static boolean isNotEmpty(String... values) {
        return !isEmpty(values);
    }

    //全部不为空返回false
    public static boolean isEmpty(String value) {
        return value == null || value.equals("") || "null".equals(value);
    }

    //全部不为空返回true
    public static boolean isNotEmpty(String value) {
        return !isEmpty(value);
    }

    //参数不为null 返回true
    public static boolean isNull(Object value) {
        return value == null;
    }

    //参数不为null 返回true
    public static boolean isNotNull(Object value) {
        return !isNull(value);
    }

    //所有的参数均不为null 返回false
    public static boolean isNull(Object... values) {
        if (values == null || values.length <= 0)
            return true;
        for (Object value : values) {
            if (value == null)
                return true;
        }
        return false;
    }

    //所有的参数均不为null 返回true
    public static boolean isNotNull(Object... values) {
        return !isNull(values);
    }

    //实体类是否存在null值（必须所有的参数都不为null）
    public boolean isEmpty(T t)
            throws NoSuchMethodException, InvocationTargetException, IllegalAccessException {
        Class<?> classes = t.getClass();
        String name = "";
        for (Field field : classes.getDeclaredFields()) {
            name = field.getName();
            Method method = t.getClass().getMethod("get" + MyCommonToolUtils.initialToUpperCase(name));
            Object invoke = method.invoke(t);
            if (invoke == null || "".equals(invoke)) {
                return true;
            }
        }
        return false;
    }

    public boolean isNotEmpty(T t) throws NoSuchMethodException, IllegalAccessException,
            InvocationTargetException {
        return !isEmpty(t);
    }

    //实体类是否存在null值， ignoreParams代表可以为null的值
    public boolean isEmpty(T t, String... ignoreParams)
            throws NoSuchMethodException, InvocationTargetException, IllegalAccessException {
        if (ignoreParams == null || ignoreParams.length <= 0)
            return isEmpty(t);
        Class<?> classes = t.getClass();
        String name;
        boolean isContinue = true;
        for (Field field : classes.getDeclaredFields()) {
            name = field.getName();
            for (String param : ignoreParams) {
                if (name.equals(param)) {
                    isContinue = false;
                    break;
                }
            }
            if (isContinue) {
                Method method = t.getClass().getMethod("get" + MyCommonToolUtils.initialToUpperCase(name));
                Object invoke = method.invoke(t);
                if (invoke == null || isEmpty(invoke.toString())) {
                    return true;
                }
            } else {
                isContinue = true;
            }
        }
        return false;
    }

    public boolean isNotEmpty(T t, String... ignoreParams)
            throws NoSuchMethodException, InvocationTargetException, IllegalAccessException {
        return !isEmpty(t, ignoreParams);
    }
    //   功能需要完善
    public T nullToEmpty(T t) {
        Class<?> eClass = t.getClass();
        Field[] fields = eClass.getDeclaredFields();
        for (Field field : fields) {
            String type = field.getType().getSimpleName();
            field.setAccessible(true);
            String name = MyCommonToolUtils.initialToUpperCase(field.getName());
            if (type.contains("int") || type.contains("Int")) {
                try {
                    Object invoke = eClass.getMethod("get" + name).invoke(t);
                    if (invoke == null) {
                        field.set(t, 0);
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            } else if (type.contains("String")) {
                try {
                    Object invoke = eClass.getMethod("get" + name).invoke(t);
                    if (invoke == null) {
                        field.set(t, "");
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            } else if (type.contains("double") || type.contains("Double")) {
                try {
                    Object invoke = eClass.getMethod("get" + name).invoke(t);
                    if (invoke == null) {
                        field.set(t, 0.0);
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            } else if (type.contains("BigInteger")) {
                try {
                    Object invoke = eClass.getMethod("get" + name).invoke(t);
                    if (invoke == null) {
                        field.set(t, 0L);
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            } else if (type.contains("BigDecimal")) {
                try {
                    Object invoke = eClass.getMethod("get" + name).invoke(t);
                    if (invoke == null) {
                        field.set(t, new BigDecimal("0"));
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }
        return t;
    }

    public static boolean isEmpty(Collection collection) {

        return collection == null || collection.size() <= 0;
    }

    public  static boolean isNotEmpty(Collection collection) {

        return collection != null && collection.size() > 0;
    }

    public static boolean isEmpty(Map map) {
        return map == null || map.size() <= 0;
    }

    public static boolean isNotEmpty(Map map) {

        return map != null && map.size() > 0;
    }

    //集合去重
    public List<T> listDistinct(List<T> list) {
        if (MyCommonToolUtils.isEmpty(list)) {
            return list.stream().distinct().collect(Collectors.toList());
        }
        return new ArrayList<>();
    }


    //集合去重  distinctName根据对象的属性名去重
    public List listDistinctT(List<T> list, String distinctName) {
        if (MyCommonToolUtils.isNotEmpty(list)) {
            String finalName = MyCommonToolUtils.initialToUpperCase(distinctName);
            Map<Object, Boolean> map = new ConcurrentHashMap<>();
            list = list.stream().filter(t -> {
                try {
                    return map.putIfAbsent(
                            t.getClass().getMethod("get" + finalName).invoke(t), Boolean.TRUE) == null;
                } catch (Exception e) {
                    e.printStackTrace();
                }
                return false;
            }).collect(Collectors.toList());
        }
        return list;
    }

    //集合去重  distinctName默认值为id
    public List listDistinctT(List<T> list) {
        return this.listDistinctT(list, "id");
    }
    // 统计集合中对象某个属性值相同的  出现次数   statisticalName为统计次数的依据  实体类的属性名
    public  Map<Object, Integer> listStatistical(List<T>  list,String  statisticalName ){
        HashMap<Object, Integer> hashMap = new HashMap<>();
        if( MyCommonToolUtils.isNotEmpty(list)){
           for (T t : list) {
               try {
                   Object invoke = t.getClass().
                           getMethod("get"+MyCommonToolUtils.initialToUpperCase(statisticalName)).invoke(t);
                   hashMap.merge(invoke,1, Integer::sum);
               } catch (Exception ex) {
                   ex.printStackTrace();
               }
           }
           return hashMap;
       }
        return  new HashMap<>();
    }
    //字符串首字母转大写
    public static   String initialToUpperCase(String str){
        if(MyCommonToolUtils.isNotEmpty(str)){
            return str.substring(0,1).toUpperCase()+str.substring(1);
        }else {
            return str;
        }
    }

}