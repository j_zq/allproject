import com.alibaba.fastjson.JSON;
import kingdee.bos.webapi.client.K3CloudApiClient;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * 物料  1
 */
public class DemoMaterialTest {
	static String K3CloudURL = "http://192.168.1.113:82";
	static String dbId = "5ee775fed6da90";
	static String uid = "administrator";
	static String pwd = "hr0103";
	static int lang = 2052;

	public static void main(String[] args) throws Exception {
		save();
	}
	public static void save() {
		try {
			K3CloudApiClient client = new K3CloudApiClient(K3CloudURL);
			Boolean result = client.login(dbId, uid, pwd, lang);
			if (result) {
				String formId = "BD_MATERIAL";
				Map<String, Object> data = new HashMap<String, Object>();
				Map<String, Object> dataMap = new HashMap<String, Object>();
				dataMap.put("FName", "开发测试联调物料");
				/*物料编码*/
				dataMap.put("FNumber", "HR_20210322001");
				/*创建组织*/
				Map<String, Object> createOrgMap = new HashMap<String, Object>();
				createOrgMap.put("FNumber", "100");
				dataMap.put("FCreateOrgId", createOrgMap);
				/*使用组织*/
				Map<String, Object> useOrgMap = new HashMap<String, Object>();
				useOrgMap.put("FNumber", "100");
				dataMap.put("FUseOrgId", useOrgMap);
				
				/*------------------------------条形码实体--------------------------------------*/
				List<Map<String, Object>> txmList = new ArrayList<Map<String,Object>>();
				Map<String, Object> txmMap = new HashMap<String, Object>();
				/*条形码类型*/
				txmMap.put("FCodeType_CMK", "");
				/*计量单位*/
				Map<String, Object> jldwMap = new HashMap<String, Object>();
				jldwMap.put("FNUMBER", "");
				jldwMap.put("FUnitId_CMK", jldwMap);
				txmList.add(txmMap);
				dataMap.put("FBarCodeEntity_CMK", txmList);
				
				/*---------------------------------------基本实体-----------------------------------------*/
				Map<String, Object> jbMap = new HashMap<String, Object>();
				/*物料属性*/
				jbMap.put("FErpClsID", "");
				/*基本单位*/
				Map<String, Object> fjbdwMap = new HashMap<String, Object>();
				fjbdwMap.put("FNumber", "");
				jbMap.put("FBaseUnitId", fjbdwMap);
				/*存货类别*/
				Map<String, Object> chlbMap = new HashMap<String, Object>();
				chlbMap.put("FNumber", "");
				jbMap.put("FCategoryID", chlbMap);
				/*套件*/
				jbMap.put("FSuite", chlbMap);
				dataMap.put("SubHeadEntity", jbMap);
				
				/*---------------------------------------库存实体-----------------------------------------*/
				Map<String, Object> kcMap = new HashMap<String, Object>();
				/*库存单位*/
				Map<String, Object> kcdwMap = new HashMap<String, Object>();
				kcdwMap.put("FNumber", "");
				kcMap.put("FStoreUnitID", kcdwMap);
				/*币别*/
				Map<String, Object> bbMap = new HashMap<String, Object>();
				bbMap.put("FNumber", "");
				kcMap.put("FCurrencyId", bbMap);
				/*换算方向*/
				kcMap.put("FUnitConvertDir", "");
				/*序列号生成时机*/
				kcMap.put("FSNGenerateTime", "");
				/*业务范围*/
				kcMap.put("FSNManageType", "");
				dataMap.put("SubHeadEntity1", kcMap);
				
				/*---------------------------------------销售实体-----------------------------------------*/
				Map<String, Object> xsMap = new HashMap<String, Object>();
				/*销售计价单位*/
				Map<String, Object> xsjjdwMap = new HashMap<String, Object>();
				xsjjdwMap.put("FNumber", "");
				xsMap.put("FSalePriceUnitId", xsjjdwMap);
				/*销售单位*/
				Map<String, Object> xsdwMap = new HashMap<String, Object>();
				xsdwMap.put("FNumber", "");
				xsMap.put("FSaleUnitId", xsdwMap);
				dataMap.put("SubHeadEntity2", xsMap);
				
				/*---------------------------------------采购实体-----------------------------------------*/
				Map<String, Object> cgMap = new HashMap<String, Object>();
				/*采购单位*/
				Map<String, Object> cgdwMap = new HashMap<String, Object>();
				cgdwMap.put("FNumber", "");
				cgMap.put("FPurchaseUnitId", cgdwMap);
				/*采购计价单位*/
				Map<String, Object> cgjjdwMap = new HashMap<String, Object>();
				cgjjdwMap.put("FNumber", "");
				cgMap.put("FPurchasePriceUnitId", cgjjdwMap);
				/*配额方式*/
				cgMap.put("FQuotaType", "");
				dataMap.put("SubHeadEntity3", cgMap);
				
				/*--------------------------------------计划实体-----------------------------------------*/
				Map<String, Object> jhMap = new HashMap<String, Object>();
				/*计划策略*/
				jhMap.put("FPlanningStrategy", "");
				/*订货策略*/
				jhMap.put("FOrderPolicy", "");
				/*固定提前期单位*/
				jhMap.put("FFixLeadTimeType", "");
				/*变动提前期单位*/
				jhMap.put("FVarLeadTimeType", "");
				/*检验提前期单位*/
				jhMap.put("FCheckLeadTimeType", "");
				/*订货间隔期单位*/
				jhMap.put("FOrderIntervalTimeType", "");
				/*预留类型*/
				jhMap.put("FReserveType", "");
				/*时间单位*/
				jhMap.put("FPlanOffsetTimeType", "");
				dataMap.put("SubHeadEntity4", jhMap);
				
				/*--------------------------------------生产实体-----------------------------------------*/
				Map<String, Object> scMap = new HashMap<String, Object>();
				/*发料方式*/
				scMap.put("FIssueType", "");
				/*超发控制方式*/
				scMap.put("FOverControlMode", "");
				/*最小发料批量单位*/
				Map<String, Object> fldwMap = new HashMap<String, Object>();
				fldwMap.put("FNUMBER", "");
				scMap.put("FMinIssueUnitId", fldwMap);
				/*工时单位*/
				scMap.put("FStandHourUnitId", "");
				/*倒冲数量*/
				scMap.put("FBackFlushType", "");
				
				dataMap.put("SubHeadEntity5", scMap);
				
				/*--------------------------------------库存属性实体-----------------------------------------*/
				List<Map<String, Object>> kcsxList = new ArrayList<Map<String,Object>>();
				Map<String, Object> kcsxMap = new HashMap<String, Object>();
				Map<String, Object> kcsxIdMap = new HashMap<String, Object>();
				kcsxIdMap.put("FNumber", "");
				kcsxMap.put("FInvPtyId", kcsxIdMap);
				kcsxList.add(kcsxMap);
				dataMap.put("FEntityInvPty", kcsxList);
				
				data.put("Model", dataMap);

				String sResult = client.save(formId, JSON.toJSONString(data));
				System.out.println("推送物料json:" + JSON.toJSONString(data));
				System.out.println("物料保存成功返回信息:" + sResult);
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

}
