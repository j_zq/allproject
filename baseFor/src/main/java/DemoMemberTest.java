import com.alibaba.fastjson.JSON;
import kingdee.bos.webapi.client.K3CloudApiClient;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class DemoMemberTest {
	static String K3CloudURL = "http://192.168.1.113:82";
	static String dbId = "5ee775fed6da90";
	static String uid = "administrator";
	static String pwd = "hr0103";
	static int lang = 2052;

	public static void main(String[] args) throws Exception {
		save();
	}

	public static void save() {
		try {
			K3CloudApiClient client = new K3CloudApiClient(K3CloudURL);
			Boolean result = client.login(dbId, uid, pwd, lang);

			if (result) {
				String formId = "BD_Empinfo";
				Map<String, Object> data = new HashMap<String, Object>();
				Map<String, Object> dataMap = new HashMap<String, Object>();
				dataMap.put("FName", "测试张三");
				dataMap.put("FStaffNumber", "1025");
				/*创建组织*/
				Map<String, Object> createOrgMap = new HashMap<String, Object>();
				createOrgMap.put("FNumber", "100");
				dataMap.put("FCreateOrgId", createOrgMap);
				/*使用组织*/
				Map<String, Object> useOrgMap = new HashMap<String, Object>();
				useOrgMap.put("FNumber", "100");
				dataMap.put("FUseOrgId", useOrgMap);
				
				List<Map<String, Object>> listPostEntity = new ArrayList<Map<String, Object>>();
				Map<String, Object> entityMap = new HashMap<String, Object>();
				/*所属部门*/
				Map<String, Object> deptMap = new HashMap<String, Object>();
				deptMap.put("FNumber", "001002");
				entityMap.put("FPostDept", deptMap);
				/*就任岗位*/
				Map<String, Object> postMap = new HashMap<String, Object>();
				postMap.put("FNumber", "cs12121");
				entityMap.put("FPost", postMap);
				/*工作组织*/
				Map<String, Object> workOrgMap = new HashMap<String, Object>();
				workOrgMap.put("FNumber", "100");
				entityMap.put("FWorkOrgId", workOrgMap);
				/*是否主任岗*/
				entityMap.put("FIsFirstPost", true);
				/*任岗开始日期*/
				entityMap.put("FStaffStartDate", "2021-03-05");
				
				listPostEntity.add(entityMap);
				dataMap.put("FPostEntity", listPostEntity);

				data.put("Model", dataMap);
				String sResult = client.save(formId, JSON.toJSONString(data));
				System.out.println("人员推送json:" + JSON.toJSONString(data));
				System.out.println("人员保存成功返回信息:" + sResult);
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
}
