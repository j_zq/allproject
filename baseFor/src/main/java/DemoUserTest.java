import com.alibaba.fastjson.JSON;
import kingdee.bos.webapi.client.K3CloudApiClient;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

public class DemoUserTest {
	static String K3CloudURL = "http://192.168.1.113:82";
	static String dbId = "5ee775fed6da90";
	static String uid = "administrator";
	static String pwd = "hr0103";
	static int lang = 2052;

	public static void main(String[] args) throws Exception {
		save();
		System.out.println(new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(new Date()));
	}

	public static void save() {
		try {
			K3CloudApiClient client = new K3CloudApiClient(K3CloudURL);
			Boolean result = client.login(dbId, uid, pwd, lang);

			if (result) {
				String formId = "SEC_User";
				Map<String, Object> data = new HashMap<String, Object>();
				Map<String, Object> dataMap = new HashMap<String, Object>();
				dataMap.put("FName", "开发测试联调用户");
				/*移动电话*/
				dataMap.put("FPhone", "13812934319");
				/*许可分组*/
				dataMap.put("FAppGroup", "");
				/*部门分组*/
				Map<String, Object> deptMap = new HashMap<String, Object>();
				deptMap.put("FNumber", "100");
				dataMap.put("FPRIMARYGROUP", deptMap);
				
				data.put("Model", dataMap);

				String sResult = client.save(formId, JSON.toJSONString(data));
				System.out.println("推送用户json:" + JSON.toJSONString(data));
				System.out.println("用户保存成功返回信息:" + sResult);
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

}
