import com.alibaba.fastjson.JSON;
import kingdee.bos.webapi.client.K3CloudApiClient;

import java.util.HashMap;
import java.util.Map;

public class DemoPostTest {
	static String K3CloudURL = "http://192.168.1.113:82";
	static String dbId = "5ee775fed6da90";
	static String uid = "administrator";
	static String pwd = "hr0103";
	static int lang = 2052;

	public static void main(String[] args) throws Exception {
		save();
	}

	public static void save() {
		try {
			K3CloudApiClient client = new K3CloudApiClient(K3CloudURL);
			Boolean result = client.login(dbId, uid, pwd, lang);

			if (result) {
				String formId = "HR_ORG_HRPOST";
				Map<String, Object> data = new HashMap<String, Object>();
				Map<String, Object> dataMap = new HashMap<String, Object>();
				dataMap.put("FName", "开发测试岗位123");
				/* 创建组织 */
				Map<String, Object> createOrgMap = new HashMap<String, Object>();
				createOrgMap.put("FNumber", "100");
				dataMap.put("FCreateOrgId", createOrgMap);
				/* 使用组织 */
				Map<String, Object> useOrgMap = new HashMap<String, Object>();
				useOrgMap.put("FNumber", "100");
				dataMap.put("FUseOrgId", useOrgMap);
				/* 生效日期 */
				dataMap.put("FEffectDate", "2021-03-05");
				/* 失效日期 */
				dataMap.put("FLapseDate", "9999-12-31");
				/* 所属部门 */
				Map<String, Object> deptMap = new HashMap<String, Object>();
				deptMap.put("FNumber", "001002");
				dataMap.put("FNumber", "cs12121");
				dataMap.put("FDept", deptMap);
//				List<Map<String, Object>> listReport = new ArrayList<Map<String, Object>>();
//				Map<String, Object> postCodeMap = new HashMap<String, Object>();
//				postCodeMap.put("FNumber", "cs1211");
//				Map<String, Object> reportMap = new HashMap<String, Object>();
//				reportMap.put("FSuperiorPost", postCodeMap);
//				listReport.add(reportMap);
//				dataMap.put("FReportEntitys", listReport);
				
				data.put("Model", dataMap);

				String sResult = client.save(formId, JSON.toJSONString(data));
				System.out.println("推送岗位json:" + JSON.toJSONString(data));
				System.out.println("岗位保存成功返回信息:" + sResult);
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

}
