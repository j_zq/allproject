import com.alibaba.fastjson.JSON;
import kingdee.bos.webapi.client.K3CloudApiClient;

import java.util.HashMap;
import java.util.Map;

//1
public class DemoProjectTest {
	static String K3CloudURL = "http://192.168.1.113:82";
	static String dbId = "5ee775fed6da90";
	static String uid = "administrator";
	static String pwd = "hr0103";
	static int lang = 2052;

	public static void main(String[] args) throws Exception {
		save();
	}
	public static void save() {
		try {
			K3CloudApiClient client = new K3CloudApiClient(K3CloudURL);
			Boolean result = client.login(dbId, uid, pwd, lang);
			if (result) {
				String formId = "PAEZ_xiangmuxinx";
				Map<String, Object> data = new HashMap<String, Object>();
				Map<String, Object> dataMap = new HashMap<String, Object>();
				dataMap.put("FName", "开发测试联调项目");
				/*项目编码*/
				dataMap.put("FNumber", "XM_20210322001");
				/*创建组织*/
				Map<String, Object> createOrgMap = new HashMap<String, Object>();
				createOrgMap.put("FNumber", "100");
				dataMap.put("FCreateOrgId", createOrgMap);
				/*使用组织*/
				Map<String, Object> useOrgMap = new HashMap<String, Object>();
				useOrgMap.put("FNumber", "100");
				dataMap.put("FUseOrgId", useOrgMap);
				/*财务项目性质*/
				Map<String, Object> cwxmxzMap = new HashMap<String, Object>();
				cwxmxzMap.put("FNumber", "100");
				dataMap.put("F_CWXMXZ", cwxmxzMap);
				
				data.put("Model", dataMap);

				String sResult = client.save(formId, JSON.toJSONString(data));
				System.out.println("推送项目json:" + JSON.toJSONString(data));
				System.out.println("项目保存成功返回信息:" + sResult);
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

}
