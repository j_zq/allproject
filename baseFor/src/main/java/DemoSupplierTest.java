import com.alibaba.fastjson.JSON;
import kingdee.bos.webapi.client.K3CloudApiClient;

import java.util.HashMap;
import java.util.Map;

/**
 * 供应商  1
 */
public class DemoSupplierTest {
	static String K3CloudURL = "http://192.168.1.113:82";
	static String dbId = "5ee775fed6da90";
	static String uid = "administrator";
	static String pwd = "hr0103";
	static int lang = 2052;

	public static void main(String[] args) throws Exception {
		save();
	}
	public static void save() {
		try {
			K3CloudApiClient client = new K3CloudApiClient(K3CloudURL);
			Boolean result = client.login(dbId, uid, pwd, lang);
			if (result) {
				String formId = "BD_Supplier";
				Map<String, Object> data = new HashMap<String, Object>();
				Map<String, Object> dataMap = new HashMap<String, Object>();
				dataMap.put("FName", "开发测试联调供应商");
				/*供应商编码*/
				dataMap.put("FNumber", "HR_20210322001");
				/*创建组织*/
				Map<String, Object> createOrgMap = new HashMap<String, Object>();
				createOrgMap.put("FNumber", "100");
				dataMap.put("FCreateOrgId", createOrgMap);
				/*使用组织*/
				Map<String, Object> useOrgMap = new HashMap<String, Object>();
				useOrgMap.put("FNumber", "100");
				dataMap.put("FUseOrgId", useOrgMap);
				
				Map<String, Object> financeMap = new HashMap<String, Object>();
				/*结算币别*/
				Map<String, Object> fpayMap = new HashMap<String, Object>();
				fpayMap.put("FNumber", "");
				financeMap.put("FPayCurrencyId", fpayMap);
				dataMap.put("FFinanceInfo", financeMap);
				
				Map<String, Object> flocationMap = new HashMap<String, Object>();
				/*地点名称*/
				flocationMap.put("FLocName", "");
				/*通讯地址*/
				flocationMap.put("FLocAddress", "");
				/*手机*/
				flocationMap.put("FLocMobile", "");
				/*联系人*/
				Map<String, Object> flocMap = new HashMap<String, Object>();
				flocMap.put("FNUMBER", "");
				flocationMap.put("FLocNewContact", flocMap);
				dataMap.put("FLocationInfo", flocationMap);
				data.put("Model", dataMap);

				String sResult = client.save(formId, JSON.toJSONString(data));
				System.out.println("推送项目json:" + JSON.toJSONString(data));
				System.out.println("项目保存成功返回信息:" + sResult);
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

}
