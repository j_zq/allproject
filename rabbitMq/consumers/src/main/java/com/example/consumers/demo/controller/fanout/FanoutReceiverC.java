package com.example.consumers.demo.controller.fanout;


import org.springframework.amqp.rabbit.annotation.RabbitHandler;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.stereotype.Component;

import java.util.Map;

/**
 * @Auther: jzq
 * @Date: 2020/4/2 17:10
 * @Description:
 */
@Component
@RabbitListener(queues = "fanout.C")
public class FanoutReceiverC {

    @RabbitHandler
    public void process(Map testMessage) {
        try{

            int i =  1/0;
            System.out.println("FanoutReceiverC消费者收到消息  : " +testMessage.toString());
        }catch (Exception e){
            System.out.println("FanoutReceiverC消费者收到消息   Exception: " +testMessage.toString());
        }

    }

}