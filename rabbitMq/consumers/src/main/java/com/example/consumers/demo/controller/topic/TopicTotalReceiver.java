package com.example.consumers.demo.controller.topic;

/**
 * @Auther: jzq
 * @Date: 2020/4/2 15:57
 * @Description:
 */

import org.springframework.amqp.rabbit.annotation.RabbitHandler;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.stereotype.Component;

import java.util.Map;

/**
 * @author  : JCccc
 * @date  : 2019/9/3
 * @description  :
 **/

@Component
@RabbitListener(queues = "topic.total")
public class TopicTotalReceiver {

    @RabbitHandler
    public void process(Map testMessage) {
        System.err.println("TopicTotalReceiver消费者收到消息  : " + testMessage.toString());
    }
}