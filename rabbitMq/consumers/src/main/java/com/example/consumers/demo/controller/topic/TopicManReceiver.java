package com.example.consumers.demo.controller.topic;

/**
 * @author: jzq
 * @date : 2020/4/2 15:56
 * @description :
 */

import org.springframework.amqp.rabbit.annotation.RabbitHandler;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.stereotype.Component;

import java.util.Map;

@Component
@RabbitListener(queues = "topic.man")
public class TopicManReceiver {

    @RabbitHandler
    public void process(Map testMessage) {
        System.err.println("TopicManReceiver消费者收到消息  : " + testMessage.toString());
    }
}