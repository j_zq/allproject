package com.example.consumers.demo.controller.topic;

/**
 * @author : jzq
 * @date : 2020/4/2 15:56
 * @description :
 */

import org.springframework.amqp.rabbit.annotation.RabbitHandler;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.stereotype.Component;

import java.util.Map;

@Component
@RabbitListener(queues = "topic.woman")
public class TopicWomanReceiver {

    @RabbitHandler
    public void process(Map testMessage) {
        System.err.println("TopicWomanReceiver消费者收到消息  : " + testMessage.toString());
    }
}